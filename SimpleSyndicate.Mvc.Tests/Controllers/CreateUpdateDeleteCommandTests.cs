﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Controllers;
using SimpleSyndicate.Mvc.Tests.App_Start;
using SimpleSyndicate.Mvc.Tests.Controllers.TestModels;
using SimpleSyndicate.Mvc.Tests.Models;

namespace SimpleSyndicate.Mvc.Tests.Controllers
{
	[TestClass]
	public class CreateUpdateDeleteCommandTests
	{
		[TestMethod]
		public void SaveChangesForCreateInputModel()
		{
			// arrange
			AutoMapperConfiguration.Configure();
			var testData = new TestData.TestData();
			var command = new CreateUpdateDeleteCommand<CreateInputModel, EditInputModel, TestModel>(testData.Repositories.TestModelRepository);
			var createInputModel = new CreateInputModel() { Name = "Z", Value = "Z1" };
			var count = testData.Repositories.TestModelRepository.Count();

			// act
			command.SaveChanges(createInputModel);

			// assert
			Assert.AreEqual(count + 1, testData.Repositories.TestModelRepository.Count());
			Assert.AreEqual("Z1", testData.Repositories.TestModelRepository.All().Where(x => x.Name == "Z").First().Value);
		}

		[TestMethod]
		[ExpectedException(typeof(System.InvalidOperationException))]
		public void SaveChangesForEditInputModelInvalidId()
		{
			// arrange
			AutoMapperConfiguration.Configure();
			var testData = new TestData.TestData();
			var command = new CreateUpdateDeleteCommand<CreateInputModel, EditInputModel, TestModel>(testData.Repositories.TestModelRepository);
			var editInputModel = new EditInputModel() { Id = -1, Name = "Z", Value = "Z1" };

			// act
			command.SaveChanges(editInputModel);

			// assert
		}

		[TestMethod]
		public void SaveChangesForEditInputModelItemUpdated()
		{
			// arrange
			AutoMapperConfiguration.Configure();
			var testData = new TestData.TestData();
			var command = new CreateUpdateDeleteCommand<CreateInputModel, EditInputModel, TestModel>(testData.Repositories.TestModelRepository);
			var testModel = testData.Repositories.TestModelRepository.All().First();
			var editInputModel = new EditInputModel() { Id = testModel.Id, Name = testModel.Name + "2", Value = testModel.Value + "2" };
			var count = testData.Repositories.TestModelRepository.Count();

			// act
			command.SaveChanges(editInputModel);

			// assert
			Assert.AreEqual(count, testData.Repositories.TestModelRepository.Count());
			Assert.AreEqual(0, testData.Repositories.TestModelRepository.All().Where(x => x.Name == testModel.Name).Count());
			Assert.AreEqual(1, testData.Repositories.TestModelRepository.All().Where(x => x.Value == testModel.Value + "2").Count());
		}

		[TestMethod]
		public void SaveChangesForEditInputModelNoOtherItemsUpdated()
		{
			// arrange
			AutoMapperConfiguration.Configure();
			var testData = new TestData.TestData();
			var command = new CreateUpdateDeleteCommand<CreateInputModel, EditInputModel, TestModel>(testData.Repositories.TestModelRepository);
			var testModel = testData.Repositories.TestModelRepository.All().First();
			var testModel2 = testData.Repositories.TestModelRepository.All().Skip(1).First();
			var editInputModel = new EditInputModel() { Id = testModel.Id, Name = testModel.Name + "2", Value = testModel.Value };
			var count = testData.Repositories.TestModelRepository.Count();

			// act
			command.SaveChanges(editInputModel);

			// assert
			Assert.AreEqual(count, testData.Repositories.TestModelRepository.Count());
			Assert.AreEqual(1, testData.Repositories.TestModelRepository.All().Where(x => x.Name == testModel2.Name).Count());
		}

		[TestMethod]
		public void DeleteIdDoesNotExist()
		{
			// arrange
			AutoMapperConfiguration.Configure();
			var testData = new TestData.TestData();
			var command = new CreateUpdateDeleteCommand<CreateInputModel, EditInputModel, TestModel>(testData.Repositories.TestModelRepository);
			var count = testData.Repositories.TestModelRepository.Count();

			// act
			command.Delete(-1);

			// assert
			Assert.AreEqual(count, testData.Repositories.TestModelRepository.Count());
		}

		[TestMethod]
		public void DeleteIdExists()
		{
			// arrange
			AutoMapperConfiguration.Configure();
			var testData = new TestData.TestData();
			var command = new CreateUpdateDeleteCommand<CreateInputModel, EditInputModel, TestModel>(testData.Repositories.TestModelRepository);
			var count = testData.Repositories.TestModelRepository.Count();

			// act
			command.Delete(testData.Repositories.TestModelRepository.All().First().Id);

			// assert
			Assert.AreEqual(count - 1, testData.Repositories.TestModelRepository.Count());
		}
	}
}
