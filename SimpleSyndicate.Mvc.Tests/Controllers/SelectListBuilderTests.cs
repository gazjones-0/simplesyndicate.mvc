﻿using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Controllers;

namespace SimpleSyndicate.Mvc.Tests.Controllers
{
	[TestClass]
	public class BuilderTests
	{
		[TestMethod]
		public void SelectListIsNotNull()
		{
			// arrange
			var testData = new TestData.TestData();
			var query = testData.Repositories.SelectListItemViewModelRepository.All();

			// act
			var selectList = SelectListBuilder.SelectList(query);

			// assert
			Assert.IsNotNull(selectList);
		}

		[TestMethod]
		public void SelectListIdMapsToValue()
		{
			// arrange
			var testData = new TestData.TestData();
			var query = testData.Repositories.SelectListItemViewModelRepository.All();

			// act
			var selectList = SelectListBuilder.SelectList(query);

			// assert
			Assert.AreEqual(testData.Repositories.SelectListItemViewModelRepository.All().OrderBy(x => x.Name).First().Id.ToString(), selectList.First().Value);
		}

		[TestMethod]
		public void SelectListNameMapsToText()
		{
			// arrange
			var testData = new TestData.TestData();
			var query = testData.Repositories.SelectListItemViewModelRepository.All();

			// act
			var selectList = SelectListBuilder.SelectList(query);

			// assert
			Assert.AreEqual(testData.Repositories.SelectListItemViewModelRepository.All().OrderBy(x => x.Name).First().Name, selectList.First().Text);
		}

		[TestMethod]
		public void SelectListHasAllItems()
		{
			// arrange
			var testData = new TestData.TestData();
			var query = testData.Repositories.SelectListItemViewModelRepository.All();

			// act
			var selectList = SelectListBuilder.SelectListItems(query);

			// assert
			Assert.AreEqual(testData.Repositories.SelectListItemViewModelRepository.Count(), selectList.Count());
		}

		[TestMethod]
		public void SelectListItemsIsNotNull()
		{
			// arrange
			var testData = new TestData.TestData();
			var query = testData.Repositories.SelectListItemViewModelRepository.All();

			// act
			var selectListItems = SelectListBuilder.SelectListItems(query);

			// assert
			Assert.IsNotNull(selectListItems);
		}

		[TestMethod]
		public void SelectListItemsHasAllItems()
		{
			// arrange
			var testData = new TestData.TestData();
			var query = testData.Repositories.SelectListItemViewModelRepository.All();

			// act
			var selectListItems = SelectListBuilder.SelectListItems(query);

			// assert
			Assert.AreEqual(testData.Repositories.SelectListItemViewModelRepository.All().Count(), selectListItems.Count());
		}
	}
}
