﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.App_Start.AutoMapperMappings;
using SimpleSyndicate.Mvc.Controllers.VersionHistory;

namespace SimpleSyndicate.Mvc.Tests.Controllers.VersionHistory
{
	[TestClass]
	public class VersionHistoryBuilderTests
	{
		[TestMethod]
		public void CurrentVersionViewModelIsNotNull()
		{
			// arrange
			VersionHistoryItem.CreateMaps();
			var testData = new TestData.TestData();
			var builder = new VersionHistoryBuilder(testData.Repositories.VersionHistoryItemRepository);

			// act
			var viewModel = builder.CurrentVersionViewModel();

			// assert
			Assert.IsNotNull(viewModel);
		}

		[TestMethod]
		public void CurrentVersionViewModelIsLatest()
		{
			// arrange
			VersionHistoryItem.CreateMaps();
			var testData = new TestData.TestData();
			var builder = new VersionHistoryBuilder(testData.Repositories.VersionHistoryItemRepository);

			// act
			var viewModel = builder.CurrentVersionViewModel();

			// assert
			Assert.AreEqual(2, viewModel.MajorVersion);
			Assert.AreEqual(1, viewModel.MinorVersion);
			Assert.AreEqual(1, viewModel.PointVersion);
		}

		[TestMethod]
		public void IndexViewModelIsNotNull()
		{
			// arrange
			VersionHistoryItem.CreateMaps();
			var testData = new TestData.TestData();
			var builder = new VersionHistoryBuilder(testData.Repositories.VersionHistoryItemRepository);

			// act
			var viewModel = builder.IndexViewModel();

			// assert
			Assert.IsNotNull(viewModel);
		}

		[TestMethod]
		public void IndexViewModelHasAllItems()
		{
			// arrange
			VersionHistoryItem.CreateMaps();
			var testData = new TestData.TestData();
			var builder = new VersionHistoryBuilder(testData.Repositories.VersionHistoryItemRepository);

			// act
			var viewModel = builder.IndexViewModel();

			// assert
			Assert.AreEqual(testData.Repositories.VersionHistoryItemRepository.Count(), viewModel.Count);
		}

		[TestMethod]
		public void IndexViewModelIsOrdered()
		{
			// arrange
			VersionHistoryItem.CreateMaps();
			var testData = new TestData.TestData();
			var builder = new VersionHistoryBuilder(testData.Repositories.VersionHistoryItemRepository);

			// act
			var viewModel = builder.IndexViewModel();

			// assert
			Assert.AreEqual(2, viewModel[0].MajorVersion);
			Assert.AreEqual(1, viewModel[0].MinorVersion);
			Assert.AreEqual(1, viewModel[0].PointVersion);

			Assert.AreEqual(2, viewModel[1].MajorVersion);
			Assert.AreEqual(1, viewModel[1].MinorVersion);
			Assert.AreEqual(0, viewModel[1].PointVersion);

			Assert.AreEqual(2, viewModel[2].MajorVersion);
			Assert.AreEqual(0, viewModel[2].MinorVersion);
			Assert.AreEqual(0, viewModel[2].PointVersion);

			Assert.AreEqual(1, viewModel[3].MajorVersion);
			Assert.AreEqual(0, viewModel[3].MinorVersion);
			Assert.AreEqual(2, viewModel[3].PointVersion);

			Assert.AreEqual(1, viewModel[4].MajorVersion);
			Assert.AreEqual(0, viewModel[4].MinorVersion);
			Assert.AreEqual(1, viewModel[4].PointVersion);

			Assert.AreEqual(1, viewModel[5].MajorVersion);
			Assert.AreEqual(0, viewModel[5].MinorVersion);
			Assert.AreEqual(0, viewModel[5].PointVersion);
		}
	}
}
