﻿using System;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.App_Start.AutoMapperMappings;
using SimpleSyndicate.Mvc.Controllers.VersionHistory;

namespace SimpleSyndicate.Mvc.Tests.Controllers.VersionHistory
{
	[TestClass]
	public class GenericVersionHistoryControllerTests
	{
		[TestMethod]
		public void IndexReturnsView()
		{
			// arrange
			VersionHistoryItem.CreateMaps();
			var testData = new TestData.TestData();
			var controller = new GenericVersionHistoryController(testData.Repositories.VersionHistoryItemRepository);

			// act
			ViewResult viewResult = controller.Index();

			// assert
			Assert.IsNotNull(viewResult);
			Assert.IsTrue(String.IsNullOrEmpty(viewResult.ViewName));
			Assert.IsInstanceOfType(viewResult.Model, typeof(IndexViewModel));
		}

		[TestMethod]
		public void IndexAllowsAnonymousAccess()
		{
			Assert.IsTrue(AttributeHelpers.HasAttribute<GenericVersionHistoryController, ViewResult>(x => x.Index(), typeof(AllowAnonymousAttribute)));
		}
	}
}
