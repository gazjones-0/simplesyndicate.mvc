﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Controllers.VersionHistory;
using SimpleSyndicate.Mvc.Tests.TestTools.UnitTesting;

namespace SimpleSyndicate.Mvc.Tests.Controllers.VersionHistory
{
	[TestClass]
	public class LatestVersionViewModelTests
	{
		[TestMethod]
		public void LatestVersionViewModelMajorVersionDisplayName()
		{
			MvcAssert.DisplayNameForEquals<CurrentVersionViewModel>("Major version", x => x.MajorVersion);
		}

		[TestMethod]
		public void LatestVersionViewModelMinorVersionDisplayName()
		{
			MvcAssert.DisplayNameForEquals<CurrentVersionViewModel>("Minor version", x => x.MinorVersion);
		}

		[TestMethod]
		public void LatestVersionViewModelPointVersionDisplayName()
		{
			MvcAssert.DisplayNameForEquals<CurrentVersionViewModel>("Point version", x => x.PointVersion);
		}

		[TestMethod]
		public void LatestVersionViewModelReleaseDateDisplayName()
		{
			MvcAssert.DisplayNameForEquals<CurrentVersionViewModel>("Release date", x => x.ReleaseDate);
		}
	}
}
