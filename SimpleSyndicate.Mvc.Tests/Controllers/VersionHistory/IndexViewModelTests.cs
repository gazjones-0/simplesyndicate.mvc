﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Controllers.VersionHistory;
using SimpleSyndicate.Mvc.Tests.TestTools.UnitTesting;

namespace SimpleSyndicate.Mvc.Tests.Controllers.VersionHistory
{
	[TestClass]
	public class IndexViewModelTests
	{
		[TestMethod]
		public void IndexViewModelMajorVersionDisplayName()
		{
			MvcAssert.DisplayNameForEquals<IndexViewModel>("Major version", x => x[0].MajorVersion);
		}

		[TestMethod]
		public void IndexViewModelMinorVersionDisplayName()
		{
			MvcAssert.DisplayNameForEquals<IndexViewModel>("Minor version", x => x[0].MinorVersion);
		}

		[TestMethod]
		public void IndexViewModelPointVersionDisplayName()
		{
			MvcAssert.DisplayNameForEquals<IndexViewModel>("Point version", x => x[0].PointVersion);
		}

		[TestMethod]
		public void IndexViewModelReleaseNotesDisplayName()
		{
			MvcAssert.DisplayNameForEquals<IndexViewModel>("Release notes", x => x[0].ReleaseNotes);
		}

		[TestMethod]
		public void IndexViewModelReleaseDateDisplayName()
		{
			MvcAssert.DisplayNameForEquals<IndexViewModel>("Release date", x => x[0].ReleaseDate);
		}
	}
}
