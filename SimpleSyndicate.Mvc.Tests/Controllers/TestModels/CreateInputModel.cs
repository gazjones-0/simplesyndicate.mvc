﻿namespace SimpleSyndicate.Mvc.Tests.Controllers.TestModels
{
	public class CreateInputModel
	{
		public string Name { get; set; }

		public string Value { get; set; }
	}
}
