﻿namespace SimpleSyndicate.Mvc.Tests.Controllers.TestModels
{
	public class EditInputModel
	{
		public int Id { get; set; }

		public string Name { get; set; }

		public string Value { get; set; }
	}
}
