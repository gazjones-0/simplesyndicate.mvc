﻿using SimpleSyndicate.Mvc.Controllers;
using SimpleSyndicate.Mvc.Tests.Models;
using SimpleSyndicate.Repositories;
using SimpleSyndicate.Testing.Repositories;

namespace SimpleSyndicate.Mvc.Tests.TestData
{
	public static class MvcRepositoriesContainerFactory
	{
		public static MvcRepositoriesContainer Create()
		{
			var container = new MvcRepositoriesContainer();
			container.SelectListItemViewModelRepository = CreateSelectListItemViewModelRepository();
			container.TestModelRepository = CreateTestModelRepository();
			container.VersionHistoryItemRepository = SimpleSyndicate.Testing.TestData.RepositoriesContainerFactory.CreateVersionHistoryItemRepository();
			return container;
		}

		public static IRepository<SelectListItemViewModel> CreateSelectListItemViewModelRepository()
		{
			var repository = new TestRepository<SelectListItemViewModel>();
			repository.Add(new SelectListItemViewModel() { Id = 1, Name = "Test 1" });
			repository.Add(new SelectListItemViewModel() { Id = 2, Name = "Test 2" });
			repository.Add(new SelectListItemViewModel() { Id = 3, Name = "Test 3" });
			return repository;
		}

		private static IRepository<TestModel> CreateTestModelRepository()
		{
			var repository = new TestRepository<TestModel>();
			repository.Add(new TestModel() { Id = 1, Name = "Test 1", Value = "Value 1" });
			repository.Add(new TestModel() { Id = 2, Name = "Test 2", Value = "Value 2" });
			repository.Add(new TestModel() { Id = 3, Name = "Test 3", Value = "Value 3" });
			return repository;
		}
	}
}
