﻿using SimpleSyndicate.Mvc.Controllers;
using SimpleSyndicate.Mvc.Tests.Models;
using SimpleSyndicate.Repositories;
using SimpleSyndicate.Testing.TestData;

namespace SimpleSyndicate.Mvc.Tests.TestData
{
	public class MvcRepositoriesContainer : RepositoriesContainer
	{
		public IRepository<TestModel> TestModelRepository { get; set; }

		public IRepository<SelectListItemViewModel> SelectListItemViewModelRepository { get; set; }
	}
}
