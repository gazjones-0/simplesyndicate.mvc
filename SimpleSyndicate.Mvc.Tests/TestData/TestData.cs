﻿namespace SimpleSyndicate.Mvc.Tests.TestData
{
	public class TestData
	{
		public TestData()
		{
			this.Repositories = MvcRepositoriesContainerFactory.Create();
		}

		public MvcRepositoriesContainer Repositories { get; set; }
	}
}
