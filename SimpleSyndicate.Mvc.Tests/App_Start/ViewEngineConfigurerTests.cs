﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.App_Start;
using SimpleSyndicate.Mvc.Web.Mvc;

namespace SimpleSyndicate.Mvc.Tests.App_Start
{
	[TestClass]
	public class ViewEngineConfigurerTests
	{
		[TestMethod]
		public void RegisterCSharpRazorViewEngineOnlyHasOneViewEngine()
		{
			// arrange
			ViewEngineConfigurer.RegisterCSharpRazorViewEngineOnly();

			// assert
			Assert.AreEqual(1, System.Web.Mvc.ViewEngines.Engines.Count);
		}

		[TestMethod]
		public void RegisterCSharpRazorViewEngineOnlyIsCSharpRazorViewEngine()
		{
			// arrange
			ViewEngineConfigurer.RegisterCSharpRazorViewEngineOnly();

			// assert
			Assert.IsInstanceOfType(System.Web.Mvc.ViewEngines.Engines[0], typeof(CSharpRazorViewEngine));
		}
	}
}
