﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.App_Start;

namespace SimpleSyndicate.Mvc.Tests.App_Start
{
	public class ViewEngineConfiguration : ViewEngineConfigurationBase
	{
	}

	[TestClass]
	public class ViewEngineConfigurationBaseTests
	{
		[TestMethod]
		[ExpectedException(typeof(ViewEnginesMustBeSetupBeforeEnablingCodeRoutingException))]
		public void EnableCodeRoutingThrowsExceptionIfViewEnginesNotSetup()
		{
			ViewEngineConfiguration.EnableCodeRouting();
		}
	}
}
