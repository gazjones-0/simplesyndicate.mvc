﻿using AutoMapper;

namespace SimpleSyndicate.Mvc.Tests.App_Start.AutoMapperMappings
{
	public static class TestModel
	{
		public static void CreateMaps()
		{
			// input -> model
			Mapper.CreateMap<Controllers.TestModels.CreateInputModel, Models.TestModel>()
				.ForMember(d => d.Id, o => o.Ignore());
			Mapper.CreateMap<Controllers.TestModels.EditInputModel, Models.TestModel>();
		}
	}
}