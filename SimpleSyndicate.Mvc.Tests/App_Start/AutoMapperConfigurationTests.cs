﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace SimpleSyndicate.Mvc.Tests.App_Start
{
	[TestClass]
	public class AutoMapperConfigurationTests
	{
		[TestMethod]
		public void ConfigurationIsValid()
		{
			// arrange
			AutoMapperConfiguration.Configure();

			// act
			AutoMapperConfiguration.CheckConfiguration();

			// assert not needed; AutoMapper will throw an exception if the configuration isn't valid
		}

		[TestMethod]
		public void TestsConfigurationIsValid()
		{
			// arrange
			AutoMapperConfiguration.Configure();

			// act
			AutoMapperConfiguration.CheckConfiguration();

			// assert not needed; AutoMapper will throw an exception if the configuration isn't valid
		}
	}
}
