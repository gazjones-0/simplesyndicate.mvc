﻿using SimpleSyndicate.Mvc.App_Start;

namespace SimpleSyndicate.Mvc.Tests.App_Start
{
	public class AutoMapperConfiguration : AutoMapperConfigurationBase
	{
		public static void Configure()
		{
			var autoMapperConfiguration = new AutoMapperConfiguration();
			autoMapperConfiguration.Setup();
		}

		protected override void SetupMappings()
		{
			AutoMapperMappings.TestModel.CreateMaps();
		}
	}
}