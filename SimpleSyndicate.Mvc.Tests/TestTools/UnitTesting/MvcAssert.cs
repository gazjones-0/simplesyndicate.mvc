﻿using System;
using System.Linq.Expressions;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Tests.Web.Mvc;

namespace SimpleSyndicate.Mvc.Tests.TestTools.UnitTesting
{
	public static class MvcAssert
	{
		public static void AllowsAnonymous<TController>(Expression<Func<TController, ViewResult>> expression)
		{
			AllowsAnonymousInternal<TController, ViewResult>(expression);
		}

		public static void DisplayNameForEquals<TModel>(string expectedValue, Expression<Func<TModel, DateTime>> expression)
		{
			DisplayNameForEqualsInternal<TModel, DateTime>(expectedValue, expression);
		}

		public static void DisplayNameForEquals<TModel>(string expectedValue, Expression<Func<TModel, int>> expression)
		{
			DisplayNameForEqualsInternal<TModel, int>(expectedValue, expression);
		}

		public static void DisplayNameForEquals<TModel>(string expectedValue, Expression<Func<TModel, string>> expression)
		{
			DisplayNameForEqualsInternal<TModel, string>(expectedValue, expression);
		}

		private static void AllowsAnonymousInternal<TController, TReturnType>(Expression<Func<TController, TReturnType>> expression)
		{
			if (!AttributeHelpers.HasAttribute(expression, typeof(AllowAnonymousAttribute)))
			{
				Assert.Fail("No AllowAnonymous attribute found");
			}
		}

		private static void DisplayNameForEqualsInternal<TModel, TType>(string expectedValue, Expression<Func<TModel, TType>> expression)
		{
			var htmlhelper = HtmlHelperFactory.Create<TModel>();
			var value = htmlhelper.DisplayNameFor(expression);
			if (value == null)
			{
				Assert.Fail("DisplayNameFor returned null string");
			}
			Assert.AreEqual(expectedValue, value.ToString());
		}
	}
}
