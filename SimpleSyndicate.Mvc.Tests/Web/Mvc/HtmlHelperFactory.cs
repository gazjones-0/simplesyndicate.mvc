﻿using System.Web.Mvc;

namespace SimpleSyndicate.Mvc.Tests.Web.Mvc
{
	public static class HtmlHelperFactory
	{
		public static HtmlHelper<TEntity> Create<TEntity>()
		{
			return new HtmlHelper<TEntity>(new ViewContext(), new TestViewDataContainer());
		}
	}
}
