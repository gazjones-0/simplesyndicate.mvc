﻿using System.Web.Mvc;

namespace SimpleSyndicate.Mvc.Tests.Web.Mvc
{
	public class TestViewDataContainer : IViewDataContainer
	{
		public TestViewDataContainer()
		{
			this.ViewData = new ViewDataDictionary();
		}

		public ViewDataDictionary ViewData { get; set; }
	}
}