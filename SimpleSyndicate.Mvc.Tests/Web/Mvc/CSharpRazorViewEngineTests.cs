﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using SimpleSyndicate.Mvc.Web.Mvc;

namespace SimpleSyndicate.Mvc.Tests.Web.Mvc
{
	[TestClass]
	public class CSharpRazorViewEngineTests
	{
		[TestMethod]
		public void OnlyHandlesCSharp()
		{
			// arrange
			var viewEngine = new CSharpRazorViewEngine();
			
			// assert
			Assert.IsTrue(this.OnlyContainsCSharpItems(viewEngine.AreaMasterLocationFormats));
			Assert.IsTrue(this.OnlyContainsCSharpItems(viewEngine.AreaPartialViewLocationFormats));
			Assert.IsTrue(this.OnlyContainsCSharpItems(viewEngine.AreaViewLocationFormats));
			Assert.IsTrue(this.OnlyContainsCSharpItems(viewEngine.FileExtensions));
			Assert.IsTrue(this.OnlyContainsCSharpItems(viewEngine.MasterLocationFormats));
			Assert.IsTrue(this.OnlyContainsCSharpItems(viewEngine.PartialViewLocationFormats));
			Assert.IsTrue(this.OnlyContainsCSharpItems(viewEngine.ViewLocationFormats));
		}

		private bool OnlyContainsCSharpItems(string[] array)
		{
			foreach (var item in array)
			{
				if (!item.EndsWith("cshtml"))
				{
					return false;
				}
			}
			return true;
		}
	}
}
