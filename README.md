# README #

SimpleSyndicate.Mvc NuGet package.

Get the package from https://www.nuget.org/packages/SimpleSyndicate.Mvc

### What is this repository for? ###

* Common functionality for ASP.Net MVC applications - configuration, generic controller, generic repository, generic version history controller, access denied attribute, account management.

### How do I get set up? ###

* See the documentation at http://gazooka_g72.bitbucket.org/SimpleSyndicate

### Reporting issues ###

* Use the tracker at https://bitbucket.org/gazooka_g72/simplesyndicate.mvc/issues?status=new&status=open
