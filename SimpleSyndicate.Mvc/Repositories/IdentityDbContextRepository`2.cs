﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Mvc.Repositories
{
	/// <summary>
	/// Concrete implementation of an <see cref="IRepository{TEntity}"/>, using an <see cref="IdentityDbContext"/> (using <see cref="Models.ApplicationUser"/>
	/// as the <see cref="IdentityUser"/> it uses) as its underlying data store.
	/// </summary>
	/// <remarks>
	/// This class is designed for direct use with an MVC application, using a model as the entity type.
	/// </remarks>
	/// <example>
	/// <code language="cs">
	/// var context = new ApplicationDbContext();
	/// var repository = new SimpleSyndicate.Mvc.Repositories.IdentityDbContextRepository&lt;SimpleSyndicate.Mvc.Models.AspNetUserLoginDetail, ApplicationDbContext&gt;(context);
	/// var count = repository.Count();
	/// </code>
	/// </example>
	/// <typeparam name="TEntity">The entity type the repository holds.</typeparam>
	/// <typeparam name="TIdentityDbContext">An <see cref="IdentityDbContext{ApplicationUser}"/> the repository uses as its data store.</typeparam>
	public class IdentityDbContextRepository<TEntity, TIdentityDbContext> : IdentityDbContextRepository<TEntity, TIdentityDbContext, Models.ApplicationUser>
		where TEntity : class
		where TIdentityDbContext : IdentityDbContext<Models.ApplicationUser>
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="IdentityDbContextRepository{TEntity, TIdentityDbContext}"/> class using the specified context.
		/// </summary>
		/// <param name="context">A <typeparamref name="TIdentityDbContext"/> object.</param>
		public IdentityDbContextRepository(TIdentityDbContext context)
			: base(context)
		{
		}

		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="IdentityDbContextRepository{TEntity, TIdentityDbContext}"/> in an unusable state.
		/// </remarks>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				// nothing to dispose
			}
			this.disposed = true;
			base.Dispose(disposing);
		}
	}
}