﻿namespace SimpleSyndicate.Mvc.Repositories
{
	/// <summary>
	/// The <see cref="Repositories"/> namespace contains a concrete repository implementation suitable for use in an MVC application
	/// and extension methods providing entity-specific functionality.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
