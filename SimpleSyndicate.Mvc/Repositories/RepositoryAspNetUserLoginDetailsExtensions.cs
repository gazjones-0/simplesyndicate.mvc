﻿using System;
using System.Linq;
using SimpleSyndicate.Mvc.Models;
using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Mvc.Repositories
{
	/// <summary>
	/// Represents <see cref="AspNetUserLoginDetail"/>-specific repository support in an application.
	/// </summary>
	/// <remarks>
	/// The <see cref="RepositoryAspNetUserLoginDetailExtensions"/> class contains methods that extend the <see cref="IRepository{AspNetUserLoginDetail}"/> class.
	/// The extension methods provide <see cref="AspNetUserLoginDetail"/>-specific functionality for working with an <see cref="AspNetUserLoginDetail"/> repository.
	/// </remarks>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login", Justification = "Login is used in existing Asp.Net class that this repository is for")]
	public static class RepositoryAspNetUserLoginDetailExtensions
	{
		/// <summary>
		/// Finds an <see cref="AspNetUserLoginDetail"/> entity with the given primary key. If no entity is found then <c>null</c> is returned.
		/// </summary>
		/// <param name="repository">The repository instance that this method extends.</param>
		/// <param name="userId">User id to find login details for.</param>
		/// <returns>The <see cref="AspNetUserLoginDetail"/> found, or <c>null</c>.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="repository"/> is <c>null</c>.</exception>
		public static AspNetUserLoginDetail Find(this IRepository<AspNetUserLoginDetail> repository, string userId)
		{
			if (repository == null)
			{
				throw new ArgumentNullException("repository");
			}
			return repository.All().Where(x => x.UserId == userId).First();
		}
	}
}