﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using AutoMapper.QueryableExtensions;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Mvc.Repositories
{
	/// <summary>
	/// Base class for a concrete implementation of an <see cref="IRepository{TEntity}"/>, using an <see cref="IdentityDbContext"/> as its underlying data store;
	/// this class allows all <typeparamref name="TIdentityUser"/> for the <typeparamref name="TIdentityDbContext"/> to be specified, but most MVC applications
	/// won't need this level of flexibility and should use <see cref="IdentityDbContextRepository{TEntity, TIdentityDbContext}"/> instead.
	/// </summary>
	/// <remarks>
	/// This class is designed for direct use with an MVC application, using a model as the entity type.
	/// </remarks>
	/// <example>
	/// <code language="cs">
	/// var context = new ApplicationDbContext();
	/// var repository = new SimpleSyndicate.Mvc.Repositories.IdentityDbContextRepository&lt;SimpleSyndicate.Mvc.Models.AspNetUserLoginDetail, IdentityDbContext, ApplicationUser&gt;(context);
	/// var count = repository.Count();
	/// </code>
	/// </example>
	/// <typeparam name="TEntity">The entity type the repository holds.</typeparam>
	/// <typeparam name="TIdentityDbContext">An <see cref="IdentityDbContext{TIdentityUser}"/> the repository uses as its data store.</typeparam>
	/// <typeparam name="TIdentityUser">An <see cref="IdentityUser"/> the data store uses.</typeparam>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes", Justification = "Number of parameters constrained by design; most users will use derived version which has two parameters")]
	public class IdentityDbContextRepository<TEntity, TIdentityDbContext, TIdentityUser> : IRepository<TEntity>
		where TEntity : class
		where TIdentityDbContext : IdentityDbContext<TIdentityUser>
		where TIdentityUser : IdentityUser
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// The underlying data store we're using.
		/// </summary>
		private TIdentityDbContext context;

		/// <summary>
		/// The entities the repository is providing access to.
		/// </summary>
		private DbSet<TEntity> entities;

		/// <summary>
		/// Initializes a new instance of the <see cref="IdentityDbContextRepository{TEntity, TIdentityDbContext, TIdentityUser}"/> class using the specified context.
		/// </summary>
		/// <param name="context">A <typeparamref name="TIdentityDbContext"/> object.</param>
		public IdentityDbContextRepository(TIdentityDbContext context)
		{
			this.context = context;
			this.entities = context.Set<TEntity>();
		}

		/// <overloads>
		/// <summary>
		/// Gets all the entities in the repository.
		/// <note type="important">
		/// It is strongly recommended that <see cref="All{TProjection}"/> be used rather than <see cref="All" /> to avoid unnecessary overhead.
		/// </note>
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Gets all the entities in the repository.
		/// <note type="important">
		/// As every field of every entity will be returned, it is strongly recommended that <see cref="All{TProjection}"/> be used instead to avoid unnecessary overhead.
		/// </note>
		/// </summary>
		/// <returns>An <see cref="IQueryable"/> of all the entities.</returns>
		public IQueryable<TEntity> All()
		{
			return this.entities;
		}

		/// <summary>
		/// Gets all the entities in the repository, projecting them to type <typeparamref name="TProjection"/>.
		/// <note type="note">
		/// Only the fields that actually get projected will be queried from the underlying data store, improving performance.
		/// </note>
		/// </summary>
		/// <typeparam name="TProjection">Type to project the entities to.</typeparam>
		/// <returns>An <see cref="IQueryable"/> of all the projected entities.</returns>
		public IQueryable<TProjection> All<TProjection>()
		{
			return this.entities.ProjectTo<TProjection>();
		}

		/// <summary>
		/// Returns the number of entities in the repository.
		/// </summary>
		/// <returns>The number of entities in the repository.</returns>
		public int Count()
		{
			return this.entities.Count();
		}

		/// <overloads>
		/// <summary>
		/// Finds an entity.
		/// <note type="important">
		/// It is strongly recommended that <see cref="Find{TProjection}"/> be used rather than <see cref="Find" /> to avoid unnecessary overhead.
		/// </note>
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Finds an entity with the given primary key. If no entity is found then <c>null</c> is returned.
		/// <note type="important">
		/// As every field of the entity will be returned, it is strongly recommended that <see cref="Find{TProjection}"/> be used instead to avoid unnecessary overhead.
		/// </note>
		/// </summary>
		/// <remarks>
		/// The <typeparamref name="TEntity"/> must have a member decorated with a <see cref="System.ComponentModel.DataAnnotations.KeyAttribute"/>.
		/// </remarks>
		/// <param name="id">Id of the entity to find.</param>
		/// <returns>The entity found, or <c>null</c>.</returns>
		public TEntity Find(int? id)
		{
			return this.entities.Find(id);
		}

		/// <summary>
		/// Finds an entity using the specified predicate, projecting it to <typeparamref name="TProjection"/>. If no entity is found then <c>null</c> is returned.
		/// <note type="note">
		/// Only the fields that actually get projected will be queried from the underlying data store, improving performance.
		/// </note>
		/// </summary>
		/// <typeparam name="TProjection">Type to project the entity to.</typeparam>
		/// <param name="predicate">Predicate used to find the entity.</param>
		/// <returns>The projected entity found, or <c>null</c>.</returns>
		public TProjection Find<TProjection>(Expression<Func<TEntity, bool>> predicate)
		{
			return this.entities.Where(predicate).ProjectTo<TProjection>().FirstOrDefault();
		}

		/// <summary>
		/// Adds an entity to the repository in such a manner that it will be inserted into the underling data store when <see cref="SaveChanges"/> is called.
		/// </summary>
		/// <param name="entity">The entity to add.</param>
		public void Add(TEntity entity)
		{
			this.entities.Add(entity);
		}

		/// <summary>
		/// Updates an entity in the repository in such a manner that the underlying data store will be updated when <see cref="SaveChanges"/> is called.
		/// </summary>
		/// <param name="entity">The entity to be updated.</param>
		public void Update(TEntity entity)
		{
			this.context.Entry(entity).State = EntityState.Modified;
		}

		/// <summary>
		/// Marks an entity in the repository in such a manner that it will be deleted from the underling data store when <see cref="SaveChanges"/> is called.
		/// </summary>
		/// <param name="entity">The entity to delete.</param>
		public void Delete(TEntity entity)
		{
			this.entities.Remove(entity);
		}

		/// <summary>
		/// Saves all changes made to the repository to the underlying data store.
		/// </summary>
		/// <returns>The number of objects written to the underlying data store.</returns>
		public int SaveChanges()
		{
			return this.context.SaveChanges();
		}

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="IdentityDbContextRepository{TEntity, TIdentityDbContext, TIdentityUser}"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="IdentityDbContextRepository{TEntity, TIdentityDbContext, TIdentityUser}"/> class.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="IdentityDbContextRepository{TEntity, TIdentityDbContext, TIdentityUser}"/> in an unusable state.
		/// </remarks>
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="IdentityDbContextRepository{TEntity, TIdentityDbContext, TIdentityUser}"/> in an unusable state.
		/// </remarks>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected virtual void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				if (this.context != null)
				{
					this.context.Dispose();
					this.context = null;
					this.entities = null;
				}
			}
			this.disposed = true;
		}
	}
}