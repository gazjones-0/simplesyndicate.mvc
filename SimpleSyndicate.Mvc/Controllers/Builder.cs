﻿using System;

namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// Base class for providing functionality for building view models and related items for controllers.
	/// </summary>
	public abstract class Builder : IDisposable
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="Builder"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="Builder"/> class.
		/// </summary>
		public void Dispose()
		{
			this.Dispose(true);
			GC.SuppressFinalize(this);
		}

		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources.
		/// </summary>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected virtual void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
			}
			this.disposed = true;
		}
	}
}