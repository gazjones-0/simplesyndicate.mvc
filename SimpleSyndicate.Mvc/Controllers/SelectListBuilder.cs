﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// Provides functionality for building <see cref="SelectList"/>s.
	/// </summary>
	public static class SelectListBuilder
	{
		/// <summary>
		/// Returns a new <see cref="SelectList"/> using the specified query for the items.
		/// </summary>
		/// <remarks>
		/// AutoMapper is used to map items from the query to the view model.
		/// </remarks>
		/// <param name="query">The list items.</param>
		/// <returns>A select list.</returns>
		public static SelectList SelectList(IQueryable<SelectListItemViewModel> query)
		{
			return new SelectList(SelectListItems(query), "Id", "Name");
		}

		/// <summary>
		/// Returns the items for a <see cref="SelectList"/> using the specified query for the items.
		/// </summary>
		/// <remarks>
		/// AutoMapper is used to map items from the query to the view model.
		/// </remarks>
		/// <param name="query">The list items.</param>
		/// <returns>A list of the items.</returns>
		public static IEnumerable<SelectListItemViewModel> SelectListItems(IQueryable<SelectListItemViewModel> query)
		{
			return AutoMapper.Mapper.Map<IEnumerable<SelectListItemViewModel>>(query);
		}
	}
}