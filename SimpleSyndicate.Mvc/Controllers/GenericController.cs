﻿using System;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using SimpleSyndicate.Mvc.Web.Mvc;
using SimpleSyndicate.Mvc.Web.Routing;

namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// Extended version of <see cref="Controller"/>.
	/// </summary>
	public abstract class GenericController : Controller
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// Returns the confirmation message that was set by a call to <see cref="SetConfirmationMessage"/>; the message will be available in
		/// both the request it was set as well as the one after, so controller methods can set the message and then redirect to another view
		/// where the message will be displayed.
		/// </summary>
		/// <returns>The confirmation message.</returns>
		/// <exception cref="InvalidOperationException">Thrown if <see cref="SetConfirmationMessage"/> has not been called.</exception>
		public ConfirmationMessageViewModel ConfirmationMessage()
		{
			var confirmationMessageViewModel = TempData[ConfirmationMessageImplementation.TempDataKeyName];
			if (confirmationMessageViewModel == null)
			{
				throw new InvalidOperationException();
			}
			return (ConfirmationMessageViewModel)confirmationMessageViewModel;
		}

		/// <overloads>
		/// <summary>
		/// Returns an instance of the <see cref="HttpStatusCodeResult"/> class that represents a bad request.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Returns an instance of the <see cref="HttpStatusCodeResult"/> class that represents a bad request.
		/// </summary>
		/// <returns>An instance of the <see cref="HttpStatusCodeResult"/> that represents a bad request.</returns>
		protected internal HttpStatusCodeResult HttpBadRequest()
		{
			return this.HttpBadRequest(null);
		}

		/// <summary>
		/// Returns an instance of the <see cref="HttpStatusCodeResult"/> class that represents a bad request.
		/// </summary>
		/// <param name="statusDescription">The status description.</param>
		/// <returns>An instance of the <see cref="HttpStatusCodeResult"/> that represents a bad request.</returns>
		protected internal virtual HttpStatusCodeResult HttpBadRequest(string statusDescription)
		{
			return new HttpStatusCodeResult(HttpStatusCode.BadRequest, statusDescription);
		}

		/// <overloads>
		/// <summary>
		/// Redirects to the specified action, including any information necessary for paging.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Redirects to the specified action using the action name, including any information necessary for paging.
		/// </summary>
		/// <param name="actionName">The name of the action.</param>
		/// <returns>The redirect result object.</returns>
		protected internal RedirectToRouteResultWithPaging RedirectToActionWithPaging(string actionName)
		{
			return this.RedirectToActionWithPaging(actionName, (RouteValueDictionary)null);
		}

		/// <summary>
		/// Redirects to the specified action using the action name and route dictionary, including any information necessary for paging.
		/// </summary>
		/// <param name="actionName">The name of the action.</param>
		/// <param name="routeValues">The parameters for a route.</param>
		/// <returns>The redirect result object.</returns>
		protected internal RedirectToRouteResultWithPaging RedirectToActionWithPaging(string actionName, RouteValueDictionary routeValues)
		{
			return this.RedirectToActionWithPaging(actionName, null, routeValues);
		}

		/// <summary>
		/// Redirects to the specified action using the action name and controller name, including any information necessary for paging.
		/// </summary>
		/// <param name="actionName">The name of the action.</param>
		/// <param name="controllerName">The name of the controller.</param>
		/// <returns>The redirect result object.</returns>
		protected internal RedirectToRouteResultWithPaging RedirectToActionWithPaging(string actionName, string controllerName)
		{
			return this.RedirectToActionWithPaging(actionName, controllerName, null);
		}

		/// <summary>
		/// Redirects to the specified action using the action name, controller name, and route values, including any information necessary for paging.
		/// </summary>
		/// <param name="actionName">The name of the action.</param>
		/// <param name="controllerName">The name of the controller.</param>
		/// <param name="routeValues">The parameters for a route.</param>
		/// <returns>The redirect result object.</returns>
		protected internal virtual RedirectToRouteResultWithPaging RedirectToActionWithPaging(string actionName, string controllerName, RouteValueDictionary routeValues)
		{
			RouteValueDictionary mergedRouteValues;

			if (this.RouteData == null)
			{
				mergedRouteValues = RouteValuesHelpers.MergeRouteValues(actionName, controllerName, null, RouteValueDictionaryWithPagingFactory.Create(this.HttpContext, routeValues), includeImplicitMvcValues: true);
			}
			else
			{
				mergedRouteValues = RouteValuesHelpers.MergeRouteValues(actionName, controllerName, this.RouteData.Values, RouteValueDictionaryWithPagingFactory.Create(this.HttpContext, routeValues), includeImplicitMvcValues: true);
			}

			return new RedirectToRouteResultWithPaging(mergedRouteValues);
		}

		/// <summary>
		/// Sets a confirmation message of the specified <see cref="ConfirmationMessageActionPerformed"/>, used to confirm an action has been performed. The
		/// message can be retrieved by <see cref="ConfirmationMessage"/>, and will be available to both this and the next request; this allows
		/// controller methods to set the message and then redirect to another view where the message will be displayed.
		/// </summary>
		/// <param name="entity">A string describing the entity that has just been acted upon.</param>
		/// <param name="type">The type of message, indicating what action has been performed.</param>
		protected internal virtual void SetConfirmationMessage(string entity, ConfirmationMessageActionPerformed type)
		{
			this.TempData[ConfirmationMessageImplementation.TempDataKeyName] = new ConfirmationMessageViewModel() { EntityDescription = entity, ActionPerformed = type };
		}

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="GenericController"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="GenericController"/> in an unusable state.
		/// </remarks>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
			}
			this.disposed = true;
			base.Dispose(disposing);
		}
	}
}
