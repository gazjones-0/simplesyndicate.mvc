﻿using System.Web.Mvc;
using SimpleSyndicate.Models;
using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Mvc.Controllers.VersionHistory
{
	/// <summary>
	/// Generic version history controller
	/// </summary>
	public class GenericVersionHistoryController : GenericController<VersionHistoryBuilder>
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="GenericVersionHistoryController"/> class using the specified repository.
		/// </summary>
		/// <param name="versionHistoryItemRepository">A <see cref="VersionHistoryItem"/> repository.</param>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Reliability", "CA2000:Dispose objects before losing scope", Justification = "Base class disposes of Builder")]
		public GenericVersionHistoryController(IRepository<VersionHistoryItem> versionHistoryItemRepository)
		{
			this.Builder = new VersionHistoryBuilder(versionHistoryItemRepository);
		}

		/// <summary>
		/// Returns the index view.
		/// </summary>
		/// <returns>The index view.</returns>
		[AllowAnonymous]
		public ViewResult Index()
		{
			return this.View(Builder.IndexViewModel());
		}

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="GenericVersionHistoryController"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="GenericVersionHistoryController"/> in an unusable state.
		/// </remarks>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				// Builder will be released by base class
			}
			this.disposed = true;
			base.Dispose(disposing);
		}
	}
}