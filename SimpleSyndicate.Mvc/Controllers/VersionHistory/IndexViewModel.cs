﻿using System.Collections.Generic;

namespace SimpleSyndicate.Mvc.Controllers.VersionHistory
{
	/// <summary>
	/// View model for the version history index view.
	/// </summary>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1710:IdentifiersShouldHaveCorrectSuffix", Justification = "The suffix ViewModel suits better as, although it's a collection, it's more intuitive.")]
	public class IndexViewModel : List<IndexItemViewModel>
	{
	}
}