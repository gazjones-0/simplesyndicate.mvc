﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SimpleSyndicate.Mvc.Controllers.VersionHistory
{
	/// <summary>
	/// View model for each item in the <see cref="IndexViewModel"/>.
	/// </summary>
	public class IndexItemViewModel
	{
		/// <summary>
		/// Gets or sets the major version number.
		/// </summary>
		/// <value>An integer holding the major version number.</value>
		[Display(Name = DataAnnotations.MajorVersion.Display.Name)]
		public int MajorVersion { get; set; }

		/// <summary>
		/// Gets or sets the minor version number.
		/// </summary>
		/// <value>An integer holding the minor version number.</value>
		[Display(Name = DataAnnotations.MinorVersion.Display.Name)]
		public int MinorVersion { get; set; }

		/// <summary>
		/// Gets or sets the point version number.
		/// </summary>
		/// <value>An integer holding the point version number.</value>
		[Display(Name = DataAnnotations.PointVersion.Display.Name)]
		public int PointVersion { get; set; }

		/// <summary>
		/// Gets or sets the release date.
		/// </summary>
		/// <value>The release date.</value>
		[Display(Name = DataAnnotations.ReleaseDate.Display.Name)]
		public DateTime ReleaseDate { get; set; }

		/// <summary>
		/// Gets or sets the release notes.
		/// </summary>
		/// <value>The release notes.</value>
		[Display(Name = DataAnnotations.ReleaseNotes.Display.Name)]
		public string ReleaseNotes { get; set; }
	}
}