﻿using System.Linq;
using AutoMapper.QueryableExtensions;
using SimpleSyndicate.Models;
using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Mvc.Controllers.VersionHistory
{
	/// <summary>
	/// Provides functionality for building view models for the <see cref="GenericVersionHistoryController"/>.
	/// </summary>
	public class VersionHistoryBuilder : Builder
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="VersionHistoryBuilder"/> class using the specified repository.
		/// </summary>
		/// <param name="versionHistoryItemRepository">A <see cref="VersionHistoryItem"/> repository.</param>
		public VersionHistoryBuilder(IRepository<VersionHistoryItem> versionHistoryItemRepository)
		{
			this.VersionHistoryItemRepository = versionHistoryItemRepository;
		}

		/// <summary>
		/// Gets or sets the version history item repository.
		/// </summary>
		private IRepository<VersionHistoryItem> VersionHistoryItemRepository { get; set; }

		/// <summary>
		/// Returns a view model holding the details of the latest version.
		/// </summary>
		/// <returns>View model holding details of the latest version.</returns>
		public CurrentVersionViewModel CurrentVersionViewModel()
		{
			return AutoMapper.Mapper.Map<CurrentVersionViewModel>(
				this.VersionHistoryItemRepository.All()
				.OrderByDescending(x => x.MajorVersion)
				.ThenByDescending(x => x.MinorVersion)
				.ThenByDescending(x => x.PointVersion)
				.ProjectTo<CurrentVersionViewModel>()
				.First());
		}

		/// <summary>
		/// Returns a view model for the index view.
		/// </summary>
		/// <returns>View model holding details of all the versions</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1002:DoNotExposeGenericLists", Justification = "IndexViewModel is not designed to be inherited")]
		public IndexViewModel IndexViewModel()
		{
			return AutoMapper.Mapper.Map<IndexViewModel>(
				this.VersionHistoryItemRepository.All()
				.OrderByDescending(x => x.MajorVersion)
				.ThenByDescending(x => x.MinorVersion)
				.ThenByDescending(x => x.PointVersion)
				.ProjectTo<IndexItemViewModel>());
		}

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="VersionHistoryBuilder"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources; if <paramref name="disposing"/> is <c>true</c>
		/// the resources passed to the constructor when this instance was created will be released.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="VersionHistoryBuilder"/> in an unusable state.
		/// </remarks>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				// repository will be disposed by dependency injection package
				this.VersionHistoryItemRepository = null;
			}
			this.disposed = true;
			base.Dispose(disposing);
		}
	}
}