﻿namespace SimpleSyndicate.Mvc.Controllers.VersionHistory
{
	/// <summary>
	/// The <see cref="VersionHistory"/> namespace contains a generic version history controller and related classes.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
