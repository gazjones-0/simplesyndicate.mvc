﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace SimpleSyndicate.Mvc.Controllers.VersionHistory
{
	/// <summary>
	/// View model that holds details of the latest version.
	/// </summary>
	public class CurrentVersionViewModel
	{
		/// <summary>
		/// Gets or sets the major version number.
		/// </summary>
		/// <value>An integer holding the major version number.</value>
		[Display(Name = DataAnnotations.MajorVersion.Display.Name)]
		public int MajorVersion { get; set; }

		/// <summary>
		/// Gets or sets the minor version number.
		/// </summary>
		/// <value>An integer holding the minor version number.</value>
		[Display(Name = DataAnnotations.MinorVersion.Display.Name)]
		public int MinorVersion { get; set; }

		/// <summary>
		/// Gets or sets the point version number.
		/// </summary>
		/// <value>An integer holding the point version number.</value>
		[Display(Name = DataAnnotations.PointVersion.Display.Name)]
		public int PointVersion { get; set; }

		/// <summary>
		/// Gets or sets the release date.
		/// </summary>
		/// <value>The release date.</value>
		[Display(Name = DataAnnotations.ReleaseDate.Display.Name)]
		public DateTime ReleaseDate { get; set; }

		/// <overloads>
		/// <summary>
		/// Returns the version as a string in the form "&lt;Major version&gt;.&lt;Minor version&gt;.&lt;Point version&gt;" with an optional prefix.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Returns the version as a string in the form "&lt;Major version&gt;.&lt;Minor version&gt;.&lt;Point version&gt;".
		/// </summary>
		/// <returns>The version as a string in the form "&lt;Major version&gt;.&lt;Minor version&gt;.&lt;Point version&gt;".</returns>
		[Display(Name = DataAnnotations.Version.Display.Name)]
		public string Version()
		{
			return this.Version(String.Empty);
		}

		/// <summary>
		/// Returns the version as a string in the form "&lt;prefix&gt;&lt;Major version&gt;.&lt;Minor version&gt;.&lt;Point version&gt;".
		/// </summary>
		/// <param name="prefix">Prefix to apply to the version.</param>
		/// <returns>The version as a string in the form "&lt;prefix&gt;&lt;Major version&gt;.&lt;Minor version&gt;.&lt;Point version&gt;".</returns>
		[Display(Name = DataAnnotations.Version.Display.Name)]
		public string Version(string prefix)
		{
			return (String.IsNullOrEmpty(prefix) ? String.Empty : prefix) + this.MajorVersion.ToString(CultureInfo.CurrentCulture) + "." + this.MinorVersion.ToString(CultureInfo.CurrentCulture) + "." + this.PointVersion.ToString(CultureInfo.CurrentCulture);
		}
	}
}