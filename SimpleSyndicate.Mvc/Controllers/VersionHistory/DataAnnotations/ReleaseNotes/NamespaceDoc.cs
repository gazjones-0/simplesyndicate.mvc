﻿namespace SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.ReleaseNotes
{
	/// <summary>
	/// The <see cref="ReleaseNotes"/> namespace contains classes that hold data annotations for the version history release notes.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
