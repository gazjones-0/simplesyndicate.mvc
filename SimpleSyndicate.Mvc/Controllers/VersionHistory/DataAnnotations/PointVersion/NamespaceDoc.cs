﻿namespace SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.PointVersion
{
	/// <summary>
	/// The <see cref="PointVersion"/> namespace contains classes that hold data annotations for the version history point version.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
