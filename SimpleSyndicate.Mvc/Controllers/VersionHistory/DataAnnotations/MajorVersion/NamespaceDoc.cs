﻿namespace SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.MajorVersion
{
	/// <summary>
	/// The <see cref="MajorVersion"/> namespace contains classes that hold data annotations for the version history major version.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
