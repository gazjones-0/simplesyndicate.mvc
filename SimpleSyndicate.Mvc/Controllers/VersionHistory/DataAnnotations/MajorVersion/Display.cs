﻿namespace SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.MajorVersion
{
	/// <summary>
	/// Display data annotations.
	/// </summary>
	public static class Display
	{
		/// <summary>
		/// Display name.
		/// </summary>
		public const string Name = "Major version";
	}
}
