﻿namespace SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.MinorVersion
{
	/// <summary>
	/// Display data annotations
	/// </summary>
	public static class Display
	{
		/// <summary>
		/// Display name.
		/// </summary>
		public const string Name = "Minor version";
	}
}
