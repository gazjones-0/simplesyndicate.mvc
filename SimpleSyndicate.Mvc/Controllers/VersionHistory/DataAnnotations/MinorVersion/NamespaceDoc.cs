﻿namespace SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.MinorVersion
{
	/// <summary>
	/// The <see cref="MinorVersion"/> namespace contains classes that hold data annotations for the version history minor version.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
