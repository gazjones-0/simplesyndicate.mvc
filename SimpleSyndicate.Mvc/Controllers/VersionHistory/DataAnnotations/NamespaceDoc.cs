﻿namespace SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations
{
	/// <summary>
	/// The <see cref="DataAnnotations"/> namespace contains classes that hold data annotations for the <see cref="VersionHistory"/> controller.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
