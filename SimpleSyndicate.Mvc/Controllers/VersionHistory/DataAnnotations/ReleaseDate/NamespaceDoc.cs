﻿namespace SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.ReleaseDate
{
	/// <summary>
	/// The <see cref="ReleaseDate"/> namespace contains classes that hold data annotations for the version history release date.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
