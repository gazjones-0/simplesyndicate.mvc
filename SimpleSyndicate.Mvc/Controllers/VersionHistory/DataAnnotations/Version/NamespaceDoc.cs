﻿namespace SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.Version
{
	/// <summary>
	/// The <see cref="Version"/> namespace contains classes that hold data annotations for the version history version.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
