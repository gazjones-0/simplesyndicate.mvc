﻿namespace SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.Version
{
	/// <summary>
	/// Display data annotations.
	/// </summary>
	public static class Display
	{
		/// <summary>
		/// Display name.
		/// </summary>
		public const string Name = "Version";
	}
}
