﻿namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// The <see cref="Controllers"/> namespace contains classes for working with, extending and providing common functionality for MVC controllers.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
