﻿using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// Generic command pattern for controllers that works with a repository, but only provides create functionality.
	/// </summary>
	/// <typeparam name="TCreateInputModel">Input model type that is used to holds details of an entity to create.</typeparam>
	/// <typeparam name="TEntity">The entity type the repository holds.</typeparam>
	public class CreateCommand<TCreateInputModel, TEntity> : Command
		where TCreateInputModel : class
		where TEntity : class
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="CreateCommand{TCreateInputModel, TEntity}"/> class using the specified repository.
		/// </summary>
		/// <param name="repository">A <typeparamref name="TEntity"/> repository.</param>
		public CreateCommand(IRepository<TEntity> repository)
		{
			this.Repository = repository;
		}

		/// <summary>
		/// Gets or sets the underlying repository being used.
		/// </summary>
		/// <value>The underlying repository being used.</value>
		protected IRepository<TEntity> Repository { get; set; }

		/// <summary>
		/// Creates a new <typeparamref name="TEntity"/> in the repository, populated using the specified <typeparamref name="TCreateInputModel"/>.
		/// The <typeparamref name="TCreateInputModel"/> is mapped to the <typeparamref name="TEntity"/> using AutoMapper.
		/// </summary>
		/// <param name="createInputModel">Input details that will be mapped to the new <typeparamref name="TEntity"/> using AutoMapper.</param>
		public virtual void SaveChanges(TCreateInputModel createInputModel)
		{
			this.Repository.Add(AutoMapper.Mapper.Map<TEntity>(createInputModel));
			this.Repository.SaveChanges();
		}

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="CreateCommand{TCreateInputModel, TEntity}"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources; if <paramref name="disposing"/> is <c>true</c>
		/// <see cref="Repository"/> will be released.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="CreateCommand{TCreateInputModel, TEntity}"/> in an unusable state.
		/// </remarks>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				if (this.Repository != null)
				{
					this.Repository.Dispose();
					this.Repository = null;
				}
			}
			this.disposed = true;
			base.Dispose(disposing);
		}
	}
}