﻿namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// View model for an item in a <see cref="System.Web.Mvc.SelectList"/>.
	/// </summary>
	public class SelectListItemViewModel
	{
		/// <summary>
		/// Gets or sets the item's id; this will be used as the data value field.
		/// </summary>
		/// <value>The id.</value>
		public int Id { get; set; }

		/// <summary>
		/// Gets or sets the item's name; this will be used as the data text field.
		/// </summary>
		/// <value>The name.</value>
		public string Name { get; set; }
	}
}