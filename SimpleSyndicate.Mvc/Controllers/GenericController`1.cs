﻿using System;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using SimpleSyndicate.Mvc.Web.Mvc;
using SimpleSyndicate.Mvc.Web.Routing;

namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// Extended version of <see cref="GenericController"/> that uses a <see cref="Builder"/> pattern for building view models and related items.
	/// </summary>
	/// <typeparam name="TBuilder">The <see cref="Builder"/> to use for providing functionality for creating view models and related items.</typeparam>
	public abstract class GenericController<TBuilder> : GenericController
		where TBuilder : Builder
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// Gets or sets the <see cref="Builder"/> providing functionality for creating view models and related items.
		/// </summary>
		/// <value>The <see cref="Builder"/> providing functionality for creating view models and related items.</value>
		protected TBuilder Builder { get; set; }

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="GenericController{TBuilder}"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources; if <paramref name="disposing"/> is <c>true</c>
		/// <see cref="Builder"/> will be released.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="GenericController"/> in an unusable state.
		/// </remarks>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				if (this.Builder != null)
				{
					this.Builder.Dispose();
					this.Builder = null;
				}
			}
			this.disposed = true;
			base.Dispose(disposing);
		}
	}
}
