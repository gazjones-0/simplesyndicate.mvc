﻿namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// View model for a confirmation message.
	/// </summary>
	public class ConfirmationMessageViewModel
	{
		/// <summary>
		/// Gets or sets action that is being confirmed as having been performed.
		/// </summary>
		/// <value>The action that is being confirmed as having been performed.</value>
		public ConfirmationMessageActionPerformed ActionPerformed { get; set; }

		/// <summary>
		/// Gets or sets a string describing the entity that has been acted upon.
		/// </summary>
		/// <value>The entity that has been acted upon.</value>
		public string EntityDescription { get; set; }
	}
}