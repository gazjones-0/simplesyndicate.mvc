﻿using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// Generic command pattern for controllers that works with a repository, but only provides create and update functionality.
	/// </summary>
	/// <typeparam name="TCreateInputModel">Input model type that is used to holds details of an entity to create.</typeparam>
	/// <typeparam name="TUpdateInputModel">Input model holding details of an entity to create.</typeparam>
	/// <typeparam name="TEntity">The entity type the repository holds.</typeparam>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes", Justification = "All type parameters are necessary.")]
	public class CreateUpdateCommand<TCreateInputModel, TUpdateInputModel, TEntity> : Command
		where TCreateInputModel : class
		where TUpdateInputModel : class
		where TEntity : class
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="CreateUpdateCommand{TCreateInputModel, TUpdateInputModel, TEntity}"/> class using the specified repository.
		/// </summary>
		/// <param name="repository">A <typeparamref name="TEntity"/> repository.</param>
		public CreateUpdateCommand(IRepository<TEntity> repository)
		{
			this.Repository = repository;
		}

		/// <summary>
		/// Gets or sets the underlying repository being used.
		/// </summary>
		/// <value>The underlying repository being used.</value>
		protected IRepository<TEntity> Repository { get; set; }

		/// <overloads>
		/// <summary>
		/// Creates or updates an entity in the repository.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Creates a new <typeparamref name="TEntity"/> in the repository, populated using the specified <typeparamref name="TCreateInputModel"/>.
		/// The <typeparamref name="TCreateInputModel"/> is mapped to the <typeparamref name="TEntity"/> using AutoMapper.
		/// </summary>
		/// <param name="createInputModel">Input details that will be mapped to the new <typeparamref name="TEntity"/> using AutoMapper.</param>
		public virtual void SaveChanges(TCreateInputModel createInputModel)
		{
			this.Repository.Add(AutoMapper.Mapper.Map<TEntity>(createInputModel));
			this.Repository.SaveChanges();
		}

		/// <summary>
		/// Updates an existing <typeparamref name="TEntity"/> in the repository, using the specified <typeparamref name="TUpdateInputModel"/>. The
		/// <typeparamref name="TUpdateInputModel"/> is mapped to the <typeparamref name="TEntity"/> using AutoMapper. The general model is to retrieve
		/// an entity from the repository into an input model, update details on the model, and then pass the updated input model in.
		/// </summary>
		/// <param name="editInputModel">Input details that will be mapped to the <typeparamref name="TEntity"/> using AutoMapper.</param>
		public virtual void SaveChanges(TUpdateInputModel editInputModel)
		{
			this.Repository.Update(AutoMapper.Mapper.Map<TEntity>(editInputModel));
			this.Repository.SaveChanges();
		}

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="CreateUpdateCommand{TCreateInputModel, TUpdateInputModel, TEntity}"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources; if <paramref name="disposing"/> is <c>true</c>
		/// <see cref="Repository"/> will be released.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="CreateUpdateCommand{TCreateInputModel, TUpdateInputModel, TEntity}"/> in an unusable state.
		/// </remarks>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				if (this.Repository != null)
				{
					this.Repository.Dispose();
					this.Repository = null;
				}
			}
			this.disposed = true;
			base.Dispose(disposing);
		}
	}
}