﻿namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// Contains details for the <see cref="ConfirmationMessageViewModel"/> implementation.
	/// </summary>
	public static class ConfirmationMessageImplementation
	{
		/// <summary>
		/// The key name that is used to store the message in <see cref="System.Web.Mvc.ControllerBase.TempData"/>.
		/// </summary>
		public const string TempDataKeyName = "SimpleSyndicate.ConfirmationMessage";
	}
}
