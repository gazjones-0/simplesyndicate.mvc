﻿using System;
using System.Net;
using System.Web.Mvc;
using System.Web.Routing;
using SimpleSyndicate.Mvc.Web.Mvc;
using SimpleSyndicate.Mvc.Web.Routing;

namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// Extended version of <see cref="GenericController{TBuilder}"/> that uses a <see cref="Command"/> pattern for storing entities.
	/// </summary>
	/// <typeparam name="TBuilder">The <see cref="Builder"/> to use for providing functionality for creating view models and related items.</typeparam>
	/// <typeparam name="TCommand">The <see cref="Command"/> to use for storing entities.</typeparam>
	public abstract class GenericController<TBuilder, TCommand> : GenericController<TBuilder>
		where TBuilder : Builder
		where TCommand : Command
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// Gets or sets the <see cref="Command"/> providing functionality for storing entities.
		/// </summary>
		/// <value>The <see cref="Command"/> providing functionality for storing entities.</value>
		protected TCommand Command { get; set; }

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="GenericController{TBuilder, TCommand}"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources; if <paramref name="disposing"/> is <c>true</c>
		/// <see cref="Command"/> will be released.
		/// </summary>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				if (this.Command != null)
				{
					this.Command.Dispose();
					this.Command = null;
				}
			}
			this.disposed = true;
			base.Dispose(disposing);
		}
	}
}
