﻿namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// The action that a <see cref="ConfirmationMessageViewModel"/> is confirming as having been performed.
	/// </summary>
	public enum ConfirmationMessageActionPerformed
	{
		/// <summary>
		/// An entity has been added.
		/// </summary>
		EntityAdded,

		/// <summary>
		/// An entity has been updated.
		/// </summary>
		EntityUpdated,

		/// <summary>
		/// An entity has been deleted.
		/// </summary>
		EntityDeleted
	}
}