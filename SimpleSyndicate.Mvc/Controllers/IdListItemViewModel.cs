﻿namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// View model for a list item that only has an id.
	/// </summary>
	public class IdListItemViewModel
	{
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
		public int Id { get; set; }
	}
}