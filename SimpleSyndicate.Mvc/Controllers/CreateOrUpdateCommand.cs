﻿using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Mvc.Controllers
{
	/// <summary>
	/// Generic command pattern for controllers that works with a repository, but only provides create or update functionality (i.e. when
	/// a change is made, if the entity doesn't already exist it is created, otherwise the existing entity is updated. This is most
	/// most commonly used when the entity is really just additional information about another entity and it's more useful to treat the
	/// additional information as always being there.
	/// </summary>
	/// <typeparam name="TCreateOrUpdateInputModel">Input model type that is used to holds details of an entity to create or update.</typeparam>
	/// <typeparam name="TEntity">The entity type the repository holds.</typeparam>
	public class CreateOrUpdateCommand<TCreateOrUpdateInputModel, TEntity> : Command
		where TCreateOrUpdateInputModel : class
		where TEntity : class
	{
		/// <summary>
		/// Whether Dispose has been called or not.
		/// </summary>
		private bool disposed = false;

		/// <summary>
		/// Initializes a new instance of the <see cref="CreateOrUpdateCommand{TCreateOrUpdateInputModel, TEntity}"/> class using the specified repository.
		/// </summary>
		/// <param name="repository">A <typeparamref name="TEntity"/> repository.</param>
		public CreateOrUpdateCommand(IRepository<TEntity> repository)
		{
			this.Repository = repository;
		}

		/// <summary>
		/// Gets or sets the underlying repository being used.
		/// </summary>
		/// <value>The underlying repository being used.</value>
		protected IRepository<TEntity> Repository { get; set; }

		/// <overloads>
		/// <summary>
		/// Creates or updates an entity in the repository.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Creates or updates an entity in the repository -- if the <paramref name="createOrUpdateInputModel"/> doesn't exist in the repository,
		/// it will be added, otherwise the existing entity will be updated, in both cases populated using the specified
		/// <typeparamref name="TCreateOrUpdateInputModel"/>. The <typeparamref name="TCreateOrUpdateInputModel"/> is mapped to the
		/// <typeparamref name="TEntity"/> using AutoMapper.
		/// </summary>
		/// <param name="createOrUpdateInputModel">Input details that will be mapped to the new <typeparamref name="TEntity"/> or existing <typeparamref name="TEntity"/> using AutoMapper.</param>
		public virtual void SaveChanges(TCreateOrUpdateInputModel createOrUpdateInputModel)
		{
			var id = SimpleSyndicate.AttributeHelpers.GetAttributeValue<int?>(createOrUpdateInputModel, typeof(System.ComponentModel.DataAnnotations.KeyAttribute));
			var existingEntity = this.Repository.Find(id);
			if (existingEntity != null)
			{
				AutoMapper.Mapper.Map<TCreateOrUpdateInputModel, TEntity>(createOrUpdateInputModel, existingEntity);
				this.Repository.Update(existingEntity);
				this.Repository.SaveChanges();
			}
			else
			{
				this.Repository.Add(AutoMapper.Mapper.Map<TEntity>(createOrUpdateInputModel));
				this.Repository.SaveChanges();
			}
		}

		/// <overloads>
		/// <summary>
		/// Releases all resources that are used by the current instance of the <see cref="CreateOrUpdateCommand{TCreateOrUpdateInputModel, TEntity}"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Releases unmanaged resources and optionally releases managed resources; if <paramref name="disposing"/> is <c>true</c>
		/// <see cref="Repository"/> will be released.
		/// </summary>
		/// <remarks>
		/// The Dispose method leaves the <see cref="CreateOrUpdateCommand{TCreateOrUpdateInputModel, TEntity}"/> in an unusable state.
		/// </remarks>
		/// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
		protected override void Dispose(bool disposing)
		{
			if (this.disposed)
			{
				return;
			}
			if (disposing)
			{
				if (this.Repository != null)
				{
					this.Repository.Dispose();
					this.Repository = null;
				}
			}
			this.disposed = true;
			base.Dispose(disposing);
		}
	}
}
