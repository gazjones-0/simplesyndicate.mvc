﻿namespace SimpleSyndicate.Mvc
{
	/// <summary>
	/// The <see cref="Mvc"/> namespace contains classes for providing common functionality for ASP.Net MVC applications.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
