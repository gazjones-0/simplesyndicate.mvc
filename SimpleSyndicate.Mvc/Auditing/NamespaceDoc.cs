﻿namespace SimpleSyndicate.Mvc.Auditing
{
	/// <summary>
	/// The <see cref="Auditing"/> namespace contains classes related to auditing entities.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
