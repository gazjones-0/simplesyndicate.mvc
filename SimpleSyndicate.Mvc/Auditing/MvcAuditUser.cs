﻿using Microsoft.AspNet.Identity;
using SimpleSyndicate.Auditing;

namespace SimpleSyndicate.Mvc.Auditing
{
	/// <summary>
	/// Provides details of the user that is saving the changes in an MVC application.
	/// </summary>
	public class MvcAuditUser : IAuditUser
	{
		/// <summary>
		/// Returns the id of the user that is saving changes.
		/// </summary>
		/// <returns>The id of the user that is saving changes, or <c>null</c> if there is no authenticated user.</returns>
		public string UserId()
		{
			var httpContext = System.Web.HttpContext.Current;
			if (httpContext != null)
			{
				var user = httpContext.User;
				if (user != null)
				{
					var identity = user.Identity;
					if (identity != null)
					{
						return identity.GetUserId();
					}
				}
			}
			return null;
		}
	}
}
