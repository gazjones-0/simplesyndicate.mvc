﻿namespace SimpleSyndicate.Mvc.Web
{
	/// <summary>
	/// The <see cref="Web"/> namespace contains classes for working with <see cref="System.Web"/>.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
