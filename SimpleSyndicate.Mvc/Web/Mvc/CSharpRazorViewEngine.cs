﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace SimpleSyndicate.Mvc.Web.Mvc
{
	/// <summary>
	/// A custom <see cref="RazorViewEngine"/> that only supports C#.
	/// </summary>
	/// <remarks>
	/// All non-C# handling is removed, which should provide improved performance.
	/// </remarks>
	public class CSharpRazorViewEngine : RazorViewEngine
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="CSharpRazorViewEngine"/> class.
		/// </summary>
		public CSharpRazorViewEngine()
			: base()
		{
			this.AreaMasterLocationFormats = RemoveNonCSharpItemsFromArray(this.AreaMasterLocationFormats);
			this.AreaPartialViewLocationFormats = RemoveNonCSharpItemsFromArray(this.AreaPartialViewLocationFormats);
			this.AreaViewLocationFormats = RemoveNonCSharpItemsFromArray(this.AreaViewLocationFormats);
			this.FileExtensions = RemoveNonCSharpItemsFromArray(this.FileExtensions);
			this.MasterLocationFormats = RemoveNonCSharpItemsFromArray(this.MasterLocationFormats);
			this.PartialViewLocationFormats = RemoveNonCSharpItemsFromArray(this.PartialViewLocationFormats);
			this.ViewLocationFormats = RemoveNonCSharpItemsFromArray(this.ViewLocationFormats);
		}

		/// <summary>
		/// Removes non-C# items from a list of view files.
		/// </summary>
		/// <param name="array">Array of view files.</param>
		/// <returns>An array that only contains C# view files.</returns>
		private static string[] RemoveNonCSharpItemsFromArray(string[] array)
		{
			var list = new List<string>();
			foreach (string item in array)
			{
				if (item.EndsWith("cshtml", StringComparison.Ordinal))
				{
					list.Add(item);
				}
			}
			return list.ToArray();
		}
	}
}
