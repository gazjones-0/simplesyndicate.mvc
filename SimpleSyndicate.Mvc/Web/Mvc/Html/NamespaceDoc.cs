﻿namespace SimpleSyndicate.Mvc.Web.Mvc.Html
{
	/// <summary>
	/// The <see cref="Html"/> namespace contains classes for working with <see cref="System.Web.Mvc.Html"/>.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
