﻿using System;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;
using SimpleSyndicate.Mvc.Web.Routing;

namespace SimpleSyndicate.Mvc.Web.Mvc.Html
{
	/// <summary>
	/// Represents support for HTML links in an application.
	/// </summary>
	/// <remarks>
	/// The <see cref="LinkExtensions"/> class contains methods that extend the <see cref="HtmlHelper"/> class. Each extension method renders an HTML anchor
	/// element (<c>a</c> element). The <c>ActionLinkWithPaging</c> method renders an element that links to an action method.
	/// </remarks>
	public static class LinkExtensions
	{
		/// <overloads>
		/// <summary>
		/// Returns an anchor element (<c>a</c> element), including any information necessary for paging.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Returns an anchor element (<c>a</c> element) for the specified link text and action, including any information necessary for paging.
		/// </summary>
		/// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
		/// <param name="linkText">The inner text of the anchor element.</param>
		/// <param name="actionName">The name of the action.</param>
		/// <returns>An anchor element (<c>a</c> element).</returns>
		public static MvcHtmlString ActionLinkWithPaging(this HtmlHelper htmlHelper, string linkText, string actionName)
		{
			return PagingHelpers.FixLink(htmlHelper.ActionLink(linkText, actionName, RouteValueDictionaryWithPagingFactory.Create()));
		}

		/// <summary>
		/// Returns an anchor element (<c>a</c> element) for the specified link text, action, and route values, including any information necessary for paging.
		/// </summary>
		/// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
		/// <param name="linkText">The inner text of the anchor element.</param>
		/// <param name="actionName">The name of the action.</param>
		/// <param name="routeValues">An object that contains the parameters for a route. The parameters are retrieved through reflection by examining the properties of the object. The object is typically created by using object initializer syntax.</param>
		/// <returns>An anchor element (<c>a</c> element).</returns>
		public static MvcHtmlString ActionLinkWithPaging(this HtmlHelper htmlHelper, string linkText, string actionName, object routeValues)
		{
			return PagingHelpers.FixLink(htmlHelper.ActionLink(linkText, actionName, RouteValueDictionaryWithPagingFactory.Create(routeValues)));
		}

		/// <summary>
		/// Returns an anchor element (<c>a</c> element) for the specified link text, action, and route values as a route value dictionary, including any information necessary for paging.
		/// </summary>
		/// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
		/// <param name="linkText">The inner text of the anchor element.</param>
		/// <param name="actionName">The name of the action.</param>
		/// <param name="routeValues">An object that contains the parameters for a route.</param>
		/// <returns>An anchor element (<c>a</c> element).</returns>
		public static MvcHtmlString ActionLinkWithPaging(this HtmlHelper htmlHelper, string linkText, string actionName, RouteValueDictionary routeValues)
		{
			return PagingHelpers.FixLink(htmlHelper.ActionLink(linkText, actionName, RouteValueDictionaryWithPagingFactory.Create(routeValues)));
		}

		/// <summary>
		/// Returns an anchor element (<c>a</c> element) for the specified link text, action, and route values as a route value dictionary, including any information necessary for paging.
		/// </summary>
		/// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
		/// <param name="linkText">The inner text of the anchor element.</param>
		/// <param name="actionName">The name of the action.</param>
		/// <param name="routeValues">An object that contains the parameters for a route. The parameters are retrieved through reflection by examining the properties of the object. The object is typically created by using object initializer syntax.</param>
		/// <param name="additionalRouteValues">An object that contains the parameters for a route.</param>
		/// <returns>An anchor element (<c>a</c> element).</returns>
		public static MvcHtmlString ActionLinkWithPaging(this HtmlHelper htmlHelper, string linkText, string actionName, object routeValues, RouteValueDictionary additionalRouteValues)
		{
			return PagingHelpers.FixLink(htmlHelper.ActionLink(linkText, actionName, RouteValueDictionaryWithPagingFactory.Create(routeValues, additionalRouteValues)));
		}

		/// <summary>
		/// Gets a URL that includes any information necessary for paging.
		/// </summary>
		/// <param name="htmlHelper">The HTML helper instance that this method extends.</param>
		/// <param name="page">The page to go to.</param>
		/// <param name="routeValues">An object that contains the parameters for a route.</param>
		/// <returns>A URL.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="htmlHelper"/> is <c>null</c>.</exception>
		public static MvcHtmlString LinkWithPaging(this HtmlHelper htmlHelper, int page, RouteValueDictionary routeValues)
		{
			if (htmlHelper == null)
			{
				throw new ArgumentNullException("htmlHelper");
			}
			var routeData = htmlHelper.ViewContext.RouteData.Values;
			var routeName = routeData["route"] != null ? routeData["route"].ToString() : null;
			var actionName = routeData["action"] != null ? routeData["action"].ToString() : null;
			var controllerName = routeData["controller"] != null ? routeData["controller"].ToString() : null;
			return new MvcHtmlString(PagingHelpers.FixUrl(UrlHelper.GenerateUrl(routeName, actionName, controllerName, RouteValueDictionaryWithPagingFactory.Create(routeValues, page), htmlHelper.RouteCollection, htmlHelper.ViewContext.RequestContext, false)));
		}
	}
}