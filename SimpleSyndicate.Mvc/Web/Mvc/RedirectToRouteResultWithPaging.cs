﻿using System;
using System.Web.Mvc;
using System.Web.Routing;

namespace SimpleSyndicate.Mvc.Web.Mvc
{
	/// <summary>
	/// Represents a result that performs a redirection by using the specified route values dictionary, and that works with paging.
	/// </summary>
	public class RedirectToRouteResultWithPaging : RedirectToRouteResult
	{
		/// <summary>
		/// Route collection.
		/// </summary>
		private RouteCollection routes;

		/// <overloads>
		/// <summary>
		/// Initializes a new instance of the <see cref="RedirectToRouteResultWithPaging"/> class.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Initializes a new instance of the <see cref="RedirectToRouteResultWithPaging"/> class by using the specified route values.
		/// </summary>
		/// <param name="routeValues">The route values.</param>
		public RedirectToRouteResultWithPaging(RouteValueDictionary routeValues)
			: base(routeValues)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RedirectToRouteResultWithPaging"/> class by using the specified route name and route values.
		/// </summary>
		/// <param name="routeName">The name of the route.</param>
		/// <param name="routeValues">The route values.</param>
		public RedirectToRouteResultWithPaging(string routeName, RouteValueDictionary routeValues)
			: base(routeName, routeValues)
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="RedirectToRouteResultWithPaging"/> class by using the specified route name, route values, and permanent-redirection flag.
		/// </summary>
		/// <param name="routeName">The name of the route.</param>
		/// <param name="routeValues">The route values.</param>
		/// <param name="permanent">A value that indicates whether the redirection should be permanent.</param>
		public RedirectToRouteResultWithPaging(string routeName, RouteValueDictionary routeValues, bool permanent)
			: base(routeName, routeValues, permanent)
		{
		}

		/// <summary>
		/// Gets the current routes for the application.
		/// </summary>
		/// <value>An object that contains all the current routes for the application.</value>
		internal RouteCollection Routes
		{
			get
			{
				if (this.routes == null)
				{
					this.routes = RouteTable.Routes;
				}
				return this.routes;
			}
		}

		/// <summary>
		/// Enables processing of the result of an action method by a custom type that inherits from the <see cref="ActionResult"/> class.
		/// </summary>
		/// <param name="context">The context within which the result is executed.</param>
		public override void ExecuteResult(ControllerContext context)
		{
			if (context == null)
			{
				throw new ArgumentNullException("context");
			}
			if (context.IsChildAction)
			{
				throw new InvalidOperationException("Child actions are not allowed to perform redirect actions.");
			}

			string destinationUrl = UrlHelper.GenerateUrl(RouteName, null /* actionName */, null /* controllerName */, RouteValues, this.Routes, context.RequestContext, false /* includeImplicitMvcValues */);
			if (String.IsNullOrEmpty(destinationUrl))
			{
				throw new InvalidOperationException("No route in the route table matches the supplied values.");
			}

			context.Controller.TempData.Keep();

			if (this.Permanent)
			{
				context.HttpContext.Response.RedirectPermanent(PagingHelpers.FixUrlNoEncoding(destinationUrl).ToString(), endResponse: false);
			}
			else
			{
				context.HttpContext.Response.Redirect(PagingHelpers.FixUrlNoEncoding(destinationUrl).ToString(), endResponse: false);
			}
		}
	}
}