﻿using System;
using System.Web;
using System.Web.Mvc;

namespace SimpleSyndicate.Mvc.Web.Mvc
{
	/// <summary>
	/// Helper methods for working with paging.
	/// </summary>
	public static class PagingHelpers
	{
		/// <overloads>
		/// <summary>
		/// Fixes an anchor element that contains paging information so that the link will work correctly.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Fixes an anchor element that contains paging information so that the link will work correctly.
		/// </summary>
		/// <remarks>
		/// Some of the paging information in the link might include commas (e.g. a filter that is filtering multiple fields),
		/// and, as a parameter gets encoded, the grid library doesn't pick up the filtering correctly; to get around
		/// this, we change the single parameter into multiple parameters (see the code for the full details).
		/// </remarks>
		/// <param name="link">The anchor element to fix.</param>
		/// <returns>Fixed anchor element.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="link"/> is <c>null</c>.</exception>
		public static MvcHtmlString FixLink(MvcHtmlString link)
		{
			if (link == null)
			{
				throw new ArgumentNullException("link");
			}
			return new MvcHtmlString(FixLink(link.ToString(), "&amp;"));
		}

		/// <summary>
		/// Fixes an anchor element that contains paging information so that the link will work correctly.
		/// </summary>
		/// <remarks>
		/// Some of the paging information in the link might include commas (e.g. a filter that is filtering multiple fields),
		/// and, as a parameter gets encoded, the grid library doesn't pick up the filtering correctly; to get around
		/// this, we change the single parameter into multiple parameters (see the code for the full details).
		/// </remarks>
		/// <param name="link">The anchor element to fix.</param>
		/// <returns>Fixed anchor element.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="link"/> is <c>null</c>.</exception>
		public static MvcHtmlString FixLink(string link)
		{
			if (link == null)
			{
				throw new ArgumentNullException("link");
			}
			return new MvcHtmlString(FixLink(link, "&amp;"));
		}

		/// <summary>
		/// Fixes a URL that contains paging information so that the URL will work correctly; it is expected that the
		/// URL will come from <see cref="UrlHelper"/> and the fixed URL will just be rendered.
		/// </summary>
		/// <remarks>
		/// Some of the paging information in the URL might include commas (e.g. a filter that is filtering multiple fields),
		/// and, as a parameter gets encoded, the grid library doesn't pick up the filtering correctly; to get around
		/// this, we change the single parameter into multiple parameters (see the code for the full details).
		/// </remarks>
		/// <param name="url">The URL to fix.</param>
		/// <returns>Fixed URL.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="url"/> is <c>null</c>.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "0#", Justification = "Expected to use output from UrlHelper which is a string.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1055:UriReturnValuesShouldNotBeStrings", Justification = "As the return value is only intended to be used for rendering, string return value is more appropriate.")]
		public static string FixUrl(string url)
		{
			if (url == null)
			{
				throw new ArgumentNullException("url");
			}
			return FixLink(url, "&amp;");
		}

		/// <summary>
		/// Fixes a URL that contains paging information so that the URL will work correctly, but doesn't encode any
		/// ampersand characters it adds to the URL; it is expected that the URL will come from
		/// <see cref="UrlHelper"/> and the fixed URL will just be rendered.
		/// </summary>
		/// <remarks>
		/// Some of the paging information in the URL might include commas (e.g. a filter that is filtering multiple fields),
		/// and, as a parameter gets encoded, the grid library doesn't pick up the filtering correctly; to get around
		/// this, we change the single parameter into multiple parameters (see the code for the full details).
		/// </remarks>
		/// <param name="url">The URL to fix.</param>
		/// <returns>Fixed URL.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="url"/> is <c>null</c>.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1054:UriParametersShouldNotBeStrings", MessageId = "0#", Justification = "Expected to use output from UrlHelper which is a string.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1055:UriReturnValuesShouldNotBeStrings", Justification = "As the return value is only intended to be used for rendering, string return value is more appropriate.")]
		public static string FixUrlNoEncoding(string url)
		{
			if (url == null)
			{
				throw new ArgumentNullException("url");
			}
			return FixLink(url, "&");
		}

		/// <summary>
		/// Fixes a link so that paging will work correctly.
		/// </summary>
		/// <param name="url">The URL to fix.</param>
		/// <param name="ampersandReplacement">String to replace ampersand characters with.</param>
		/// <returns>Fixed link.</returns>
		private static string FixLink(string url, string ampersandReplacement)
		{
			// the grid-filter parameter might be of the form "grid-filter=Column1,Column2", but the problem is that this will
			// get encoded to "grid-filter=Column1%2cColumn2" and the grid library doesn't handle the encoding; to get around
			// this, we change it to be "grid-filter=Column1&grid-filter=Column2" which will work as when the multiple parameters
			// are submitted in a URL they'll get merged to "grid-filter=Column1,Column2"

			// grid-filter paramater is the only one that might include commas, so see if it's in the URL
			int i = url.IndexOf("?grid-filter=", StringComparison.OrdinalIgnoreCase);
			if (i < 0)
			{
				// not first parameter, but it might be somewhere else in the URL
				i = url.IndexOf("&grid-filter=", StringComparison.OrdinalIgnoreCase);
			}
			if (i < 0)
			{
				// not at start, or in the URL, but the ampersand separator might have been encoded so check for that
				i = url.IndexOf("&amp;grid-filter=", StringComparison.OrdinalIgnoreCase);
			}
			if (i >= 0)
			{
				// grid-filter parameter exists, so split the URL into before the parameter (pre), the parameter (gridFilter) and anything after it (post)
				string pre = url.Substring(0, i + 1);
				string gridFilter = url.Substring(i + 1);
				string post = String.Empty;
				int j = gridFilter.IndexOf('&');
				if (j > 0)
				{
					post = gridFilter.Substring(j);
					gridFilter = gridFilter.Substring(0, j);
				}

				// return the adjusted URL
				gridFilter = gridFilter.ReplaceIgnoreCase(HttpUtility.UrlEncode(","), ampersandReplacement + "grid-filter=");
				return pre + gridFilter + post;
			}
			return url;
		}
	}
}