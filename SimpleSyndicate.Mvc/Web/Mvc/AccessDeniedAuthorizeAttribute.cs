﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.Web.Mvc;

namespace SimpleSyndicate.Mvc.Web.Mvc
{
	/// <summary>
	/// An extended version of <see cref="AuthorizeAttribute"/> that specifies that access to a controller or action method
	/// is restricted to users who meet the authorization requirement. If the user has been authenticated, but doesn't have
	/// access it redirects them to an access denied page (~/Account/AccessDenied) rather than the login page.
	/// </summary>
	[SuppressMessage("Microsoft.Performance", "CA1813:AvoidUnsealedAttributes", Justification = "Unsealed so that subclassed types can set properties in the default constructor or override our behavior.")]
	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = true)]
	public class AccessDeniedAuthorizeAttribute : AuthorizeAttribute
	{
		/// <summary>
		/// Called when a process requests authorization.
		/// </summary>
		/// <param name="filterContext">The filter context, which encapsulates information for using <see cref="AccessDeniedAuthorizeAttribute"/>.</param>
		/// <exception cref="System.ArgumentNullException">The <paramref name="filterContext"/> parameter is <c>null</c>.</exception>
		public override void OnAuthorization(AuthorizationContext filterContext)
		{
			if (filterContext == null)
			{
				throw new ArgumentNullException("filterContext");
			}
			base.OnAuthorization(filterContext);

			// if they're not authenticated, we want to keep the default behaviour so they can log in
			if (filterContext.HttpContext.User.Identity.IsAuthenticated)
			{
				// authenticated so if they're not authorised send them to the access denied page
				if (filterContext.Result is HttpUnauthorizedResult)
				{
					filterContext.Result = new RedirectResult("~/Account/AccessDenied");
				}
			}
		}
	}
}