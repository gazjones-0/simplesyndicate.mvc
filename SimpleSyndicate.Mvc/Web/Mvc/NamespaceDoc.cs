﻿namespace SimpleSyndicate.Mvc.Web.Mvc
{
	/// <summary>
	/// The <see cref="Mvc"/> namespace contains classes for working with <see cref="System.Web.Mvc"/>.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
