﻿using System;
using System.Web.Mvc;
using SimpleInjector;
using SimpleSyndicate.Models;
using SimpleSyndicate.Repositories;
	
namespace SimpleSyndicate.Mvc.Web.Mvc
{
	/// <summary>
	/// MVC filter that populates the <see cref="ControllerBase.ViewBag"/> with the details of the application's current version;
	/// the details are recorded in a key called "CurrentVersion" that is of type <see cref="VersionHistoryItem"/>.
	/// </summary>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Performance", "CA1813:AvoidUnsealedAttributes", Justification = "Users may want to derive from it.")]
	[AttributeUsageAttribute(AttributeTargets.Class | AttributeTargets.Method, Inherited = true, AllowMultiple = false)]
	public class VersionHistoryActionFilterAttribute : ActionFilterAttribute
	{
		/// <summary>
		/// Gets or sets the Simple Injector container to retrieve any required instances from.
		/// </summary>
		/// <value>Simple Injector container.</value>
		public Container Container { get; set; }

		/// <summary>
		/// Stores the current version in the <see cref="ControllerBase.ViewBag"/> under the "CurrentVersion" key; the type is
		/// <see cref="VersionHistoryItem"/>.
		/// </summary>
		/// <param name="filterContext">The current context.</param>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="filterContext"/> is <c>null</c>.</exception>
		public override void OnResultExecuting(ResultExecutingContext filterContext)
		{
			if (filterContext == null)
			{
				throw new ArgumentNullException("filterContext");
			}
			var builder = new Controllers.VersionHistory.VersionHistoryBuilder(Container.GetInstance<IRepository<VersionHistoryItem>>());
			try
			{
				filterContext.Controller.ViewBag.CurrentVersion = builder.CurrentVersionViewModel();
				base.OnResultExecuting(filterContext);
			}
			finally
			{
				builder.Dispose();
			}
		}
	}
}