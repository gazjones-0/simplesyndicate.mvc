﻿using System.Collections.Generic;
using System.Web.Routing;

namespace SimpleSyndicate.Mvc.Web.Mvc
{
	/// <summary>
	/// Direct copy of System.Web.Mvc.RouteValuesHelpers, which has functionality we need, but which is marked internal.
	/// </summary>
	public static class RouteValuesHelpers
	{
		/// <summary>
		/// Creates a new <see cref="RouteValueDictionary"/> instance, populated from an existing <see cref="RouteValueDictionary"/>.
		/// </summary>
		/// <param name="routeValues">A <see cref="RouteValueDictionary"/> to populate the new instance with; if this is <c>null</c> an empty <see cref="RouteValueDictionary"/> is returned.</param>
		/// <returns>A new <see cref="RouteValueDictionary"/> instance.</returns>
		public static RouteValueDictionary GetRouteValues(RouteValueDictionary routeValues)
		{
			return (routeValues != null) ? new RouteValueDictionary(routeValues) : new RouteValueDictionary();
		}

		/// <summary>
		/// Gets a <see cref="RouteValueDictionary"/> containing various routing information merged together.
		/// </summary>
		/// <param name="actionName">The action name.</param>
		/// <param name="controllerName">The controller name.</param>
		/// <param name="implicitRouteValues">A <see cref="RouteValueDictionary"/> of route values for implicit MVC routing information.</param>
		/// <param name="routeValues">A <see cref="RouteValueDictionary"/> of route values.</param>
		/// <param name="includeImplicitMvcValues">Whether to include implicit MVC routing information.</param>
		/// <returns>A <see cref="RouteValueDictionary"/> containing all the routing information merged together.</returns>
		public static RouteValueDictionary MergeRouteValues(string actionName, string controllerName, RouteValueDictionary implicitRouteValues, RouteValueDictionary routeValues, bool includeImplicitMvcValues)
		{
			// Create a new dictionary containing implicit and auto-generated values
			RouteValueDictionary mergedRouteValues = new RouteValueDictionary();

			if (includeImplicitMvcValues)
			{
				// We only include MVC-specific values like 'controller' and 'action' if we are generating an action link.
				// If we are generating a route link [as to MapRoute("Foo", "any/url", new { controller = ... })], including
				// the current controller name will cause the route match to fail if the current controller is not the same
				// as the destination controller.
				object implicitValue;
				if (implicitRouteValues != null && implicitRouteValues.TryGetValue("action", out implicitValue))
				{
					mergedRouteValues["action"] = implicitValue;
				}

				if (implicitRouteValues != null && implicitRouteValues.TryGetValue("controller", out implicitValue))
				{
					mergedRouteValues["controller"] = implicitValue;
				}
			}

			// Merge values from the user's dictionary/object
			if (routeValues != null)
			{
				foreach (KeyValuePair<string, object> routeElement in GetRouteValues(routeValues))
				{
					mergedRouteValues[routeElement.Key] = routeElement.Value;
				}
			}

			// Merge explicit parameters when not null
			if (actionName != null)
			{
				mergedRouteValues["action"] = actionName;
			}

			if (controllerName != null)
			{
				mergedRouteValues["controller"] = controllerName;
			}

			return mergedRouteValues;
		}
	}
}