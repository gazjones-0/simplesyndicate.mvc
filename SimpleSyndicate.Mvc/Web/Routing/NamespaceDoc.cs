﻿namespace SimpleSyndicate.Mvc.Web.Routing
{
	/// <summary>
	/// The <see cref="Routing"/> namespace contains classes for working with <see cref="System.Web.Routing"/>.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
