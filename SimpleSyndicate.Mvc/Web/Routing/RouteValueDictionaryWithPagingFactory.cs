﻿using System;
using System.Web.Routing;

namespace SimpleSyndicate.Mvc.Web.Routing
{
	/// <summary>
	/// Factory class for creating <see cref="RouteValueDictionary"/> instances that includes paging support.
	/// </summary>
	public static class RouteValueDictionaryWithPagingFactory
	{
		/// <overloads>
		/// <summary>
		/// Creates a new <see cref="RouteValueDictionary"/> that includes paging support.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Creates a new <see cref="RouteValueDictionary"/> that includes paging support.
		/// </summary>
		/// <returns>A set of route values.</returns>
		public static RouteValueDictionary Create()
		{
			var routeValues = new RouteValueDictionary();
			return Create(routeValues, null, null);
		}

		/// <summary>
		/// Creates a new <see cref="RouteValueDictionary"/> using the specified route values, that includes paging support.
		/// </summary>
		/// <param name="routeValues">An object that contains the parameters for a route. The parameters are retrieved through reflection by examining the properties of the object. The object is typically created by using object initializer syntax.</param>
		/// <returns>A set of route values.</returns>
		public static RouteValueDictionary Create(object routeValues)
		{
			if (routeValues != null)
			{
				return Create(new RouteValueDictionary(routeValues), null, null);
			}
			return Create(new RouteValueDictionary(), null, null);
		}

		/// <summary>
		/// Creates a new <see cref="RouteValueDictionary"/> using the specified route values, that includes paging support.
		/// </summary>
		/// <param name="routeValues">The route values</param>
		/// <returns>A set of route values.</returns>
		public static RouteValueDictionary Create(RouteValueDictionary routeValues)
		{
			if (routeValues != null)
			{
				return Create(new RouteValueDictionary(routeValues), null, null);
			}
			return Create(new RouteValueDictionary(), null, null);
		}

		/// <summary>
		/// Creates a new <see cref="RouteValueDictionary"/> using the specified route values, that includes paging support and goes to the specified page.
		/// </summary>
		/// <param name="routeValues">The route values</param>
		/// <param name="page">The page to go to.</param>
		/// <returns>A set of route values.</returns>
		public static RouteValueDictionary Create(RouteValueDictionary routeValues, int page)
		{
			if (routeValues != null)
			{
				return Create(new RouteValueDictionary(routeValues), null, page);
			}
			return Create(new RouteValueDictionary(), null, page);
		}

		/// <summary>
		/// Creates a new <see cref="RouteValueDictionary"/> using the specified route values, that includes paging support.
		/// </summary>
		/// <param name="routeValues">An object that contains the parameters for a route. The parameters are retrieved through reflection by examining the properties of the object. The object is typically created by using object initializer syntax.</param>
		/// <param name="additionalRouteValues">Additional route values.</param>
		/// <returns>A set of route values.</returns>
		public static RouteValueDictionary Create(object routeValues, RouteValueDictionary additionalRouteValues)
		{
			if ((routeValues != null) && (additionalRouteValues != null))
			{
				return Create(new RouteValueDictionary(routeValues), additionalRouteValues, null);
			}
			if (routeValues != null)
			{
				return Create(new RouteValueDictionary(routeValues), null, null);
			}
			if (additionalRouteValues != null)
			{
				return Create(new RouteValueDictionary(additionalRouteValues), null, null);
			}
			return Create(new RouteValueDictionary(), null, null);
		}

		/// <summary>
		/// Returns the specified <paramref name="routeValues"/> with any <paramref name="additionalRouteValues"/> added to it, plus
		/// any parameters required for paging, optionally setting the page to the one specified.
		/// </summary>
		/// <param name="routeValues">Route values to update.</param>
		/// <param name="additionalRouteValues">Additional route values to add to the <paramref name="routeValues"/>.</param>
		/// <param name="page">Page to route to, or <c>null</c> to not change it.</param>
		/// <returns>An updated set of route values.</returns>
		private static RouteValueDictionary Create(RouteValueDictionary routeValues, RouteValueDictionary additionalRouteValues, int? page)
		{
			// we'll return the passed in routeValues, but updated, so add everything from the additional route values
			if (additionalRouteValues != null)
			{
				foreach (var route in additionalRouteValues)
				{
					routeValues.Add(route.Key, route.Value);
				}
			}

			// add any paging parameters from the current request into our route value 
			var queryString = System.Web.HttpContext.Current.Request.QueryString;
			var gridColumn = queryString["grid-column"];
			if (!String.IsNullOrEmpty(gridColumn))
			{
				routeValues.Add("grid-column", gridColumn);
			}

			var gridDir = queryString["grid-dir"];
			if (!String.IsNullOrEmpty(gridDir))
			{
				routeValues.Add("grid-dir", gridDir);
			}

			var gridFilter = queryString["grid-filter"];
			if (!String.IsNullOrEmpty(gridFilter))
			{
				routeValues.Add("grid-filter", gridFilter);
			}

			// if there's no page specified, just use the current one, otherwise add in the one specified
			if (page == null)
			{
				var gridPage = queryString["grid-page"];
				if (!String.IsNullOrEmpty(gridPage))
				{
					routeValues.Add("grid-page", gridPage);
				}
			}
			else
			{
				routeValues.Add("grid-page", page.ToString());
			}
			return routeValues;
		}
	}
}