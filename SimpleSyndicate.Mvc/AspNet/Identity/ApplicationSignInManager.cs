﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SimpleSyndicate.Mvc.Models;

namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// <para>
	/// Manages sign in operations for users of type <see cref="ApplicationUser"/>, suitable for most MVC applications.
	/// </para>
	/// <para>
	/// If you want to use your own <see cref="ApplicationUser"/> type (e.g. the one created automatically in a new MVC project) then use <see cref="ApplicationSignInManager{TApplicationUser, TApplicationUserManager}"/>.
	/// </para>
	/// </summary>
	/// <summary>
	/// </summary>
	public class ApplicationSignInManager : ApplicationSignInManager<ApplicationUser, ApplicationUserManager>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ApplicationSignInManager"/> class using the specified <paramref name="userManager"/> and <paramref name="authenticationManager"/>.
		/// </summary>
		/// <param name="userManager">The user manager to use.</param>
		/// <param name="authenticationManager">The authentication manager to use.</param>
		public ApplicationSignInManager(ApplicationUserManager userManager, IAuthenticationManager authenticationManager)
			: base(userManager, authenticationManager)
		{
		}
	}
}