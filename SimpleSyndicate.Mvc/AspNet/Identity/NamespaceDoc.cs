﻿namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// The <see cref="Identity"/> namespace contains classes that work with the <see cref="Microsoft.AspNet.Identity"/> namespace.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
