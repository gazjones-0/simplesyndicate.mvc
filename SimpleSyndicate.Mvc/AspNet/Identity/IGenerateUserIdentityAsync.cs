﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// Interface for generating user identity claims for MVC users; this interface is required for <see cref="SimpleSyndicate.Mvc.App_Start.AuthenticationConfigurer{TApplicationUser, TApplicationUserManager}"/>
	/// to work as it needs to be able to perform these tasks. If you're using a custom <see cref="IdentityUser"/>-derived class that isn't derived from
	/// <see cref="SimpleSyndicate.Mvc.Models.ApplicationUser"/> and want to use <see cref="SimpleSyndicate.Mvc.App_Start.AuthenticationConfigurer{TApplicationUser, TApplicationUserManager}"/>,
	/// your class will need to implement this interface.
	/// </summary>
	/// <typeparam name="TApplicationUser">An <see cref="IdentityUser"/>-derived type; a standard one suitable for MVC applications is <see cref="SimpleSyndicate.Mvc.Models.ApplicationUser"/>.</typeparam>
	public interface IGenerateUserIdentityAsync<TApplicationUser>
		where TApplicationUser : IdentityUser
	{
		/// <overloads>
		/// <summary>
		/// Creates a <see cref="ClaimsIdentity"/> representing the user.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Creates a <see cref="ClaimsIdentity"/> representing the user, using <see cref="DefaultAuthenticationTypes.ApplicationCookie"/> as the authentication type.
		/// </summary>
		/// <param name="userManager">A <see cref="UserManager{TUser}"/> object that provides access to the user store.</param>
		/// <returns>A <see cref="ClaimsIdentity"/> representing the user.</returns>
		Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<TApplicationUser> userManager);

		/// <summary>
		/// Creates a <see cref="ClaimsIdentity"/> representing the user.
		/// </summary>
		/// <param name="userManager">A <see cref="UserManager{TUser}"/> object that provides access to the user store.</param>
		/// <param name="authenticationType">The authentication type; this must match the one defined in <see cref="Microsoft.Owin.Security.Cookies.CookieAuthenticationOptions"/>.</param>
		/// <returns>A <see cref="ClaimsIdentity"/> representing the user.</returns>
		Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<TApplicationUser> userManager, string authenticationType);
	}
}