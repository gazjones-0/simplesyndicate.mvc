﻿namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// Stores configuration details for <see cref="Microsoft.AspNet.Identity.PasswordValidator"/>s; used by <see cref="IdentityConfiguration"/>.
	/// </summary>
	public class PasswordValidatorConfiguration
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="PasswordValidatorConfiguration"/> class, with <see cref="RequiredLength"/> set to 6,
		/// <see cref="RequireNonLetterOrDigit"/> set to <c>true</c>, <see cref="RequireDigit"/> set to <c>true</c>,
		/// <see cref="RequireLowercase"/> set to <c>true</c> and <see cref="RequireUppercase"/> set to <c>true</c>.
		/// </summary>
		public PasswordValidatorConfiguration()
		{
			this.RequiredLength = 6;
			this.RequireNonLetterOrDigit = true;
			this.RequireDigit = true;
			this.RequireLowercase = true;
			this.RequireUppercase = true;
		}

		/// <summary>
		/// Gets or sets the minimum required length of passwords; new instances of <see cref="PasswordValidatorConfiguration"/> have this set to 6.
		/// </summary>
		/// <value>Minimum required length of passwords.</value>
		public int RequiredLength { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to require a non-letter or digit character (e.g. an '!' character) in passwords; new instances of <see cref="PasswordValidatorConfiguration"/> have this set to <c>true</c>.
		/// </summary>
		/// <value><c>true</c> if a non-letter or digit character is required in passwords; <c>false</c> otherwise.</value>
		public bool RequireNonLetterOrDigit { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to require a digit ('0' - '9') character in passwords; new instances of <see cref="PasswordValidatorConfiguration"/> have this set to <c>true</c>.
		/// </summary>
		/// <value><c>true</c> if a digit character is required in passwords; <c>false</c> otherwise.</value>
		public bool RequireDigit { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to require a lower case ('a' - 'z') letter in passwords; new instances of <see cref="PasswordValidatorConfiguration"/> have this set to <c>true</c>.
		/// </summary>
		/// <value><c>true</c> if a lower case character is required in passwords; <c>false</c> otherwise.</value>
		public bool RequireLowercase { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to require an upper case ('A' - 'Z') letter in passwords; new instances of <see cref="PasswordValidatorConfiguration"/> have this set to <c>true</c>.
		/// </summary>
		/// <value><c>true</c> if an upper case character is required in passwords; <c>false</c> otherwise.</value>
		public bool RequireUppercase { get; set; }
	}
}