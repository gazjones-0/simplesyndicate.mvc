﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSyndicate.Mvc.Models;

namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// <para>
	/// Base user manager class allowing the user type to be configured; most MVC applications won't need this level of flexibility and should use
	/// <see cref="ApplicationUserManager"/> instead.
	/// </para>
	/// <para>
	/// If you want to use your own <typeparamref name="TApplicationUser"/> type (e.g. the one created automatically in a new MVC project) then derive
	/// from this class using the appropriate user type.
	/// </para>
	/// </summary>
	/// <typeparam name="TApplicationUser">A <see cref="IdentityUser"/>-derived type.</typeparam>
	/// <remarks>
	/// Our general approach is to use the <see cref="ApplicationUser"/> from within SimpleSyndicate.Mvc, rather than the one provided by a new MVC project,
	/// and let SimpleSyndicate.Mvc do as much of the work as possible.
	/// </remarks>
	public abstract class ApplicationUserManager<TApplicationUser> : UserManager<TApplicationUser>
		where TApplicationUser : IdentityUser
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ApplicationUserManager{TApplicationUser}"/> class using the specified backing store.
		/// </summary>
		/// <param name="store">Backing store.</param>
		protected ApplicationUserManager(IUserStore<TApplicationUser> store)
			: base(store)
		{
		}
	}
}