﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SimpleSyndicate.Mvc.Models;

namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// <para>
	/// Base class for managing sign in operations that allows the user type to be configured; most MVC applications won't need this level of flexibility and should use
	/// <see cref="ApplicationSignInManager"/> instead.
	/// </para>
	/// <para>
	/// If you want to use your own <typeparamref name="TApplicationUser"/> type (e.g. the one created automatically in a new MVC project) then derive
	/// from this class using the appropriate user type.
	/// </para>
	/// </summary>
	/// <typeparam name="TApplicationUser">A <see cref="IdentityUser"/>-derived type.</typeparam>
	/// <typeparam name="TApplicationUserManager">A <see cref="UserManager{TApplicationUser}"/>-derived type.</typeparam>
	/// <remarks>
	/// Our general approach is to use the <see cref="ApplicationUser"/> from within SimpleSyndicate.Mvc, rather than the one provided by a new MVC project,
	/// and let SimpleSyndicate.Mvc do as much of the work as possible.
	/// </remarks>
	public abstract class ApplicationSignInManager<TApplicationUser, TApplicationUserManager> : SignInManager<TApplicationUser, string>
		where TApplicationUser : IdentityUser, IGenerateUserIdentityAsync<TApplicationUser>
		where TApplicationUserManager : UserManager<TApplicationUser>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ApplicationSignInManager{TApplicationUser, TApplicationUserManager}"/> class using the specified <paramref name="userManager"/> and <paramref name="authenticationManager"/>.
		/// </summary>
		/// <param name="userManager">The user manager to use.</param>
		/// <param name="authenticationManager">The authentication manager to use.</param>
		protected ApplicationSignInManager(TApplicationUserManager userManager, IAuthenticationManager authenticationManager)
			: base(userManager, authenticationManager)
		{
		}

		/// <summary>
		/// Generates a <see cref="ClaimsIdentity"/> for the specified <paramref name="user"/>.
		/// </summary>
		/// <param name="user">The <see cref="ApplicationUser"/> to generate a <see cref="ClaimsIdentity"/> for.</param>
		/// <returns>A <see cref="ClaimsIdentity"/> for the user.</returns>
		public override Task<ClaimsIdentity> CreateUserIdentityAsync(TApplicationUser user)
		{
			if (user == null)
			{
				throw new ArgumentNullException("user");
			}
			return user.GenerateUserIdentityAsync((TApplicationUserManager)UserManager);
		}
	}
}