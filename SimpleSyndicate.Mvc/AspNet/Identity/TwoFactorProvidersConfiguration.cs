﻿namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// Stores configuration details for <see cref="Microsoft.AspNet.Identity"/> two factor authentication providers; used by <see cref="IdentityConfiguration"/>.
	/// </summary>
	public class TwoFactorProvidersConfiguration
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="TwoFactorProvidersConfiguration"/> class.
		/// </summary>
		public TwoFactorProvidersConfiguration()
		{
			this.PhoneCode = new TwoFactorProviderPhoneCodeConfiguration();
			this.EmailCode = new TwoFactorProviderEmailCodeConfiguration();
		}

		/// <summary>
		/// Gets the <see cref="Microsoft.AspNet.Identity.PhoneNumberTokenProvider{TApplicationUser}"/> (phone code two factor authentication provider) configuration.
		/// </summary>
		/// <value><see cref="TwoFactorProviderPhoneCodeConfiguration"/> for the <see cref="Microsoft.AspNet.Identity.PhoneNumberTokenProvider{TApplicationUser}"/>.</value>
		public TwoFactorProviderPhoneCodeConfiguration PhoneCode { get; private set; }

		/// <summary>
		/// Gets the <see cref="Microsoft.AspNet.Identity.EmailTokenProvider{TApplicationUser}"/> (email code two factor authentication provider) configuration.
		/// </summary>
		/// <value><see cref="TwoFactorProviderEmailCodeConfiguration"/> for the <see cref="Microsoft.AspNet.Identity.PhoneNumberTokenProvider{TApplicationUser}"/>.</value>
		public TwoFactorProviderEmailCodeConfiguration EmailCode { get; private set; }
	}
}