﻿using System;

namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// Stores configuration details for <see cref="Microsoft.AspNet.Identity.UserManager{TUser}"/>s; used by <see cref="IdentityConfiguration"/>.
	/// </summary>
	public class UserLockoutConfiguration
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UserLockoutConfiguration"/> class, setting <see cref="UserLockoutEnabledByDefault"/> to <c>true</c>,
		/// <see cref="DefaultAccountLockoutTimeSpan"/> to 5 minutes and <see cref="MaxFailedAccessAttemptsBeforeLockout"/> to 5.
		/// </summary>
		public UserLockoutConfiguration()
		{
			this.UserLockoutEnabledByDefault = true;
			this.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
			this.MaxFailedAccessAttemptsBeforeLockout = 5;
		}

		/// <summary>
		/// Gets or sets a value indicating whether user lockout is enabled for newly created users; new instances of <see cref="PasswordValidatorConfiguration"/> have this set to <c>true</c>.
		/// </summary>
		/// <value><c>true</c> if lockout is enabled for newly created users; <c>false</c> otherwise.</value>
		public bool UserLockoutEnabledByDefault { get; set; }

		/// <summary>
		/// Gets or sets the default amount of time that a user is locked out for after <see cref="MaxFailedAccessAttemptsBeforeLockout"/> is reached; new instances of <see cref="PasswordValidatorConfiguration"/> have this set to 5 minutes.
		/// </summary>
		/// <value>Amount of time a user is locked out for.</value>
		public TimeSpan DefaultAccountLockoutTimeSpan { get; set; }

		/// <summary>
		/// Gets or sets the number of access attempts allowed before a user is locked out (if lockout is enabled); ; new instances of <see cref="PasswordValidatorConfiguration"/> have this set to 5.
		/// </summary>
		/// <value>Number of access attempts allowed before a user is locked out (if locked out is enabled).</value>
		public int MaxFailedAccessAttemptsBeforeLockout { get; set; }
	}
}