﻿using System;
using Microsoft.Owin.Security;

namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// A concrete implementation of <see cref="IAuthenticationManager"/> that always throws <see cref="NotImplementedException"/> whenever any operation
	/// is performed. This is used during Simple Injector configuration when there's no Owin context available, see
	/// <see cref="SimpleSyndicate.Mvc.App_Start.SimpleInjectorConfigurationBase{TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager}.CreateAuthenticationManager"/>.
	/// </summary>
	public class NotImplementedAuthenticationManager : IAuthenticationManager
	{
		/// <summary>
		/// Gets or sets the Security.Challenge environment value as a strong type.
		/// </summary>
		/// <value>Not applicable; <see cref="NotImplementedException"/> is always thrown.</value>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", Justification = "Class is not expected to provide any implementation.")]
		public AuthenticationResponseChallenge AuthenticationResponseChallenge
		{
			get
			{
				throw new NotImplementedException();
			}

			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Gets or sets the Security.SignIn environment value as a strong type.
		/// </summary>
		/// <value>Not applicable; <see cref="NotImplementedException"/> is always thrown.</value>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", Justification = "Class is not expected to provide any implementation.")]
		public AuthenticationResponseGrant AuthenticationResponseGrant
		{
			get
			{
				throw new NotImplementedException();
			}

			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Gets or sets the security.SignOut environment value as a strong type.
		/// </summary>
		/// <value>Not applicable; <see cref="NotImplementedException"/> is always thrown.</value>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", Justification = "Class is not expected to provide any implementation.")]
		public AuthenticationResponseRevoke AuthenticationResponseRevoke
		{
			get
			{
				throw new NotImplementedException();
			}

			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Gets or sets the current user for the request.
		/// </summary>
		/// <value>Not applicable; <see cref="NotImplementedException"/> is always thrown.</value>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1065:DoNotRaiseExceptionsInUnexpectedLocations", Justification = "Class is not expected to provide any implementation.")]
		public System.Security.Claims.ClaimsPrincipal User
		{
			get
			{
				throw new NotImplementedException();
			}

			set
			{
				throw new NotImplementedException();
			}
		}

		/// <summary>
		/// Call back through the middleware to ask for a specific form of authentication to be performed
		/// on the current request.
		/// </summary>
		/// <param name="authenticationTypes">Identifies which middleware should respond to the request
		/// for authentication. This value is compared to the middleware's Options.AuthenticationType property.</param>
		/// <returns>Returns an object with the results of the authentication. The AuthenticationResult.Identity
		/// may be null if authentication failed. Even if the Identity property is null, there may still be 
		/// AuthenticationResult.properties and AuthenticationResult.Description information returned.</returns>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		public System.Threading.Tasks.Task<System.Collections.Generic.IEnumerable<AuthenticateResult>> AuthenticateAsync(string[] authenticationTypes)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Call back through the middleware to ask for a specific form of authentication to be performed
		/// on the current request
		/// </summary>
		/// <param name="authenticationType">Identifies which middleware should respond to the request
		/// for authentication. This value is compared to the middleware's Options.AuthenticationType property.</param>
		/// <returns>Returns an object with the results of the authentication. The AuthenticationResult.Identity
		/// may be null if authentication failed. Even if the Identity property is null, there may still be 
		/// AuthenticationResult.properties and AuthenticationResult.Description information returned.</returns>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		public System.Threading.Tasks.Task<AuthenticateResult> AuthenticateAsync(string authenticationType)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Called to perform any number of authentication mechanisms on the current request.
		/// </summary>
		/// <param name="authenticationTypes">Identifies one or more middleware which should attempt to respond</param>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		public void Challenge(params string[] authenticationTypes)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Add information into the response environment that will cause the authentication middleware to challenge
		/// the caller to authenticate. This also changes the status code of the response to 401. The nature of that 
		/// challenge varies greatly, and ranges from adding a response header or changing the 401 status code to 
		/// a 302 redirect.
		/// </summary>
		/// <param name="properties">Additional arbitrary values which may be used by particular authentication types.</param>
		/// <param name="authenticationTypes">Identify which middleware should perform their alterations on the
		/// response. If the authenticationTypes is null or empty, that means the 
		/// AuthenticationMode.Active middleware should perform their alterations on the response.</param>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		public void Challenge(AuthenticationProperties properties, params string[] authenticationTypes)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Lists the description data of all of the authentication middleware which are true for a given predicate
		/// </summary>
		/// <param name="predicate">A function provided by the caller which returns true for descriptions that should be in the returned list</param>
		/// <returns>The authentication descriptions</returns>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		public System.Collections.Generic.IEnumerable<AuthenticationDescription> GetAuthenticationTypes(Func<AuthenticationDescription, bool> predicate)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Lists all of the description data provided by authentication middleware that have been chained
		/// </summary>
		/// <returns>The authentication descriptions</returns>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		public System.Collections.Generic.IEnumerable<AuthenticationDescription> GetAuthenticationTypes()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Add information to the response environment that will cause the appropriate authentication middleware
		/// to grant a claims-based identity to the recipient of the response. The exact mechanism of this may vary.
		/// Examples include setting a cookie, to adding a fragment on the redirect url, or producing an OAuth2
		/// access code or token response.
		/// </summary>
		/// <param name="identities">Determines which claims are granted to the signed in user. The 
		/// ClaimsIdentity.AuthenticationType property is compared to the middleware's Options.AuthenticationType 
		/// value to determine which claims are granted by which middleware. The recommended use is to have a single
		/// ClaimsIdentity which has the AuthenticationType matching a specific middleware.</param>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		public void SignIn(params System.Security.Claims.ClaimsIdentity[] identities)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Add information to the response environment that will cause the appropriate authentication middleware
		/// to grant a claims-based identity to the recipient of the response. The exact mechanism of this may vary.
		/// Examples include setting a cookie, to adding a fragment on the redirect url, or producing an OAuth2
		/// access code or token response.
		/// </summary>
		/// <param name="properties">Contains additional properties the middleware are expected to persist along with
		/// the claims. These values will be returned as the AuthenticateResult.properties collection when AuthenticateAsync
		/// is called on subsequent requests.</param>
		/// <param name="identities">Determines which claims are granted to the signed in user. The 
		/// ClaimsIdentity.AuthenticationType property is compared to the middleware's Options.AuthenticationType 
		/// value to determine which claims are granted by which middleware. The recommended use is to have a single
		/// ClaimsIdentity which has the AuthenticationType matching a specific middleware.</param>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		public void SignIn(AuthenticationProperties properties, params System.Security.Claims.ClaimsIdentity[] identities)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Add information to the response environment that will cause the appropriate authentication middleware
		/// to revoke any claims identity associated the the caller. The exact method varies.
		/// </summary>
		/// <param name="authenticationTypes">Identifies which middleware should perform the work to sign out.
		/// Multiple authentication types may be provided to clear out more than one cookie at a time, or to clear
		/// cookies and redirect to an external single-sign out url.</param>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		public void SignOut(params string[] authenticationTypes)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Add information to the response environment that will cause the appropriate authentication middleware
		/// to revoke any claims identity associated the the caller. The exact method varies.
		/// </summary>
		/// <param name="properties">Additional arbitrary values which may be used by particular authentication types.</param>
		/// <param name="authenticationTypes">Identifies which middleware should perform the work to sign out.
		/// Multiple authentication types may be provided to clear out more than one cookie at a time, or to clear
		/// cookies and redirect to an external single-sign out url.</param>
		/// <exception cref="NotImplementedException">Always thrown.</exception>
		public void SignOut(AuthenticationProperties properties, params string[] authenticationTypes)
		{
			throw new NotImplementedException();
		}
	}
}