﻿namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// Holds ASP.Net Identity configuration information; used by <see cref="SimpleSyndicate.Mvc.App_Start.SimpleInjectorConfigurationBase{TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager}"/>.
	/// </summary>
	public class IdentityConfiguration
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="IdentityConfiguration"/> class.
		/// </summary>
		public IdentityConfiguration()
		{
			this.PasswordValidator = new PasswordValidatorConfiguration();
			this.TwoFactorProviders = new TwoFactorProvidersConfiguration();
			this.UserLockout = new UserLockoutConfiguration();
			this.UserValidator = new UserValidatorConfiguration();
		}

		/// <summary>
		/// Gets the <see cref="Microsoft.AspNet.Identity.PasswordValidator"/> configuration.
		/// </summary>
		/// <value>The <see cref="PasswordValidatorConfiguration"/> for the <see cref="Microsoft.AspNet.Identity.PasswordValidator"/>.</value>
		public PasswordValidatorConfiguration PasswordValidator { get; private set; }

		/// <summary>
		/// Gets the <see cref="Microsoft.AspNet.Identity"/> configuration for two factor authentication providers.
		/// </summary>
		/// <value>The <see cref="TwoFactorProvidersConfiguration"/> for the <see cref="Microsoft.AspNet.Identity"/> two factor authentication providers.</value>
		public TwoFactorProvidersConfiguration TwoFactorProviders { get; private set; }

		/// <summary>
		/// Gets the <see cref="Microsoft.AspNet.Identity.UserManager{TUser}"/> configuration.
		/// </summary>
		/// <value>The <see cref="UserLockoutConfiguration"/> for the <see cref="Microsoft.AspNet.Identity.UserManager{TUser}"/>.</value>
		public UserLockoutConfiguration UserLockout { get; private set; }

		/// <summary>
		/// Gets the <see cref="Microsoft.AspNet.Identity.UserValidator{TUser}"/> configuration.
		/// </summary>
		/// <value>The <see cref="UserValidatorConfiguration"/> for the <see cref="Microsoft.AspNet.Identity.UserValidator{TUser}"/>.</value>
		public UserValidatorConfiguration UserValidator { get; private set; }
	}
}