﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSyndicate.Mvc.Models;

namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// <para>
	/// User manager suitable for most MVC applications, for users of type <see cref="ApplicationUser"/>.
	/// </para>
	/// <para>
	/// If you want to use your own <see cref="ApplicationUser"/> type (e.g. the one created automatically in a new MVC project) then use <see cref="ApplicationUserManager{TApplicationUser}"/>.
	/// </para>
	/// </summary>
	public class ApplicationUserManager : ApplicationUserManager<ApplicationUser>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="ApplicationUserManager"/> class using the specified backing store.
		/// </summary>
		/// <param name="store">Backing store.</param>
		public ApplicationUserManager(IUserStore<ApplicationUser> store)
			: base(store)
		{
		}
	}
}