﻿using Microsoft.AspNet.Identity;

namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// Stores configuration details for <see cref="Microsoft.AspNet.Identity.EmailTokenProvider{TApplicationUser}"/>s (email code two factor authentication); used by <see cref="TwoFactorProvidersConfiguration"/>.
	/// </summary>
	public class TwoFactorProviderEmailCodeConfiguration
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="TwoFactorProviderEmailCodeConfiguration"/> class, setting <see cref="Subject"/>
		/// to "Security Code" and .
		/// </summary>
		public TwoFactorProviderEmailCodeConfiguration()
		{
			this.Subject = "Security Code";
			this.BodyFormat = "Your security code is {0}.";
		}

		/// <summary>
		/// Gets or sets the email service that will send the email containing the code; new instances of <see cref="TwoFactorProviderEmailCodeConfiguration"/> have this set to <c>null</c>.
		/// </summary>
		/// <value>Email service that sends email containing the code.</value>
		public IIdentityMessageService EmailService { get; set; }

		/// <summary>
		/// Gets or sets the subject for the email sent containing the code; new instances of <see cref="TwoFactorProviderEmailCodeConfiguration"/> have this set to "Security Code".
		/// </summary>
		/// <value>Subject for the email sent containing the code.</value>
		public string Subject { get; set; }

		/// <summary>
		/// Gets or sets the body for the email sent containing the code, which should be a format string which the code will be the only argument; new instances of <see cref="TwoFactorProviderEmailCodeConfiguration"/> have this set to "Your security code is {0}.".
		/// </summary>
		/// <value>Body for the email sent containing the code, which should be a format string which the code will be the only argument.</value>
		public string BodyFormat { get; set; }
	}
}