﻿namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// Stores configuration details for <see cref="Microsoft.AspNet.Identity.UserValidator{TUser}"/>s; used by <see cref="IdentityConfiguration"/>.
	/// </summary>
	public class UserValidatorConfiguration
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="UserValidatorConfiguration"/> class, with <see cref="AllowOnlyAlphanumericUserNames"/> set to <c>false</c>
		/// and <see cref="RequireUniqueEmail"/> set to <c>true</c>.
		/// </summary>
		public UserValidatorConfiguration()
		{
			this.AllowOnlyAlphanumericUserNames = false;
			this.RequireUniqueEmail = true;
		}

		/// <summary>
		/// Gets or sets a value indicating whether to only allow alphanumeric <c>[A-Za-z0-9@_]</c> characters in usernames; new instances of <see cref="PasswordValidatorConfiguration"/> have this set to <c>false</c>.
		/// </summary>
		/// <value><c>true</c> if only alphanumeric <c>[A-Za-z0-9@_]</c> characters are allowed in usernames; <c>false</c> otherwise.</value>
		public bool AllowOnlyAlphanumericUserNames { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to require email addresses to be non-empty, valid and unique; new instances of <see cref="PasswordValidatorConfiguration"/> have this set to <c>true</c>. 
		/// </summary>
		/// <value><c>true</c> if email addresses are require to be non-empty, valid and unique; <c>false</c> otherwise.</value>
		public bool RequireUniqueEmail { get; set; }
	}
}