﻿using Microsoft.AspNet.Identity;

namespace SimpleSyndicate.Mvc.AspNet.Identity
{
	/// <summary>
	/// Stores configuration details for <see cref="Microsoft.AspNet.Identity.PhoneNumberTokenProvider{TApplicationUser}"/>s (phone code two factor authentication); used by <see cref="TwoFactorProvidersConfiguration"/>.
	/// </summary>
	public class TwoFactorProviderPhoneCodeConfiguration
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="TwoFactorProviderPhoneCodeConfiguration"/> class, setting <see cref="MessageFormat"/>
		/// to "Your security code is {0}".
		/// </summary>
		public TwoFactorProviderPhoneCodeConfiguration()
		{
			this.MessageFormat = "Your security code is {0}.";
		}

		/// <summary>
		/// Gets or sets the SMS service that will send the SMS containing the code; new instances of <see cref="TwoFactorProviderPhoneCodeConfiguration"/> have this set to <c>null</c>.
		/// </summary>
		/// <value>Email service that sends email containing the code.</value>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Sms", Justification = "Sms is an appropirate acronym and not Hungarian notation")]
		public IIdentityMessageService SmsService { get; set; }

		/// <summary>
		/// Gets or sets the message contents which should be a format string which the code will be the only argument; new instances of <see cref="TwoFactorProviderPhoneCodeConfiguration"/> have this set to "Your security code is {0}.".
		/// </summary>
		/// <value>Content for the message sent containing the code, which should be a format string which the code will be the only argument.</value>
		public string MessageFormat { get; set; }
	}
}