﻿namespace SimpleSyndicate.Mvc.AspNet
{
	/// <summary>
	/// The <see cref="AspNet"/> namespace contains classes that work with the <see cref="Microsoft.AspNet"/> namespace.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
