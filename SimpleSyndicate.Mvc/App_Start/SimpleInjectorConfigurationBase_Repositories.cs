﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using SimpleInjector;
using SimpleSyndicate.Models;
using SimpleSyndicate.Mvc.Models;
using SimpleSyndicate.Mvc.Repositories;
using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <content>
	/// Contains repository related functionality.
	/// </content>
	public abstract partial class SimpleInjectorConfigurationBase<TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager>
		where TIdentityDbContext : IdentityDbContext<TApplicationUser>
		where TApplicationUser : IdentityUser
		where TApplicationUserManager : UserManager<TApplicationUser>
		where TApplicationSignInManager : SignInManager<TApplicationUser, string>
	{
		/// <summary>
		/// <para>
		/// Registers various <see cref="IRepository{TEntity}"/> with concrete implementations suitable for most MVC applications; this is called automatically when
		/// <see cref="Setup"/> is called, immediately after <see cref="RegisterIdentityDbContext"/> has been called but before <see cref="SetupContainer"/> is called.
		/// </para>
		/// <para>
		/// For completeness, this method calls <see cref="RegisterRepositoriesAspNetUserDefaultRole"/> first, <see cref="RegisterRepositoriesAspNetUserLoginDetail"/>
		/// second and then <see cref="RegisterRepositoriesVersionHistoryItem"/>.
		/// </para>
		/// <para>
		/// If you don't want some (or all) of these registered, override the appropriate method(s) in your derived class.
		/// </para>
		/// </summary>
		protected virtual void RegisterRepositories()
		{
			this.RegisterRepositoriesAspNetUserDefaultRole();
			this.RegisterRepositoriesAspNetUserLoginDetail();
			this.RegisterRepositoriesVersionHistoryItem();
		}

		/// <summary>
		/// <para>
		/// Registers an <see cref="IRepository{TEntity}"/> for the <see cref="AspNetUserDefaultRole"/> entity with an <see cref="IdentityDbContextRepository{TEntity, TIdentityDbContext, TIdentityUser}"/>
		/// concrete implementation; the repository uses the <typeparamref name="TIdentityDbContext"/> as the underlying data store; this is called automatically
		/// when <see cref="Setup"/> is called (via <see cref="RegisterRepositories"/>), immediately after <see cref="RegisterIdentityDbContext"/> has been called
		/// but before <see cref="SetupContainer"/> is called.
		/// </para>
		/// <para>
		/// For completeness, <see cref="RegisterRepositories"/> calls <see cref="RegisterRepositoriesAspNetUserDefaultRole"/> first, then
		/// <see cref="RegisterRepositoriesAspNetUserLoginDetail"/> and finally <see cref="RegisterRepositoriesVersionHistoryItem"/>.
		/// </para>
		/// </summary>
		protected virtual void RegisterRepositoriesAspNetUserDefaultRole()
		{
			container.RegisterPerWebRequest<IRepository<AspNetUserDefaultRole>>(() => CreateRepository<AspNetUserDefaultRole>());
		}

		/// <summary>
		/// <para>
		/// Registers an <see cref="IRepository{TEntity}"/> for the <see cref="AspNetUserLoginDetail"/> entity with an <see cref="IdentityDbContextRepository{TEntity, TIdentityDbContext, TIdentityUser}"/>
		/// concrete implementation; the repository uses the <typeparamref name="TIdentityDbContext"/> as the underlying data store; this is called automatically
		/// when <see cref="Setup"/> is called (via <see cref="RegisterRepositories"/>), immediately after <see cref="RegisterIdentityDbContext"/> has been called
		/// but before <see cref="SetupContainer"/> is called.
		/// </para>
		/// <para>
		/// For completeness, <see cref="RegisterRepositories"/> calls <see cref="RegisterRepositoriesAspNetUserDefaultRole"/> first, then
		/// <see cref="RegisterRepositoriesAspNetUserLoginDetail"/> and finally <see cref="RegisterRepositoriesVersionHistoryItem"/>.
		/// </para>
		/// </summary>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login", Justification = "Login is used more consistently within ASP.Net")]
		protected virtual void RegisterRepositoriesAspNetUserLoginDetail()
		{
			container.RegisterPerWebRequest<IRepository<AspNetUserLoginDetail>>(() => CreateRepository<AspNetUserLoginDetail>());
		}

		/// <summary>
		/// <para>
		/// Registers an <see cref="IRepository{TEntity}"/> for the <see cref="VersionHistoryItem"/> entity with an <see cref="IdentityDbContextRepository{TEntity, TIdentityDbContext, TIdentityUser}"/>
		/// concrete implementation; the repository uses the <typeparamref name="TIdentityDbContext"/> as the underlying data store; this is called automatically
		/// when <see cref="Setup"/> is called (via <see cref="RegisterRepositories"/>), immediately after <see cref="RegisterIdentityDbContext"/> has been called
		/// but before <see cref="SetupContainer"/> is called.
		/// </para>
		/// <para>
		/// For completeness, <see cref="RegisterRepositories"/> calls <see cref="RegisterRepositoriesAspNetUserDefaultRole"/> first, then
		/// <see cref="RegisterRepositoriesAspNetUserLoginDetail"/> and finally <see cref="RegisterRepositoriesVersionHistoryItem"/>.
		/// </para>
		/// </summary>
		protected virtual void RegisterRepositoriesVersionHistoryItem()
		{
			container.RegisterPerWebRequest<IRepository<VersionHistoryItem>>(() => CreateRepository<VersionHistoryItem>());
		}

		/// <summary>
		/// Creates instances of <see cref="IRepository{TEntity}"/> when they need to be injected by Simple Injector; instances use
		/// <typeparamref name="TIdentityDbContext"/> as its underlying data store.
		/// </summary>
		/// <typeparam name="TEntity">The entity type the repository holds.</typeparam>
		/// <returns>An <see cref="IRepository{TEntity}"/> instance.</returns>
		protected virtual IRepository<TEntity> CreateRepository<TEntity>()
			where TEntity : class
		{
			return new IdentityDbContextRepository<TEntity, TIdentityDbContext, TApplicationUser>(container.GetInstance<TIdentityDbContext>());
		}
	}
}