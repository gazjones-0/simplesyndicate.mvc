﻿using AutoMapper;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// <para>
	/// Base class for performing AutoMapper configuration during application startup; derive from this class, customize <see cref="SetupMappings"/>
	/// to configure authentication for your application, add a static helper method that creates the class and calls <see cref="Setup"/> then add
	/// a call to the static helper method in your <c>global.asax.cs</c> file.
	/// </para>
	/// </summary>
	/// <remarks>
	/// Our general approach for application startup configuration is to derive from a library base class designed to provide the necessary configuration
	/// functionality, add our application-specific functionality and then providing a static Configure method that instantiates a new instance
	/// of the class and calls Setup. The example provides a template class, ready to use in an MVC application.
	/// </remarks>
	/// <example>
	/// <code language="cs" source="..\..\SimpleSyndicate.Mvc\SimpleSyndicate.Mvc.Help.Examples\App_Start\AutoMapperConfiguration.cs" />
	/// </example>
	public abstract class AutoMapperConfigurationBase
	{
		/// <summary>
		/// Whether to map <see cref="SimpleSyndicate.Models.VersionHistoryItem"/>.
		/// </summary>
		private bool mapVersionHistoryItem = true;

		/// <summary>
		/// Checks whether the configuration is valid by using <see cref="Mapper.AssertConfigurationIsValid()"/>.
		/// </summary>
		public static void CheckConfiguration()
		{
			Mapper.AssertConfigurationIsValid();
		}

		/// <summary>
		/// <para>
		/// Sets up the AutoMapper mappings configuration; this method will call <see cref="SetupMappings"/> which should be overridden by derived classes
		/// to set up any application-specific mappings.
		/// </para>
		/// <para>
		/// By default, mappings for <see cref="SimpleSyndicate.Models.VersionHistoryItem"/> will be set up;
		/// if you don't want this, call <see cref="DoNotMapVersionHistoryItem"/> before calling <see cref="Setup"/>.
		/// </para>
		/// </summary>
		protected void Setup()
		{
			this.SetupMappings();
			if (this.mapVersionHistoryItem)
			{
				AutoMapperMappings.VersionHistoryItem.CreateMaps();
			}
			CheckConfiguration();
		}

		/// <summary>
		/// Performs any application-specific mapping set up; this method should be overridden in derived classes.
		/// </summary>
		protected virtual void SetupMappings()
		{
		}

		/// <summary>
		/// Disables mapping of <see cref="SimpleSyndicate.Models.VersionHistoryItem"/>; this must be called before calling <see cref="Setup"/>.
		/// </summary>
		protected void DoNotMapVersionHistoryItem()
		{
			this.mapVersionHistoryItem = false;
		}
	}
}
