﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <content>
	/// Contains MVC related functionality.
	/// </content>
	public abstract partial class SimpleInjectorConfigurationBase<TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager>
		where TIdentityDbContext : IdentityDbContext<TApplicationUser>
		where TApplicationUser : IdentityUser
		where TApplicationUserManager : UserManager<TApplicationUser>
		where TApplicationSignInManager : SignInManager<TApplicationUser, string>
	{
		/// <summary>
		/// Assemblies that have had their MVC controllers registered, so we guarantee we only register an assembly once.
		/// </summary>
		private static List<Assembly> assembliesRegisteredForMvcControllers = null;

		/// <summary>
		/// Registers the MVC controllers in the specified assembly with the <see cref="Container"/>; if the assembly has previously been registered it does nothing.
		/// </summary>
		/// <param name="assembly">The <see cref="Assembly"/> to register.</param>
		/// <exception cref="InvalidOperationException">Thrown if <see cref="Setup"/> has not already been called.</exception>
		public void RegisterMvcControllers(Assembly assembly)
		{
			if (container == null)
			{
				throw new InvalidOperationException();
			}

			// create the list of registered assemblies if it hasn't been already
			if (assembliesRegisteredForMvcControllers == null)
			{
				assembliesRegisteredForMvcControllers = new List<Assembly>();
			}

			// if the assembly has already been registered, return
			foreach (var registeredAssembly in assembliesRegisteredForMvcControllers)
			{
				if (registeredAssembly == assembly)
				{
					return;
				}
			}

			// not already registered, so register and add to the list of registered assemblies
			container.RegisterMvcControllers(assembly);
			assembliesRegisteredForMvcControllers.Add(assembly);
		}

		/// <summary>
		/// Clears the list of assemblies that have been already registered (via <see cref="RegisterMvcControllersInCallingAssembly"/>,
		/// <see cref="RegisterMvcControllersInExecutingAssembly"/> or <see cref="RegisterMvcControllers"/>; this is called automatically when <see cref="Setup"/> is called,
		/// immediately after the <see cref="Container"/> has been created, but before <see cref="RegisterAppBuilder"/> is called.
		/// </summary>
		protected virtual void ClearRegisteredAssemblies()
		{
			if (assembliesRegisteredForMvcControllers != null)
			{
				assembliesRegisteredForMvcControllers.Clear();
			}
		}

		/// <summary>
		/// Registers any MVC controllers in the calling assembly with the <see cref="Container"/>; this is called automatically when <see cref="Setup"/> is called,
		/// immediately after <see cref="SetupContainer"/> has finished but before <see cref="RegisterMvcControllersInExecutingAssembly"/> is called.
		/// </summary>
		protected virtual void RegisterMvcControllersInCallingAssembly()
		{
			// Assembly.GetCallingAssembly returns the assembly the method calling this one is in (which will be SimpleSyndicate.Mvc), so
			// we can't use that; we could try and call it earlier but it quickly gets painful and inlining might throw things off anyway;
			// instead, we work our way back up the stack trace until we hit an assembly that isn't this one, which will be the calling one
			var stackTrace = new System.Diagnostics.StackTrace();
			int frame = 0;
			Assembly callingAssembly = null;
			while (frame < stackTrace.FrameCount)
			{
				try
				{
					var assembly = stackTrace.GetFrame(frame).GetMethod().Module.Assembly;
					if (assembly != Assembly.GetExecutingAssembly())
					{
						callingAssembly = assembly;
						break;
					}
				}
				catch (NotImplementedException)
				{
					// in theory, the Module property above could throw this exception; if it does, there isn't really much
					// we can do so we just swallow it and hope that we can get the module from one of the later frames
				}
				frame++;
			}
			if (callingAssembly != null)
			{
				this.RegisterMvcControllers(callingAssembly);
			}
		}

		/// <summary>
		/// Registers any MVC controllers in the executing assembly with the <see cref="Container"/>; this is called automatically when <see cref="Setup"/> is called,
		/// immediately after <see cref="RegisterMvcControllersInCallingAssembly"/> has been called but before <see cref="RegisterMvcIntegratedFilterProvider"/> is called.
		/// </summary>
		protected virtual void RegisterMvcControllersInExecutingAssembly()
		{
			this.RegisterMvcControllers(Assembly.GetExecutingAssembly());
		}

		/// <summary>
		/// Registers the MVC integrated filter provider with the <see cref="Container"/>; this is called automatically when <see cref="Setup"/> is called,
		/// immediately after the MVC controllers have been registered (via <see cref="RegisterMvcControllersInCallingAssembly"/> and
		/// <see cref="RegisterMvcControllersInExecutingAssembly"/>) but before <see cref="RegisterIdentityDbContextWithOwin"/> is called.
		/// </summary>
		protected virtual void RegisterMvcIntegratedFilterProvider()
		{
			container.RegisterMvcIntegratedFilterProvider();
		}

		/// <summary>
		/// Sets <see cref="SimpleInjectorDependencyResolver"/> as the dependency resolver MVC uses; this is called automatically when <see cref="Setup"/> is called,
		/// immediately after <see cref="CheckConfiguration"/> has been called.
		/// </summary>
		protected virtual void SetMvcResolver()
		{
			DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));
		}
	}
}