namespace SimpleSyndicate.Mvc.App_Start.AutoMapperMappings
{
	/// <summary>
	/// The <see cref="AutoMapperMappings"/> namespace contains classes that create AutoMapper mappings.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
