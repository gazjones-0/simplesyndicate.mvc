using AutoMapper;

namespace SimpleSyndicate.Mvc.App_Start.AutoMapperMappings
{
	/// <summary>
	/// Sets up AutoMapper mappings for using <see cref="SimpleSyndicate.Models.VersionHistoryItem"/> in <see cref="Controllers.VersionHistory.GenericVersionHistoryController"/>.
	/// </summary>
	public static class VersionHistoryItem
	{
		/// <summary>
		/// Creates AutoMapper mappings for <see cref="SimpleSyndicate.Models.VersionHistoryItem"/>.
		/// </summary>
		public static void CreateMaps()
		{
			// model -> view model
			Mapper.CreateMap<SimpleSyndicate.Models.VersionHistoryItem, Controllers.VersionHistory.IndexItemViewModel>();
			Mapper.CreateMap<SimpleSyndicate.Models.VersionHistoryItem, Controllers.VersionHistory.CurrentVersionViewModel>();
		}
	}
}