﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using SimpleInjector;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <content>
	/// Contains <see cref="IdentityDbContext"/> related functionality.
	/// </content>
	public abstract partial class SimpleInjectorConfigurationBase<TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager>
		where TIdentityDbContext : IdentityDbContext<TApplicationUser>
		where TApplicationUser : IdentityUser
		where TApplicationUserManager : UserManager<TApplicationUser>
		where TApplicationSignInManager : SignInManager<TApplicationUser, string>
	{
		/// <summary>
		/// Delegate that creates instances of <typeparamref name="TIdentityDbContext"/>.
		/// </summary>
		private Func<TIdentityDbContext> savedIdentityDbContextInstanceProducer = null;

		/// <summary>
		/// Checks whether <typeparamref name="TIdentityDbContext"/> has been registered, throwing an exception if it hasn't been. It is expected that this
		/// type is registered by the time the check is performed otherwise the application won't work.
		/// </summary>
		/// <exception cref="InvalidOperationException">Thrown if <typeparamref name="TIdentityDbContext"/> has not been registered.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "TIdentityDbContext", Justification = "TIdentityDbContext is spelt correctly.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "ApplicationDbContext", Justification = "ApplicationDbContext is spelt correctly.")]
		protected static void CheckIdentityDbContextIsRegistered()
		{
			var registrations = container.GetCurrentRegistrations();
			foreach (var registration in registrations)
			{
				if (registration.ServiceType == typeof(TIdentityDbContext))
				{
					return;
				}
			}
			throw new InvalidOperationException("Simple Injector setup failed - type TIdentityDbContext (probably ApplicationDbContext in your application) has not been registered; your application cannot work without this.");
		}

		/// <summary>
		/// Registers the <typeparamref name="TIdentityDbContext"/> as per web request, using the specified <paramref name="identityDbContextInstanceCreator"/>
		/// delegate to create instances of it; this method is called immediately after <see cref="SetupAspNetIdentityConfiguration"/> has been called
		/// but before <see cref="RegisterRepositories"/> is called.
		/// </summary>
		/// <param name="identityDbContextInstanceCreator">Delegate that creates instances of <typeparamref name="TIdentityDbContext"/>.</param>
		protected virtual void RegisterIdentityDbContext(Func<TIdentityDbContext> identityDbContextInstanceCreator)
		{
			container.RegisterPerWebRequest<TIdentityDbContext>(identityDbContextInstanceCreator);
		}

		/// <summary>
		/// <para>
		/// Registers the <typeparamref name="TIdentityDbContext"/> with Owin so that applications that use the Owin context to get the <typeparamref name="TIdentityDbContext"/>
		/// can continue to work without any changes; this is called automatically when <see cref="Setup"/> is called, immediately after
		/// <see cref="RegisterMvcIntegratedFilterProvider"/> has been called but before <see cref="CheckIdentityDbContextIsRegistered"/> is called.
		/// </para>
		/// <para>
		/// Registration with Owin has to be done after all other setup has completed as it retrieves instances from the <see cref="Container"/>
		/// which will lock the container.
		/// </para>
		/// </summary>
		protected virtual void RegisterIdentityDbContextWithOwin()
		{
			container.GetInstance<IAppBuilder>().CreatePerOwinContext<TIdentityDbContext>(() => container.GetInstance<TIdentityDbContext>());
		}
	}
}