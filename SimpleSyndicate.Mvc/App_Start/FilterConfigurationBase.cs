﻿using System;
using System.Web.Mvc;
using SimpleInjector;
using SimpleSyndicate.Mvc.DependencyInjection.SimpleInjector;
using SimpleSyndicate.Mvc.Web.Mvc;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// <para>
	/// Base class for performing filter configuration during application startup; derive from this class, customize <see cref="SetupFilters"/>
	/// to configure authentication for your application, add a static helper method that creates the class and calls <see cref="Setup()"/>
	/// or <see cref="Setup(Container)"/> then add a call to the static helper method in your <c>global.asax.cs</c> file.
	/// </para>
	/// <para>
	/// By default, the <see cref="AccessDeniedAuthorizeAttribute"/>, <see cref="HandleErrorAttribute"/> and
	/// <see cref="VersionHistoryActionFilterAttribute"/> are added to the global filters; if you don't want this behavior,
	/// use the various methods on the class to disable the ones you don't want.
	/// </para>
	/// </summary>
	/// <remarks>
	/// Our general approach for application startup configuration is to derive from a library base class designed to provide the necessary configuration
	/// functionality, add our application-specific functionality and then providing a static Configure method that instantiates a new instance
	/// of the class and calls Setup. The example provides a template class, ready to use in an MVC application.
	/// </remarks>
	/// <example>
	/// <code language="cs" source="..\..\SimpleSyndicate.Mvc\SimpleSyndicate.Mvc.Help.Examples\App_Start\FilterConfiguration.cs" />
	/// </example>
	public abstract class FilterConfigurationBase
	{
		/// <summary>
		/// Whether to add <see cref="AccessDeniedAuthorizeAttribute"/> to the global filters.
		/// </summary>
		private bool addAccessDeniedAuthorizeAttribute = true;

		/// <summary>
		/// Whether to add <see cref="HandleErrorAttribute"/> to the global filters.
		/// </summary>
		private bool addHandleErrorAttribute = true;

		/// <summary>
		/// Whether to add <see cref="VersionHistoryActionFilterAttribute"/> to the global filters.
		/// </summary>
		private bool addVersionHistoryActionFilterAttribute = true;

		/// <summary>
		/// Disables adding <see cref="AccessDeniedAuthorizeAttribute"/> to the global filters; this must be called before calling <see cref="Setup()"/> or <see cref="Setup(Container)"/>.
		/// </summary>
		protected void DoNotAddAccessDeniedAuthorizeAttribute()
		{
			this.addAccessDeniedAuthorizeAttribute = false;
		}

		/// <summary>
		/// Disables adding <see cref="HandleErrorAttribute"/> to the global filters; this must be called before calling <see cref="Setup()"/> or <see cref="Setup(Container)"/>.
		/// </summary>
		protected void DoNotAddHandleErrorAttribute()
		{
			this.addHandleErrorAttribute = false;
		}

		/// <summary>
		/// Disables adding <see cref="VersionHistoryActionFilterAttribute"/> to the global filters; this must be called before calling <see cref="Setup()"/> or <see cref="Setup(Container)"/>.
		/// </summary>
		protected void DoNotAddVersionHistoryActionFilterAttribute()
		{
			this.addVersionHistoryActionFilterAttribute = false;
		}

		/// <overloads>
		/// <summary>
		/// <para>
		/// Sets up the filter configuration; this method will call <see cref="SetupFilters"/> which should be overridden by derived classes
		/// to perform any application-specific filter configuration.
		/// </para>
		/// <para>
		/// By default, <see cref="HandleErrorAttribute"/>, <see cref="AccessDeniedAuthorizeAttribute"/> and
		/// <see cref="VersionHistoryActionFilterAttribute"/> will be added to the global filters; if you don't want this behavior,
		/// use the various methods on the class to disable the ones you don't want.
		/// </para>
		/// </summary>
		/// </overloads>
		/// <summary>
		/// <para>
		/// Sets up the filter configuration; this method will call <see cref="SetupFilters"/> which should be overridden by derived classes
		/// to perform any application-specific filter configuration.
		/// </para>
		/// <para>
		/// By default, <see cref="HandleErrorAttribute"/>, <see cref="AccessDeniedAuthorizeAttribute"/> and
		/// <see cref="VersionHistoryActionFilterAttribute"/> will be added to the global filters; if you don't want this behavior,
		/// use the various methods on the class to disable the ones you don't want before calling <see cref="Setup()"/>.
		/// </para>
		/// <para>
		/// If you're adding the <see cref="VersionHistoryActionFilterAttribute"/>, note that this method will cause an exception to be
		/// thrown as the <see cref="VersionHistoryActionFilterAttribute"/> requires a Simple Injector container to work and one isn't
		/// being provided -- you should use <see cref="Setup(Container)"/> instead.
		/// </para>
		/// </summary>
		/// <exception cref="InvalidOperationException">Thrown when adding <see cref="VersionHistoryActionFilterAttribute"/> as there's no Simple Injector container provided; use <see cref="Setup(Container)"/> instead.</exception>
		protected void Setup()
		{
			this.Setup(false, null);
		}

		/// <summary>
		/// <para>
		/// Sets up the filter configuration; this method will call <see cref="SetupFilters"/> which should be overridden by derived classes
		/// to perform any application-specific filter configuration.
		/// </para>
		/// <para>
		/// By default, <see cref="HandleErrorAttribute"/>, <see cref="AccessDeniedAuthorizeAttribute"/> and
		/// <see cref="VersionHistoryActionFilterAttribute"/> will be added to the global filters; if you don't want this behavior,
		/// use the various methods on the class to disable the ones you don't want before calling <see cref="Setup(Container)"/>.
		/// </para>
		/// </summary>
		/// <param name="container">Simple Injector container to use for the <see cref="VersionHistoryActionFilterAttribute"/>.</param>
		/// <exception cref="InvalidOperationException">Thrown when adding <see cref="VersionHistoryActionFilterAttribute"/> and <paramref name="container"/> is <c>null</c>.</exception>
		protected void Setup(Container container)
		{
			this.Setup(true, container);
		}

		/// <summary>
		/// Performs any application-specific filter configuration; this method should be overridden in derived classes.
		/// </summary>
		/// <param name="filters">The current filters.</param>
		protected virtual void SetupFilters(GlobalFilterCollection filters)
		{
		}

		/// <summary>
		/// <para>
		/// Sets up the filter configuration; this method will call <see cref="SetupFilters"/> which should be overridden by derived classes
		/// to perform any application-specific filter configuration.
		/// </para>
		/// <para>
		/// By default, <see cref="HandleErrorAttribute"/>, <see cref="AccessDeniedAuthorizeAttribute"/> and
		/// <see cref="VersionHistoryActionFilterAttribute"/> will be added to the global filters; if you don't want this behavior,
		/// use the various methods on the class to disable the ones you don't want before calling <see cref="Setup(Container)"/>.
		/// </para>
		/// </summary>
		/// <param name="containerWasProvided">Whether a Simple Injector container was originally passed in to this class.</param>
		/// <param name="container">Simple Injector container to use for the <see cref="VersionHistoryActionFilterAttribute"/>; pass <c>null</c> if you're not using Simple Injector or not adding the <see cref="VersionHistoryActionFilterAttribute"/>.</param>
		/// <exception cref="InvalidOperationException">Thrown when adding <see cref="VersionHistoryActionFilterAttribute"/> and <paramref name="containerWasProvided"/> is <c>false</c> or when adding <see cref="VersionHistoryActionFilterAttribute"/> and <paramref name="containerWasProvided"/> is <c>true</c> and <paramref name="container"/> is <c>null</c>.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", Justification = "Literals in this method are entire strings.")]
		private void Setup(bool containerWasProvided, Container container)
		{
			if (this.addHandleErrorAttribute)
			{
				GlobalFilters.Filters.Add(new HandleErrorAttribute());
			}
			if (this.addAccessDeniedAuthorizeAttribute)
			{
				GlobalFilters.Filters.Add(new AccessDeniedAuthorizeAttribute());
			}
			if (this.addVersionHistoryActionFilterAttribute)
			{
				if (!containerWasProvided)
				{
					throw new InvalidOperationException(
						"SimpleSyndicate.Mvc.App_Start.FilterConfigurationBase was configured to add VersionHistoryActionFilterAttribute "
						+ "but no Simple Injector container was provided. If you want to use the VersionHistoryActionFilterAttribute, "
						+ "check that you're calling FilterConfigurationBase.Setup(Container container) rather than "
						+ "FilterConfigurationBase.Setup(); if you don't want to use VersionHistoryActionFilterAttribute, you'll need "
						+ "to add a call to FilterConfigurationBase.DoNotAddVersionHistoryActionFilterAttribute() before calling Setup().");
				}
				if (containerWasProvided && (container == null))
				{
					throw new InvalidOperationException(
						"SimpleSyndicate.Mvc.App_Start.FilterConfigurationBase was configured to add VersionHistoryActionFilterAttribute "
						+ "but the Simple Injector container passed in was null. If you want to use the VersionHistoryActionFilterAttribute, "
						+ "check that you're setting up the container (e.g. via a class derived from "
						+ "SimpleInjectorConfigurationBase) before performing filter configuration (the most likely cause "
						+ "is that you've got the filter setup before the Simple Injector setup in your global.asax.cs file); "
						+ "if you don't want to use VersionHistoryActionFilterAttribute, you'll need to add a call to "
						+ "FilterConfigurationBase.DoNotAddVersionHistoryActionFilterAttribute() before calling Setup().");
				}
				if (!container.IsVersionHistoryItemRepositoryRegistered())
				{
					throw new InvalidOperationException(
						"SimpleSyndicate.Mvc.App_Start.FilterConfigurationBase was configured to add VersionHistoryActionFilterAttribute "
						+ "but the Simple Injector container passed in does not have a registration for "
						+ "IRepository<VersionHistoryItem> and the filter will not work without this. If you want to use the "
						+ "VersionHistoryActionFilterAttribute check that you've added this registration before performing filter "
						+ "configuration (the most likely cause is you've not done this in your overridden SetupContainer "
						+ "method in your SimpleInjectorConfigurationBase derived class); if you don't want to use "
						+ "VersionHistoryActionFilterAttribute, you'll need to add a call to "
						+ "FilterConfigurationBase.DoNotAddVersionHistoryActionFilterAttribute() before calling Setup().");
				}
				var filter = new VersionHistoryActionFilterAttribute();
				filter.Container = container;
				GlobalFilters.Filters.Add(filter);
			}
			this.SetupFilters(GlobalFilters.Filters);
		}
	}
}