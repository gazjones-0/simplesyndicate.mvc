﻿namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// Interface that provides functionality for easily configuring authentication in an MVC application, used by
	/// <see cref="AuthenticationConfigurationBase{TApplicationUser, TApplicationUserManager}"/>; use
	/// <see cref="AuthenticationConfigurerFactory{TApplicationUser, TApplicationUserManager}"/> to obtain a concrete implementation suitable for use
	/// in most MVC applications, although in general you won't need to do this as <see cref="AuthenticationConfigurationBase{TApplicationUser, TApplicationUserManager}"/>
	/// creates one for you.
	/// </summary>
	/// <remarks>
	/// Information on configuring authentication can be found at <see href="http://go.microsoft.com/fwlink/?LinkId=301864"/>.
	/// </remarks>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Configurer", Justification = "Identifier is spelt correctly.")]
	public interface IAuthenticationConfigurer
	{
		/// <overloads>
		/// <summary>
		/// Enables cookie authentication.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Enables cookie authentication using "/Account/Login" as the page to the login page.
		/// </summary>
		void ConfigureCookie();

		/// <summary>
		/// Enables cookie authentication using the specified path for the login page.
		/// </summary>
		/// <param name="path">The path to the login page.</param>
		void ConfigureCookie(string path);

		/// <overloads>
		/// <summary>
		/// Enables Facebook authentication.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Enables Facebook authentication using the specified application id and secret and, when the user authenticates via
		/// Facebook, asks for permission to know the email address they've registered with Facebook.
		/// </summary>
		/// <param name="appId">Application id.</param>
		/// <param name="appSecret">Application secret.</param>
		void ConfigureFacebook(string appId, string appSecret);

		/// <summary>
		/// Enables Facebook authentication using the specified application id, secret and, when the user authenticates via Facebook,
		/// whether to ask permission to know the email address they've registered with Facebook.
		/// </summary>
		/// <param name="appId">Application id.</param>
		/// <param name="appSecret">Application secret.</param>
		/// <param name="addEmailScope">When a user authenticates, whether to ask for permission to know the email address they've registered with Facebook.</param>
		void ConfigureFacebook(string appId, string appSecret, bool addEmailScope);

		/// <summary>
		/// Enables Google authentication.
		/// </summary>
		void ConfigureGoogle();

		/// <overloads>
		/// <summary>
		/// Enables Microsoft account authentication.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Enables Microsoft account authentication using the specified client id and secret and, when the user authenticates via
		/// Microsoft, asks for permission to know the email address they've registered with Microsoft.
		/// </summary>
		/// <param name="clientId">Client id.</param>
		/// <param name="clientSecret">Client secret.</param>
		void ConfigureMicrosoft(string clientId, string clientSecret);

		/// <summary>
		/// Enables Microsoft account authentication using the specified client id, secret and, when the user authenticates via Microsoft,
		/// whether to ask permission to know the email address they've registered with Microsoft.
		/// </summary>
		/// <param name="clientId">Client id.</param>
		/// <param name="clientSecret">Client secret.</param>
		/// <param name="addEmailScope">When a user authenticates, whether to ask for permission to know the email address they've registered with Microsoft.</param>
		void ConfigureMicrosoft(string clientId, string clientSecret, bool addEmailScope);

		/// <summary>
		/// Enables Twitter authentication using the specified consumer key and secret.
		/// </summary>
		/// <param name="consumerKey">Consumer key.</param>
		/// <param name="consumerSecret">Consumer secret.</param>
		void ConfigureTwitter(string consumerKey, string consumerSecret);
	}
}
