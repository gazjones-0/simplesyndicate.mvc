﻿using Owin;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// Interface for performing authentication configuration during application startup; see <see cref="AuthenticationConfigurationBase"/> for a concrete implementation.
	/// </summary>
	/// <remarks>
	/// This interface is necessary to facilitate unit testing of Owin authentication.
	/// </remarks>
	/// <seealso cref="AuthenticationConfigurationBase"/>
	public interface IAuthenticationConfiguration
	{
		/// <summary>
		/// Sets up the authentication configuration.
		/// </summary>
		/// <param name="appBuilder">An <see cref="IAppBuilder"/> instance.</param>
		void Setup(IAppBuilder appBuilder);
	}
}
