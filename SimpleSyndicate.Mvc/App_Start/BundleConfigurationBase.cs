﻿using System.Web.Optimization;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// <para>
	/// Base class for performing bundle configuration during application startup; derive from this class, customize <see cref="SetupBundles"/>
	/// to configure authentication for your application, add a static helper method that creates the class and calls <see cref="Setup"/> then add
	/// a call to the static helper method in your <c>global.asax.cs</c> file.
	/// </para>
	/// </summary>
	/// <remarks>
	/// Our general approach for application startup configuration is to derive from a library base class designed to provide the necessary configuration
	/// functionality, add our application-specific functionality and then providing a static Configure method that instantiates a new instance
	/// of the class and calls Setup. The example provides a template class, ready to use in an MVC application. Information on configuring bundling
	/// can be found at <see href="http://go.microsoft.com/fwlink/?LinkId=301862"/>.
	/// </remarks>
	/// <example>
	/// <code language="cs" source="..\..\SimpleSyndicate.Mvc\SimpleSyndicate.Mvc.Help.Examples\App_Start\BundleConfiguration.cs" />
	/// </example>
	public abstract class BundleConfigurationBase
	{
		/// <summary>
		/// Sets up the bundle configuration; this method will call <see cref="SetupBundles"/> which should be overridden by derived classes
		/// to perform any application-specific bundle configuration.
		/// </summary>
		protected void Setup()
		{
			this.SetupBundles(BundleTable.Bundles);
		}

		/// <summary>
		/// Performs any application-specific bundle configuration; this method should be overridden in derived classes.
		/// </summary>
		/// <param name="bundles">The current bundles.</param>
		protected virtual void SetupBundles(BundleCollection bundles)
		{
		}
	}
}