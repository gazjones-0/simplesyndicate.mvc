﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using SimpleInjector;
using SimpleInjector.Integration.Web;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <content>
	/// Contains Container related functionality.
	/// </content>
	public abstract partial class SimpleInjectorConfigurationBase<TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager>
		where TIdentityDbContext : IdentityDbContext<TApplicationUser>
		where TApplicationUser : IdentityUser
		where TApplicationUserManager : UserManager<TApplicationUser>
		where TApplicationSignInManager : SignInManager<TApplicationUser, string>
	{
		/// <summary>
		/// Backing variable for <see cref="Container"/>.
		/// </summary>
		private static Container container = null;

		/// <summary>
		/// Performs any application-specific Simple Injector configuration on the <see cref="Container"/>; this method should be overridden in derived classes, and is
		/// called automatically when <see cref="Setup"/> is called, immediately after <see cref="RegisterRepositories"/> has been called but before
		/// the MVC controllers are registered (via <see cref="RegisterMvcControllersInCallingAssembly"/> and <see cref="RegisterMvcControllersInExecutingAssembly"/>).
		/// </summary>
		/// <param name="appBuilder">The app builder being used.</param>
		protected virtual void SetupContainer(IAppBuilder appBuilder)
		{
		}

		/// <summary>
		/// Creates the Simple Injector container; the default scoped lifestyle is set to <see cref="WebRequestLifestyle"/>.
		/// </summary>
		private static void CreateContainer()
		{
			container = new Container();
			container.Options.DefaultScopedLifestyle = new WebRequestLifestyle();
		}
	}
}