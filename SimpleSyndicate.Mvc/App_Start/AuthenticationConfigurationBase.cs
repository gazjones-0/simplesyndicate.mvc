﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Owin;
using SimpleSyndicate.Mvc.AspNet.Identity;
using SimpleSyndicate.Mvc.Models;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// <para>
	/// Base class for performing authentication configuration during application startup; this class is suitable for most MVC applications, and uses
	/// <see cref="ApplicationUser"/> as the Entity Framework user and <see cref="ApplicationUserManager"/>for ASP.Net Identity user management.
	/// </para>
	/// <para>
	/// To use this class, derive from it, customize <see cref="AuthenticationConfigurationBase{TApplicationUser, TApplicationUserManager}.SetupAuthentication"/>
	/// to configure authentication for your application, add a static helper method that creates the class and calls
	/// <see cref="AuthenticationConfigurationBase{TApplicationUser, TApplicationUserManager}.Setup"/>
	/// then add a call to the static helper method in your <c>global.asax.cs</c> file.
	/// </para>
	/// <para>
	/// <code language="cs" source="..\..\SimpleSyndicate.Mvc\SimpleSyndicate.Mvc.Help.Examples\App_Start\AuthenticationConfiguration.cs" />
	/// </para>
	/// </summary>
	/// <remarks>
	/// Our general approach for application startup configuration is to derive from a library base class designed to provide the necessary configuration
	/// functionality, add our application-specific functionality and then providing a static Configure method that instantiates a new instance
	/// of the class and calls Setup. The example provides a template class, ready to use in an MVC application.
	/// </remarks>
	/// <example>
	/// <code language="cs" source="..\..\SimpleSyndicate.Mvc\SimpleSyndicate.Mvc.Help.Examples\App_Start\AuthenticationConfiguration.cs" />
	/// </example>
	public abstract class AuthenticationConfigurationBase : AuthenticationConfigurationBase<ApplicationUser, ApplicationUserManager>
	{
	}
}
