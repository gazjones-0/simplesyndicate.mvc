﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Owin;
using SimpleSyndicate.Mvc.AspNet.Identity;
using SimpleSyndicate.Mvc.Models;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// <para>
	/// Base class for performing authentication configuration during application startup; this class allows all the various component types to be configured,
	/// but most MVC applications won't need this level of flexibility and should use <see cref="AuthenticationConfigurationBase"/> instead.
	/// </para>
	/// <para>
	/// If you need a more custom <typeparamref name="TApplicationUser"/> or <typeparamref name="TApplicationUserManager"/> than already provided by
	/// SimpleSyndicate.Mvc then derive from this class, customize <see cref="SetupAuthentication"/> and, if required, <see cref="CreateAuthenticationConfigurerFactory"/>
	/// for your application, add a static helper method that creates the class and calls <see cref="Setup"/> then add a call to the static helper method
	/// in your <c>global.asax.cs</c> file.
	/// </para>
	/// </summary>
	/// <typeparam name="TApplicationUser">An <see cref="IdentityUser"/>-derived type; a standard one suitable for MVC applications is <see cref="ApplicationUser"/>.</typeparam>
	/// <typeparam name="TApplicationUserManager">A <see cref="UserManager{TApplicationUser}"/>-derived type; a standard one suitable for MVC applications is <see cref="ApplicationUserManager"/>.</typeparam>
	/// <remarks>
	/// Our general approach for application startup configuration is to derive from a library base class designed to provide the necessary configuration
	/// functionality, add our application-specific functionality and then providing a static Configure method that instantiates a new instance
	/// of the class and calls Setup. The example provides a template class, ready to use in an MVC application.
	/// </remarks>
	/// <example>
	/// <code language="cs" source="..\..\SimpleSyndicate.Mvc\SimpleSyndicate.Mvc.Help.Examples\App_Start\AuthenticationConfiguration.cs" />
	/// </example>
	public abstract class AuthenticationConfigurationBase<TApplicationUser, TApplicationUserManager> : IAuthenticationConfiguration
		where TApplicationUser : IdentityUser, IGenerateUserIdentityAsync<TApplicationUser>
		where TApplicationUserManager : UserManager<TApplicationUser>
	{
		/// <summary>
		/// Gets the <see cref="IAuthenticationConfigurer"/> set up when <see cref="Setup"/> was called, so that it can be used to easily configure
		/// authentication; it is expected that this will be used by <see cref="SetupAuthentication"/> when overridden in derived classes.
		/// </summary>
		/// <value>An object that implements <see cref="IAuthenticationConfigurer"/>.</value>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Configurer", Justification = "Identifier is spelt correctly.")]
		protected IAuthenticationConfigurer AuthenticationConfigurer { get; private set; }

		/// <summary>
		/// Sets up the authentication configuration; this method will call <see cref="SetupAuthentication"/> which should be overridden by derived classes
		/// to perform any application-specific authentication configuration. The <see cref="AuthenticationConfigurer"/> property will be set up before
		/// <see cref="SetupAuthentication"/> is called so that it can be used to more easily configure authentication.
		/// </summary>
		/// <param name="appBuilder">The app builder being used.</param>
		public void Setup(IAppBuilder appBuilder)
		{
			this.AuthenticationConfigurer = this.CreateAuthenticationConfigurerFactory(appBuilder);
			this.SetupAuthentication(appBuilder);
		}

		/// <summary>
		/// Performs any application-specific authentication configuration; this method should be overridden in derived classes.
		/// The <see cref="AuthenticationConfigurer"/> property is available to easily configure authentication.
		/// </summary>
		/// <param name="appBuilder">The app builder being used.</param>
		protected virtual void SetupAuthentication(IAppBuilder appBuilder)
		{
		}

		/// <summary>
		/// Creates an instance that implements <see cref="IAuthenticationConfigurer"/>, that derived classes can use through
		/// <see cref="AuthenticationConfigurer"/>; you should only need to override this if you have a very custom <typeparamref name="TApplicationUser"/>
		/// or <typeparamref name="TApplicationUserManager"/>.
		/// </summary>
		/// <param name="appBuilder">The app builder being used.</param>
		/// <returns>An object that implements <see cref="IAuthenticationConfigurer"/>.</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Configurer", Justification = "Identifier is spelt correctly.")]
		protected virtual IAuthenticationConfigurer CreateAuthenticationConfigurerFactory(IAppBuilder appBuilder)
		{
			return AuthenticationConfigurerFactory<TApplicationUser, TApplicationUserManager>.Create(appBuilder);
		}
	}
}