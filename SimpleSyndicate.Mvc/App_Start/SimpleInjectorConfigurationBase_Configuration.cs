﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using SimpleInjector;
using SimpleInjector.Diagnostics;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <content>
	/// Contains configuration related functionality.
	/// </content>
	public abstract partial class SimpleInjectorConfigurationBase<TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager>
		where TIdentityDbContext : IdentityDbContext<TApplicationUser>
		where TApplicationUser : IdentityUser
		where TApplicationUserManager : UserManager<TApplicationUser>
		where TApplicationSignInManager : SignInManager<TApplicationUser, string>
	{
		/// <summary>
		/// Checks the Simple Injector configuration of the <see cref="Container"/> using <see cref="SimpleInjector.Container.Verify()"/>; this is called automatically
		/// when <see cref="Setup"/> is called, immediately after <see cref="RegisterMvcIntegratedFilterProvider"/> has been called.
		/// </summary>
		/// <exception cref="InvalidOperationException">Thrown when there is no Simple Injector container (i.e. <see cref="Setup"/> has not been called, or it failed).</exception>
		/// <exception cref="DiagnosticVerificationException">Thrown when the configuration is not valid.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Class is abstract so users won't access through a generic.")]
		public static void CheckConfiguration()
		{
			if (container == null)
			{
				throw new InvalidOperationException();
			}
			try
			{
				container.Verify();
			}
			catch (DiagnosticVerificationException e)
			{
				throw new DiagnosticVerificationException(GenerateDiagnosticResultMessage(from result in Analyzer.Analyze(container) select result), e);
			}
		}

		/// <summary>
		/// Analyzes the Simple Injector configuration of the <see cref="Container"/> using <see cref="SimpleInjector.Diagnostics.Analyzer.Analyze"/>; this
		/// is not called automatically when <see cref="Setup"/> is called as any issues diagnosed might not be critical.
		/// </summary>
		/// <exception cref="InvalidOperationException">Thrown when there is no Simple Injector container (i.e. <see cref="Setup"/> has not been called, or it failed).</exception>
		/// <exception cref="DiagnosticVerificationException">Thrown when the analysis has diagnosed an issue.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Class is abstract so users won't access through a generic.")]
		public static void AnalyzeConfiguration()
		{
			if (container == null)
			{
				throw new InvalidOperationException();
			}
			var results =
				from result in Analyzer.Analyze(container)
				select result;
			if (results.Any())
			{
				throw new DiagnosticVerificationException(GenerateDiagnosticResultMessage(results));
			}
		}

		/// <summary>
		/// Generates a human-readable message based on the supplied diagnostic results.
		/// </summary>
		/// <param name="results">Collection of <see cref="DiagnosticResult"/>.</param>
		/// <returns>The diagnostic results formatted as a single message, or an empty string if there were no results.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="results"/> is <c>null</c>.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Class is abstract so users won't access through a generic.")]
		public static string GenerateDiagnosticResultMessage(IEnumerable<DiagnosticResult> results)
		{
			if (results == null)
			{
				throw new ArgumentNullException("results");
			}
			var message = String.Empty;
			foreach (var result in results)
			{
				message = message + result.Severity.ToString() + ": " + TypeHelpers.FullNameFromCodeDom(result.ServiceType) + Environment.NewLine
					+ result.DiagnosticType.ToString() + ": " + result.Description + Environment.NewLine
					+ result.DocumentationUrl.ToString() + Environment.NewLine;
				switch (result.DiagnosticType)
				{
					case DiagnosticType.ContainerRegisteredComponent:
						foreach (var relationship in ((ContainerRegisteredServiceDiagnosticResult)result).Relationships)
						{
							message = message + String.Join(String.Empty, relationship.Dependency.VisualizeObjectGraph().Split('\n').Select(l => l.Trim())) + Environment.NewLine;
						}
						break;

					case DiagnosticType.DisposableTransientComponent:
						message = message + String.Join(String.Empty, ((DisposableTransientComponentDiagnosticResult)result).Registration.VisualizeObjectGraph().Split('\n').Select(l => l.Trim())) + Environment.NewLine;
						break;
				}
				message = message + Environment.NewLine;
			}
			return message;
		}
	}
}