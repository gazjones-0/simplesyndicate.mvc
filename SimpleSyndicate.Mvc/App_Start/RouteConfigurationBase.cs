﻿using System.Web.Routing;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// <para>
	/// Base class for performing route configuration during application startup; derive from this class, customize <see cref="SetupRoutes"/>
	/// to configure authentication for your application, add a static helper method that creates the class and calls <see cref="Setup"/> then add
	/// a call to the static helper method in your <c>global.asax.cs</c> file.
	/// </para>
	/// </summary>
	/// <remarks>
	/// Our general approach for application startup configuration is to derive from a library base class designed to provide the necessary configuration
	/// functionality, add our application-specific functionality and then providing a static Configure method that instantiates a new instance
	/// of the class and calls Setup. The example provides a template class, ready to use in an MVC application.
	/// </remarks>
	/// <example>
	/// <code language="cs" source="..\..\SimpleSyndicate.Mvc\SimpleSyndicate.Mvc.Help.Examples\App_Start\RouteConfiguration.cs" />
	/// </example>
	public abstract class RouteConfigurationBase
	{
		/// <summary>
		/// Sets up the route configuration; this method will call <see cref="SetupRoutes"/> which should be overridden by derived classes
		/// to perform any application-specific route configuration.
		/// </summary>
		protected void Setup()
		{
			this.SetupRoutes(RouteTable.Routes);
		}

		/// <summary>
		/// Performs any application-specific route configuration; this method should be overridden in derived classes.
		/// </summary>
		/// <param name="routes">The current routes.</param>
		protected virtual void SetupRoutes(RouteCollection routes)
		{
		}
	}
}