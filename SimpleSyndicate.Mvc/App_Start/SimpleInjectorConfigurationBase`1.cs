﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using SimpleSyndicate.Collections.ObjectModel;
using SimpleSyndicate.Mvc.AspNet.Identity;
using SimpleSyndicate.Mvc.Models;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// <para>
	/// Base class for performing Simple Injector configuration during application startup; this class is suitable for most MVC applications, and uses
	/// <see cref="ApplicationUser"/> as the Entity Framework user and <see cref="ApplicationUserManager"/> and <see cref="ApplicationSignInManager"/>
	/// for ASP.Net Identity user management.
	/// </para>
	/// <para>
	/// To use this class, derive from it, customize <see cref="SimpleInjectorConfigurationBase{TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager}.SetupAspNetIdentityConfiguration"/>
	/// and <see cref="SimpleInjectorConfigurationBase{TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager}.SetupContainer"/>
	/// to configure Simple Injector for your application, add a static helper method that creates the class and calls
	/// <see cref="SimpleInjectorConfigurationBase{TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager}.Setup"/>
	/// then add a call to the static helper method in your <c>global.asax.cs</c> file.
	/// </para>
	/// </summary>
	/// <typeparam name="TIdentityDbContext">A <see cref="System.Data.Entity.DbContext"/>-derived type; for MVC applications this is typically <c>ApplicationDbContext</c>.</typeparam>
	/// <remarks>
	/// Our general approach for application startup configuration is to derive from a library base class designed to provide the necessary configuration
	/// functionality, add our application-specific functionality and then providing a static Configure method that instantiates a new instance
	/// of the class and calls Setup. The example provides a template class, ready to use in an MVC application.
	/// </remarks>
	/// <example>
	/// <code language="cs" source="..\..\SimpleSyndicate.Mvc\SimpleSyndicate.Mvc.Help.Examples\App_Start\SimpleInjectorConfiguration.cs" />
	/// </example>
	public abstract class SimpleInjectorConfigurationBase<TIdentityDbContext> : SimpleInjectorConfigurationBase<TIdentityDbContext, ApplicationUser, ApplicationUserManager, ApplicationSignInManager>
		where TIdentityDbContext : IdentityDbContext<ApplicationUser>
	{
	}
}