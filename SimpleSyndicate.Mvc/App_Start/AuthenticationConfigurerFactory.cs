﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Owin;
using SimpleSyndicate.Mvc.AspNet.Identity;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// Factory class for creating objects that implement <see cref="IAuthenticationConfigurer"/>; used by
	/// <see cref="AuthenticationConfigurationBase{TApplicationUser, TApplicationUserManager}"/>.
	/// </summary>
	/// <typeparam name="TApplicationUser">An <see cref="IdentityUser"/>-derived type; a standard one suitable for MVC applications is <see cref="SimpleSyndicate.Mvc.Models.ApplicationUser"/>.</typeparam>
	/// <typeparam name="TApplicationUserManager">A <see cref="UserManager{TApplicationUser}"/>-derived type; a standard one suitable for MVC applications is <see cref="ApplicationUserManager"/>.</typeparam>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Configurer", Justification = "Identifier is spelt correctly.")]
	public static class AuthenticationConfigurerFactory<TApplicationUser, TApplicationUserManager>
		where TApplicationUser : IdentityUser, IGenerateUserIdentityAsync<TApplicationUser>
		where TApplicationUserManager : UserManager<TApplicationUser>
	{
		/// <summary>
		/// Creates an instance that implements <see cref="IAuthenticationConfigurer"/>.
		/// </summary>
		/// <param name="appBuilder">The app builder being used.</param>
		/// <returns>An instance that implements <see cref="IAuthenticationConfigurer"/>.</returns>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "This is a factory method, and generally will only be used internally.")]
		public static IAuthenticationConfigurer Create(IAppBuilder appBuilder)
		{
			return new AuthenticationConfigurer<TApplicationUser, TApplicationUserManager>(appBuilder);
		}
	}
}
