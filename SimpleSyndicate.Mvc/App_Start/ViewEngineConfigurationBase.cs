﻿using System.Web.Mvc;
using MvcCodeRouting;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// <para>
	/// Base class for performing view engine configuration during application startup; derive from this class, customize <see cref="SetupViewEngines"/>
	/// to configure authentication for your application, add a static helper method that creates the class and calls <see cref="Setup"/> then add
	/// a call to the static helper method in your <c>global.asax.cs</c> file; optionally add a call to <see cref="EnableCodeRouting"/> if you want
	/// to use MVC Code Routing.
	/// </para>
	/// </summary>
	/// <remarks>
	/// Our general approach for application startup configuration is to derive from a library base class designed to provide the necessary configuration
	/// functionality, add our application-specific functionality and then providing a static Configure method that instantiates a new instance
	/// of the class and calls Setup. The example provides a template class, ready to use in an MVC application.
	/// </remarks>
	/// <example>
	/// <code language="cs" source="..\..\SimpleSyndicate.Mvc\SimpleSyndicate.Mvc.Help.Examples\App_Start\ViewEngineConfiguration.cs" />
	/// </example>
	public abstract class ViewEngineConfigurationBase
	{
		/// <summary>
		/// Whether the view engines have been set up or not.
		/// </summary>
		private static bool viewEnginesSetup = false;

		/// <summary>
		/// Enables code routing using MVC Code Routing; this must only be done after <see cref="Setup"/> has been called.
		/// </summary>
		/// <exception cref="ViewEnginesMustBeSetupBeforeEnablingCodeRoutingException">Thrown if code routing is set up before view engines have been configured.</exception>
		public static void EnableCodeRouting()
		{
			// code routing must only be enabled after view engine configuration is complete
			if (!viewEnginesSetup)
			{
				throw new ViewEnginesMustBeSetupBeforeEnablingCodeRoutingException();
			}
			ViewEngines.Engines.EnableCodeRouting();
		}

		/// <summary>
		/// Sets up the view engine configuration; this method will call <see cref="SetupViewEngines"/> which should be overridden by derived classes
		/// to perform any application-specific view engine configuration.
		/// </summary>
		public void Setup()
		{
			this.SetupViewEngines(ViewEngines.Engines);
			viewEnginesSetup = true;
		}

		/// <summary>
		/// Performs any application-specific view engine configuration; this method should be overridden in derived classes.
		/// </summary>
		/// <param name="viewEngines">The current view engines.</param>
		protected virtual void SetupViewEngines(ViewEngineCollection viewEngines)
		{
		}
	}
}