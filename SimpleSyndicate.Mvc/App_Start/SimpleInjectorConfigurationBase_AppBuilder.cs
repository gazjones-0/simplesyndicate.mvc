﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Owin;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <content>
	/// Contains AppBuilder-related functionality.
	/// </content>
	public abstract partial class SimpleInjectorConfigurationBase<TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager>
		where TIdentityDbContext : IdentityDbContext<TApplicationUser>
		where TApplicationUser : IdentityUser
		where TApplicationUserManager : UserManager<TApplicationUser>
		where TApplicationSignInManager : SignInManager<TApplicationUser, string>
	{
		/// <summary>
		/// Registers the app builder being used as a single instance with the <see cref="Container"/>; this is called automatically when <see cref="Setup"/> is called,
		/// immediately after <see cref="ClearRegisteredAssemblies"/> has been called but before <see cref="RegisterAspNetIdentityComponents"/> is called.
		/// </summary>
		/// <param name="appBuilder">The app builder in use; this will be registered with the <see cref="Container"/> as a single instance.</param>
		protected virtual void RegisterAppBuilder(IAppBuilder appBuilder)
		{
			container.RegisterSingleton<IAppBuilder>(appBuilder);
		}
	}
}