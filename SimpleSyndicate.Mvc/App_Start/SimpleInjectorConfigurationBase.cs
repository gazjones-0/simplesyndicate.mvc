﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Owin;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using SimpleSyndicate.Collections.ObjectModel;
using SimpleSyndicate.Mvc.AspNet.Identity;
using SimpleSyndicate.Mvc.Models;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// <para>
	/// Base class for performing Simple Injector configuration during application startup; this class allows all the various component types to be configured,
	/// but most MVC applications won't need this level of flexibility and should use <see cref="SimpleInjectorConfigurationBase{TIdentityDbContext}"/> instead.
	/// </para>
	/// <para>
	/// If you need a more custom <typeparamref name="TApplicationUser"/>, <typeparamref name="TApplicationUserManager"/> or <typeparamref name="TApplicationSignInManager"/>
	/// than already provided by SimpleSyndicate.Mvc then derive from this class, customize <see cref="SetupAspNetIdentityConfiguration"/> and <see cref="SetupContainer"/>
	/// and whatever other methods you deem necessary to configure Simple Injector for your application, add a static helper method that creates the class and calls
	/// <see cref="Setup"/> then add a call to the static helper method in your <c>global.asax.cs</c> file.
	/// </para>
	/// </summary>
	/// <typeparam name="TIdentityDbContext">A <see cref="System.Data.Entity.DbContext"/>-derived type; for MVC applications this is typically <c>ApplicationDbContext</c>.</typeparam>
	/// <typeparam name="TApplicationUser">An <see cref="IdentityUser"/>-derived type; a standard one suitable for MVC applications is <see cref="ApplicationUser"/>.</typeparam>
	/// <typeparam name="TApplicationUserManager">A <see cref="UserManager{TApplicationUser}"/>-derived type; a standard one suitable for MVC applications is <see cref="ApplicationUserManager"/>.</typeparam>
	/// <typeparam name="TApplicationSignInManager">A <see cref="SignInManager{TUser, TKey}"/>-derived type; a standard one suitable for MVC applications is <see cref="ApplicationSignInManager"/>.</typeparam>
	/// <remarks>
	/// Our general approach for application startup configuration is to derive from a library base class designed to provide the necessary configuration
	/// functionality, add our application-specific functionality and then providing a static Configure method that instantiates a new instance
	/// of the class and calls Setup. The example provides a template class, ready to use in an MVC application.
	/// </remarks>
	/// <example>
	/// <code language="cs" source="..\..\SimpleSyndicate.Mvc\SimpleSyndicate.Mvc.Help.Examples\App_Start\SimpleInjectorConfiguration.cs" />
	/// </example>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1005:AvoidExcessiveParametersOnGenericTypes", Justification = "Number of parameters constrained by design; most users will use derived version which has one parameter")]
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Maintainability", "CA1506:AvoidExcessiveClassCoupling", Justification = "Coupling is constrained by design; class must know about / use lots of other types to perform its function")]
	public abstract partial class SimpleInjectorConfigurationBase<TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager>
		where TIdentityDbContext : IdentityDbContext<TApplicationUser>
		where TApplicationUser : IdentityUser
		where TApplicationUserManager : UserManager<TApplicationUser>
		where TApplicationSignInManager : SignInManager<TApplicationUser, string>
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="SimpleInjectorConfigurationBase{TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager}"/> class.
		/// </summary>
		protected SimpleInjectorConfigurationBase()
		{
			this.AspNetIdentityConfiguration = new IdentityConfiguration();
		}

		/// <summary>
		/// Gets the <see cref="SimpleInjector.Container"/> that was created when <see cref="Setup"/> was called.
		/// </summary>
		/// <value>A <see cref="SimpleInjector.Container"/> object.</value>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1000:DoNotDeclareStaticMembersOnGenericTypes", Justification = "Class is abstract so users won't access through a generic.")]
		public static Container Container
		{
			get
			{
				return container;
			}
		}

		/// <summary>
		/// Gets or sets the <see cref="IdentityConfiguration"/> to use for the ASP.Net Identity components; this is used to configure the
		/// <typeparamref name="TApplicationSignInManager"/> when it is initialized by <see cref="InitializeApplicationUserManager"/>.
		/// </summary>
		/// <value><see cref="IdentityConfiguration"/> to use for the ASP.Net Identity components.</value>
		public IdentityConfiguration AspNetIdentityConfiguration { get; set; }

		/// <summary>
		/// Re-sets up the <see cref="Container"/> by setting it to <c>null</c> and then calling <see cref="Setup"/>; the
		/// <see cref="IAppBuilder"/> used for the re-setup is the one that was originally provided to <see cref="Setup"/>.
		/// </summary>
		/// <exception cref="InvalidOperationException">Thrown if <see cref="Setup"/> has not already been called.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Re", Justification = "Re is the appropriate casing.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "ReSetup", Justification = "ReSetup is spelt correctly.")]
		public void ReSetup()
		{
			if (container == null)
			{
				throw new InvalidOperationException("Simple Injector container is null; Setup has not been called prior to ReSetup being called.");
			}
			IAppBuilder appBuilder = container.GetInstance<IAppBuilder>();
			this.ReSetup(appBuilder, this.savedIdentityDbContextInstanceProducer);
		}

		/// <summary>
		/// Re-sets up the <see cref="Container"/> by setting it to <c>null</c> and then calling <see cref="Setup"/>.
		/// </summary>
		/// <param name="appBuilder">The app builder in use; this will be registered with the <see cref="Container"/> as a single instance.</param>
		/// <param name="identityDbContextInstanceCreator">Delegate that creates instances of <typeparamref name="TIdentityDbContext"/>.</param>
		/// <exception cref="InvalidOperationException">Thrown if <see cref="Setup"/> has not already been called.</exception>
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1709:IdentifiersShouldBeCasedCorrectly", MessageId = "Re", Justification = "Re is the appropriate casing.")]
		[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA2204:Literals should be spelled correctly", MessageId = "ReSetup", Justification = "ReSetup is spelt correctly.")]
		public void ReSetup(IAppBuilder appBuilder, Func<TIdentityDbContext> identityDbContextInstanceCreator)
		{
			if (container == null)
			{
				throw new InvalidOperationException("Simple Injector container is null; Setup has not been called prior to ReSetup being called.");
			}

			// if SimpleInjector was previously set up, it will have been registered with MVC as an integrated filter provider,
			// but there's no way to unregister it so we have to manually remove it before we re-set everything up; also, as
			// it's an internal class within SimpleInjector so we have to remove it by use of its type name
			FilterProviders.Providers.RemoveByTypeName("SimpleInjectorFilterAttributeFilterProvider");

			// re-set everything up
			container = null;
			this.Setup(appBuilder, identityDbContextInstanceCreator);
		}

		/// <summary>
		/// <para>
		/// Sets up the Simple Injector configuration; this method will call <see cref="SetupContainer"/> which should be overridden by derived classes
		/// to perform any application-specific Simple Injector configuration.
		/// </para>
		/// <para>
		/// The specified <paramref name="appBuilder"/> will be registered as a single instance (via <see cref="RegisterAppBuilder"/>), then the ASP.Net Identity components
		/// registered (see <see cref="RegisterAspNetIdentityComponents"/>) and set up (<see cref="SetupAspNetIdentityConfiguration"/>),  then
		/// <typeparamref name="TIdentityDbContext"/> registered (see <see cref="RegisterIdentityDbContext"/>) and then <see cref="SetupContainer"/> is called
		/// to perform application-specific configuration.
		/// </para>
		/// <para>
		/// After <see cref="SetupContainer"/> finishes, any MVC controllers in the calling and currently executing assemblies will be registered (see
		/// <see cref="RegisterMvcControllersInCallingAssembly"/> and <see cref="RegisterMvcControllersInExecutingAssembly"/>), the MVC integrated filter
		/// provider will be registered (see <see cref="RegisterMvcIntegratedFilterProvider"/>), the ASP.Net Identity components registered with Owin (see
		/// <see cref="RegisterAspNetIdentityComponentsWithOwin"/>, the configuration checked (see <see cref="CheckConfiguration"/>) and finally the
		/// <see cref="SimpleInjectorDependencyResolver"/> will be set as the MVC dependency resolver to use (see <see cref="SetMvcResolver"/>).
		/// </para>
		/// <para>
		/// Note that the configuration is <i>not</i> analyzed as part of setup as any issues diagnosed might not be critical; a helper method (see
		/// <see cref="AnalyzeConfiguration"/> is provided to perform diagnostics and it is recommended that you either call this from time to time
		/// or add it as part of your unit tests.
		/// </para>
		/// </summary>
		/// <param name="appBuilder">The app builder in use; this will be registered with the <see cref="Container"/> as a single instance.</param>
		/// <param name="identityDbContextInstanceCreator">Delegate that creates instances of <typeparamref name="TIdentityDbContext"/>.</param>
		/// <exception cref="InvalidOperationException">Thrown if <see cref="Setup"/> has already been called.</exception>
		protected virtual void Setup(IAppBuilder appBuilder, Func<TIdentityDbContext> identityDbContextInstanceCreator)
		{
			if (container != null)
			{
				throw new InvalidOperationException();
			}
			this.savedIdentityDbContextInstanceProducer = identityDbContextInstanceCreator;
			CreateContainer();
			this.ClearRegisteredAssemblies();
			this.RegisterAppBuilder(appBuilder);
			this.RegisterAspNetIdentityComponents();
			this.SetupAspNetIdentityConfiguration();
			this.RegisterIdentityDbContext(identityDbContextInstanceCreator);
			this.RegisterRepositories();
			this.SetupContainer(appBuilder);
			this.RegisterMvcControllersInCallingAssembly();
			this.RegisterMvcControllersInExecutingAssembly();
			this.RegisterMvcIntegratedFilterProvider();
			this.RegisterIdentityDbContextWithOwin();
			this.RegisterAspNetIdentityComponentsWithOwin();
			this.SetMvcResolver();
			CheckIdentityDbContextIsRegistered();
			CheckConfiguration();
		}
	}
}
