﻿using System;
using System.Web;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.DataProtection;
using Owin;
using SimpleInjector;
using SimpleInjector.Advanced;
using SimpleSyndicate.Mvc.AspNet.Identity;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <content>
	/// Contains Asp.Net Identity-related functionality.
	/// </content>
	public abstract partial class SimpleInjectorConfigurationBase<TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager>
		where TIdentityDbContext : IdentityDbContext<TApplicationUser>
		where TApplicationUser : IdentityUser
		where TApplicationUserManager : UserManager<TApplicationUser>
		where TApplicationSignInManager : SignInManager<TApplicationUser, string>
	{
		/// <summary>
		/// <para>
		/// Registers the ASP.Net Identity components with the <see cref="Container"/>; this is called automatically when <see cref="Setup"/> is called, immediately
		/// after <see cref="RegisterAppBuilder"/> has been called but before <see cref="SetupAspNetIdentityConfiguration"/> is called.
		/// </para>
		/// <para>
		/// For completeness, this method calls <see cref="RegisterApplicationUserManager"/> first and then <see cref="RegisterApplicationSignInManager"/>;
		/// note that the ASP.Net Identity components aren't registered with Own until all container setup is complete (see <see cref="RegisterAspNetIdentityComponentsWithOwin"/>
		/// for the full details).
		/// </para>
		/// </summary>
		protected virtual void RegisterAspNetIdentityComponents()
		{
			this.RegisterApplicationUserManager();
			this.RegisterApplicationSignInManager();
		}

		/// <summary>
		/// <para>
		/// Registers the ASP.Net Identity application user manager components -- registers <typeparamref name="TApplicationUserManager"/> as per web request,
		/// <see cref="IUserStore{TApplicationUser}"/> as per web request (using <see cref="CreateUserStore"/> to create instances of it) and
		/// <see cref="IRoleStore{TIdentityRole}"/> as per web request ; this is called automatically when
		/// <see cref="Setup"/> is called (via <see cref="RegisterAspNetIdentityComponents"/>), immediately after <see cref="RegisterAppBuilder"/> has been
		/// called but before <see cref="SetupAspNetIdentityConfiguration"/> is called.
		/// </para>
		/// <para>
		/// For completeness, <see cref="RegisterAspNetIdentityComponents"/> calls <see cref="RegisterApplicationUserManager"/> first and then
		/// <see cref="RegisterApplicationSignInManager"/>.
		/// </para>
		/// <para>
		/// Note that the ASP.Net Identity components aren't registered with Own until all container setup is complete (see <see cref="RegisterAspNetIdentityComponentsWithOwin"/>
		/// for the full details).
		/// </para>
		/// </summary>
		protected virtual void RegisterApplicationUserManager()
		{
			container.RegisterPerWebRequest<TApplicationUserManager>();
			container.RegisterInitializer<TApplicationUserManager>(manager => this.InitializeApplicationUserManager(manager));
			container.RegisterPerWebRequest<IUserStore<TApplicationUser>>(() => this.CreateUserStore());
			container.RegisterPerWebRequest<IRoleStore<IdentityRole, string>>(() => this.CreateRoleStore());
		}

		/// <summary>
		/// Initializes the <typeparamref name="TApplicationUserManager"/> whenever a new instance is created, using the details in
		/// <see cref="AspNetIdentityConfiguration"/> for the configuration; applications should set these details by overriding
		/// <see cref="SetupAspNetIdentityConfiguration"/> in derived classes.
		/// </summary>
		/// <param name="applicationUserManager">The <typeparamref name="TApplicationUserManager"/> to initialize.</param>
		protected virtual void InitializeApplicationUserManager(TApplicationUserManager applicationUserManager)
		{
			// configure validation logic for usernames
			applicationUserManager.UserValidator = new UserValidator<TApplicationUser>(applicationUserManager)
			{
				AllowOnlyAlphanumericUserNames = this.AspNetIdentityConfiguration.UserValidator.AllowOnlyAlphanumericUserNames,
				RequireUniqueEmail = this.AspNetIdentityConfiguration.UserValidator.RequireUniqueEmail
			};

			// configure validation logic for passwords
			applicationUserManager.PasswordValidator = new PasswordValidator
			{
				RequireDigit = this.AspNetIdentityConfiguration.PasswordValidator.RequireDigit,
				RequiredLength = this.AspNetIdentityConfiguration.PasswordValidator.RequiredLength,
				RequireLowercase = this.AspNetIdentityConfiguration.PasswordValidator.RequireLowercase,
				RequireNonLetterOrDigit = this.AspNetIdentityConfiguration.PasswordValidator.RequireNonLetterOrDigit,
				RequireUppercase = this.AspNetIdentityConfiguration.PasswordValidator.RequireUppercase
			};

			// configure user lockout defaults
			applicationUserManager.UserLockoutEnabledByDefault = this.AspNetIdentityConfiguration.UserLockout.UserLockoutEnabledByDefault;
			applicationUserManager.DefaultAccountLockoutTimeSpan = this.AspNetIdentityConfiguration.UserLockout.DefaultAccountLockoutTimeSpan;
			applicationUserManager.MaxFailedAccessAttemptsBeforeLockout = this.AspNetIdentityConfiguration.UserLockout.MaxFailedAccessAttemptsBeforeLockout;

			// register two factor authentication providers
			applicationUserManager.RegisterTwoFactorProvider(
				"Phone Code",
				new PhoneNumberTokenProvider<TApplicationUser>
				{
					MessageFormat = this.AspNetIdentityConfiguration.TwoFactorProviders.PhoneCode.MessageFormat
				});
			applicationUserManager.RegisterTwoFactorProvider(
				"Email Code",
				new EmailTokenProvider<TApplicationUser>
				{
					Subject = this.AspNetIdentityConfiguration.TwoFactorProviders.EmailCode.Subject,
					BodyFormat = this.AspNetIdentityConfiguration.TwoFactorProviders.EmailCode.BodyFormat
				});
			applicationUserManager.EmailService = this.AspNetIdentityConfiguration.TwoFactorProviders.EmailCode.EmailService;
			applicationUserManager.SmsService = this.AspNetIdentityConfiguration.TwoFactorProviders.PhoneCode.SmsService;

			// register data protection provider
			var dataProtectionProvider = container.GetInstance<IAppBuilder>().GetDataProtectionProvider();
			if (dataProtectionProvider != null)
			{
				applicationUserManager.UserTokenProvider = new DataProtectorTokenProvider<TApplicationUser>(dataProtectionProvider.Create("ASP.NET Identity"));
			}
		}

		/// <summary>
		/// Creates instances of <see cref="IUserStore{TApplicationUser}"/> when they need to be injected by Simple Injector; the user store is created using a
		/// <typeparamref name="TIdentityDbContext"/> instance that has been registered with the <see cref="Container"/> as its backing store.
		/// </summary>
		/// <returns>An <see cref="IUserStore{TApplicationUser}"/> instance that uses a <typeparamref name="TIdentityDbContext"/> instance as its backing store.</returns>
		protected virtual IUserStore<TApplicationUser> CreateUserStore()
		{
			return new UserStore<TApplicationUser>(container.GetInstance<TIdentityDbContext>());
		}

		/// <summary>
		/// Creates instances of <see cref="IRoleStore{TRole, TKey}"/> when they need to be injected by Simple Injector; the role store is created using a
		/// <typeparamref name="TIdentityDbContext"/> instance that has been registered with the <see cref="Container"/> as its backing store.
		/// </summary>
		/// <returns>An <see cref="IRoleStore{TRole, TKey}"/> instance that uses a <typeparamref name="TIdentityDbContext"/> instance as its backing store.</returns>
		protected virtual IRoleStore<IdentityRole, string> CreateRoleStore()
		{
			return new RoleStore<IdentityRole>(container.GetInstance<TIdentityDbContext>());
		}

		/// <summary>
		/// <para>
		/// Registers the ASP.Net Identity application application sign in manager components -- registers <typeparamref name="TApplicationSignInManager"/>
		/// as per web request, and <see cref="IAuthenticationManager"/> as per web request using <see cref="CreateAuthenticationManager"/> to create instances of it;
		/// this is called automatically when <see cref="Setup"/> is called (via <see cref="RegisterAspNetIdentityComponents"/>), immediately after
		/// <see cref="RegisterAppBuilder"/> has been called but before <see cref="SetupAspNetIdentityConfiguration"/> is called.
		/// </para>
		/// <para>
		/// For completeness, <see cref="RegisterAspNetIdentityComponents"/> calls <see cref="RegisterApplicationUserManager"/> first and then
		/// <see cref="RegisterApplicationSignInManager"/>.
		/// </para>
		/// <para>
		/// Note that the ASP.Net Identity components aren't registered with Own until all container setup is complete (see <see cref="RegisterAspNetIdentityComponentsWithOwin"/>
		/// for the full details).
		/// </para>
		/// </summary>
		protected virtual void RegisterApplicationSignInManager()
		{
			var lifestyle = new SimpleInjector.Integration.Web.WebRequestLifestyle();
			var registration = lifestyle.CreateRegistration<TApplicationSignInManager>(container);
			container.AddRegistration(typeof(TApplicationSignInManager), registration);
			container.AddRegistration(typeof(SignInManager<TApplicationUser, string>), registration);
			container.RegisterPerWebRequest<IAuthenticationManager>(() => this.CreateAuthenticationManager());
		}

		/// <summary>
		/// Creates instances of <see cref="IAuthenticationManager"/> when they need to be injected by Simple Injector; authentication manager
		/// instances aren't actually created, instead the one from the current Owin context is retrieved.
		/// </summary>
		/// <returns>An <see cref="IAuthenticationManager"/> instance.</returns>
		protected virtual IAuthenticationManager CreateAuthenticationManager()
		{
			IOwinContext context = null;
			try
			{
				// if there's no Owin context, GetOwinContext will throw an invalid operation exception; however, if there's no HTTP context (e.g. because
				// we're being executed from a unit test) it'll never get that far and a null reference exception will get thrown. To handle this, if there's
				// no HTTP context we treat this as if there's no Owin context and throw an invalid operation exception (we don't catch the null reference
				// exception as there could be various reasons for this, whilst an invalid operation exception means something in this context)
				if (System.Web.HttpContext.Current == null)
				{
					throw new InvalidOperationException();
				}
				context = System.Web.HttpContext.Current.GetOwinContext();
			}
			catch (InvalidOperationException)
			{
				// no Owin context; however, this is expected if we're in the middle of verifying the container, so in that case return
				// a fake authentication manager (that doesn't actually implement anything) to use just whilst we verify the container
				if (container.IsVerifying())
				{
					return new NotImplementedAuthenticationManager();
				}
				throw;
			}
			return context.Authentication;
		}

		/// <summary>
		/// Performs any application-specific ASP.Net Identity configuration on the <see cref="AspNetIdentityConfiguration"/>, which is used
		/// when injecting instances of the ASP.Net Identity components; this method should be overridden in derived classes, and is called
		/// automatically when <see cref="Setup"/> is called, immediately after <see cref="RegisterAspNetIdentityComponents"/> has been
		/// called but before <see cref="RegisterIdentityDbContext"/> is called.
		/// </summary>
		protected virtual void SetupAspNetIdentityConfiguration()
		{
		}

		/// <summary>
		/// <para>
		/// Registers some of the ASP.Net Identity components that have been registered with the <see cref="Container"/> with Owin, which is necessary because
		/// Owin internally uses some of the components and tries to retrieve them from the Owin context; this is called automatically when <see cref="Setup"/>
		/// is called, immediately after <see cref="RegisterIdentityDbContextWithOwin"/> has been called but before <see cref="CheckIdentityDbContextIsRegistered"/>
		/// is called.
		/// </para>
		/// <para>
		/// Registration with Owin has to be done after all other setup has completed as it retrieves instances from the <see cref="Container"/>
		/// which will lock the container.
		/// </para>
		/// </summary>
		protected virtual void RegisterAspNetIdentityComponentsWithOwin()
		{
			container.GetInstance<IAppBuilder>().CreatePerOwinContext<TApplicationUserManager>(() => container.GetInstance<TApplicationUserManager>());
			container.GetInstance<IAppBuilder>().CreatePerOwinContext<TApplicationSignInManager>(() => container.GetInstance<TApplicationSignInManager>());
		}
	}
}