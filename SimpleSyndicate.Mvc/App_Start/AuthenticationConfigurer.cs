﻿using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.Facebook;
using Microsoft.Owin.Security.MicrosoftAccount;
using Owin;
using SimpleSyndicate.Mvc.AspNet.Identity;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// Concrete implementation of <see cref="IAuthenticationConfigurer"/>; use <see cref="AuthenticationConfigurerFactory{TApplicationUser, TApplicationUserManager}"/> to create.
	/// </summary>
	/// <typeparam name="TApplicationUser">An <see cref="IdentityUser"/>-derived type; a standard one suitable for MVC applications is <see cref="SimpleSyndicate.Mvc.Models.ApplicationUser"/>.</typeparam>
	/// <typeparam name="TApplicationUserManager">A <see cref="UserManager{TApplicationUser}"/>-derived type; a standard one suitable for MVC applications is <see cref="ApplicationUserManager"/>.</typeparam>
	internal class AuthenticationConfigurer<TApplicationUser, TApplicationUserManager> : IAuthenticationConfigurer
		where TApplicationUser : IdentityUser, IGenerateUserIdentityAsync<TApplicationUser>
		where TApplicationUserManager : UserManager<TApplicationUser>
	{
		/// <summary>
		/// Backing variable for the <see cref="IAppBuilder"/> that is being configured.
		/// </summary>
		private IAppBuilder appBuilder;

		/// <summary>
		/// Initializes a new instance of the <see cref="AuthenticationConfigurer{TApplicationUser, TApplicationUserManager}"/> class using the specified app builder.
		/// </summary>
		/// <param name="appBuilder">The app builder to configure.</param>
		public AuthenticationConfigurer(IAppBuilder appBuilder)
		{
			this.appBuilder = appBuilder;
		}

		/// <overloads>
		/// <summary>
		/// Enables cookie authentication.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Enables cookie authentication using "/Account/Login" as the page to the login page.
		/// </summary>
		public void ConfigureCookie()
		{
			this.ConfigureCookie("/Account/Login");
		}

		/// <summary>
		/// Enables cookie authentication using the specified path for the login page.
		/// </summary>
		/// <param name="path">The path to the login page.</param>
		public void ConfigureCookie(string path)
		{
			// enable the application to use a cookie to store information for the signed in user
			this.appBuilder.UseCookieAuthentication(new CookieAuthenticationOptions
			{
				AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
				LoginPath = new PathString(path),
				Provider = new CookieAuthenticationProvider
				{
					// enable the application to validate the security stamp when the user logs in; this is a security feature
					// which is used when you change a password or add an external login to your account. 
					OnValidateIdentity = SecurityStampValidator.OnValidateIdentity<TApplicationUserManager, TApplicationUser>(
						validateInterval: TimeSpan.FromMinutes(30),
						regenerateIdentity: (manager, user) => user.GenerateUserIdentityAsync(manager))
				}
			});
			this.appBuilder.UseExternalSignInCookie(DefaultAuthenticationTypes.ExternalCookie);

			// enable the application to temporarily store user information when they are verifying the second factor in the two-factor authentication process
			this.appBuilder.UseTwoFactorSignInCookie(DefaultAuthenticationTypes.TwoFactorCookie, TimeSpan.FromMinutes(5));

			// enable the application to remember the second login verification factor such as phone or email
			this.appBuilder.UseTwoFactorRememberBrowserCookie(DefaultAuthenticationTypes.TwoFactorRememberBrowserCookie);
		}

		/// <overloads>
		/// <summary>
		/// Enables Facebook authentication.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Enables Facebook authentication using the specified application id and secret and, when the user authenticates via
		/// Facebook, asks for permission to know the email address they've registered with Facebook.
		/// </summary>
		/// <param name="appId">Application id.</param>
		/// <param name="appSecret">Application secret.</param>
		public void ConfigureFacebook(string appId, string appSecret)
		{
			this.ConfigureFacebook(appId, appSecret, true);
		}

		/// <summary>
		/// Enables Facebook authentication using the specified application id, secret and, when the user authenticates via Facebook,
		/// whether to ask permission to know the email address they've registered with Facebook.
		/// </summary>
		/// <param name="appId">Application id.</param>
		/// <param name="appSecret">Application secret.</param>
		/// <param name="addEmailScope">When a user authenticates, whether to ask for permission to know the email address they've registered with Facebook.</param>
		public void ConfigureFacebook(string appId, string appSecret, bool addEmailScope)
		{
			var facebookAuthenticationOptions = new FacebookAuthenticationOptions();
			facebookAuthenticationOptions.AppId = appId;
			facebookAuthenticationOptions.AppSecret = appSecret;
			if (addEmailScope)
			{
				facebookAuthenticationOptions.Scope.Add("email");
			}
			this.appBuilder.UseFacebookAuthentication(facebookAuthenticationOptions);
		}

		/// <summary>
		/// Enables Google authentication.
		/// </summary>
		public void ConfigureGoogle()
		{
			this.appBuilder.UseGoogleAuthentication();
		}

		/// <overloads>
		/// <summary>
		/// Enables Microsoft account authentication.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Enables Microsoft account authentication using the specified client id and secret and, when the user authenticates via
		/// Microsoft, asks for permission to know the email address they've registered with Microsoft.
		/// </summary>
		/// <param name="clientId">Client id.</param>
		/// <param name="clientSecret">Client secret.</param>
		public void ConfigureMicrosoft(string clientId, string clientSecret)
		{
			this.ConfigureMicrosoft(clientId, clientSecret, true);
		}

		/// <summary>
		/// Enables Microsoft account authentication using the specified client id, secret and, when the user authenticates via Microsoft,
		/// whether to ask permission to know the email address they've registered with Microsoft.
		/// </summary>
		/// <param name="clientId">Client id.</param>
		/// <param name="clientSecret">Client secret.</param>
		/// <param name="addEmailScope">When a user authenticates, whether to ask for permission to know the email address they've registered with Microsoft.</param>
		public void ConfigureMicrosoft(string clientId, string clientSecret, bool addEmailScope)
		{
			var microsoftAuthenticationOptions = new MicrosoftAccountAuthenticationOptions();
			microsoftAuthenticationOptions.ClientId = clientId;
			microsoftAuthenticationOptions.ClientSecret = clientSecret;
			if (addEmailScope)
			{
				microsoftAuthenticationOptions.Scope.Add("wl.emails");
			}
			this.appBuilder.UseMicrosoftAccountAuthentication(microsoftAuthenticationOptions);
		}

		/// <summary>
		/// Enables Twitter authentication using the specified consumer key and secret.
		/// </summary>
		/// <param name="consumerKey">Consumer key.</param>
		/// <param name="consumerSecret">Consumer secret.</param>
		public void ConfigureTwitter(string consumerKey, string consumerSecret)
		{
			this.appBuilder.UseTwitterAuthentication(consumerKey, consumerSecret);
		}
	}
}
