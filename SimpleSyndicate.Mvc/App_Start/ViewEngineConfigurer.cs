﻿using System.Web.Mvc;
using SimpleSyndicate.Mvc.Web.Mvc;

namespace SimpleSyndicate.Mvc.App_Start
{
	/// <summary>
	/// Provides functionality for configuring view engines.
	/// </summary>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1704:IdentifiersShouldBeSpelledCorrectly", MessageId = "Configurer", Justification = "Identifier is spelt correctly.")]
	public static class ViewEngineConfigurer
	{
		/// <summary>
		/// Registers a C#-specific razor view engine (<see cref="CSharpRazorViewEngine"/>) and removes any other view engines.
		/// </summary>
		public static void RegisterCSharpRazorViewEngineOnly()
		{
			ViewEngines.Engines.Clear();
			ViewEngines.Engines.Add(new CSharpRazorViewEngine());
		}
	}
}
