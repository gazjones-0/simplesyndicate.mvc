﻿using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSyndicate.Mvc.AspNet.Identity;

namespace SimpleSyndicate.Mvc.Models
{
	/// <summary>
	/// Represents a user in an MVC application, and should be suitable for most MVC applications; if you need something more custom, derive from this class
	/// and extend as required -- this will allow you to have the benefits of a custom user that can be used with all the SimpleSyndicate.Mvc functionality.
	/// </summary>
	public class ApplicationUser : IdentityUser, IGenerateUserIdentityAsync<ApplicationUser>
	{
		/// <overloads>
		/// <summary>
		/// Creates a <see cref="ClaimsIdentity"/> representing the user.
		/// </summary>
		/// </overloads>
		/// <summary>
		/// Creates a <see cref="ClaimsIdentity"/> representing the user, using <see cref="DefaultAuthenticationTypes.ApplicationCookie"/> as the authentication type.
		/// </summary>
		/// <param name="userManager">A <see cref="UserManager{TUser}"/> object that provides access to the user store.</param>
		/// <returns>A <see cref="ClaimsIdentity"/> representing the user.</returns>
		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> userManager)
		{
			return await this.GenerateUserIdentityAsync(userManager, DefaultAuthenticationTypes.ApplicationCookie);
		}

		/// <summary>
		/// Creates a <see cref="ClaimsIdentity"/> representing the user.
		/// </summary>
		/// <param name="userManager">A <see cref="UserManager{TUser}"/> object that provides access to the user store.</param>
		/// <param name="authenticationType">The authentication type; this must match the one defined in <see cref="Microsoft.Owin.Security.Cookies.CookieAuthenticationOptions"/>.</param>
		/// <returns>A <see cref="ClaimsIdentity"/> representing the user.</returns>
		public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> userManager, string authenticationType)
		{
			var userIdentity = await userManager.CreateIdentityAsync(this, authenticationType);
			return userIdentity;
		}
	}
}