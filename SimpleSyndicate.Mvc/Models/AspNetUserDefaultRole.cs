﻿using System.ComponentModel.DataAnnotations;

namespace SimpleSyndicate.Mvc.Models
{
	/// <summary>
	/// Represents a role that is assigned by default to any user that registers.
	/// </summary>
	[System.Data.Linq.Mapping.Table(Name = "AspNetUserDefaultRoles")]
	public class AspNetUserDefaultRole
	{
		/// <summary>
		/// Gets or sets the role id.
		/// </summary>
		/// <value>The role id.</value>
		[Key]
		[MaxLength(DataAnnotations.AspNetUserDefaultRoleDataAnnotations.RoleId.MaxLength)]
		public string RoleId { get; set; }
	}
}