﻿using System.ComponentModel.DataAnnotations;

namespace SimpleSyndicate.Mvc.Models
{
	/// <summary>
	/// Represents login details for a user in an MVC application.
	/// </summary>
	[System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login", Justification = "Login is used in existing Asp.Net class that this class provides additional information about")]
	[System.Data.Linq.Mapping.Table(Name = "AspNetUserLoginDetails")]
	public class AspNetUserLoginDetail
	{
		/// <summary>
		/// Gets or sets the user id.
		/// </summary>
		/// <value>The user id.</value>
		[Key]
		[MaxLength(DataAnnotations.AspNetUserLoginDetailDataAnnotations.UserId.MaxLength)]
		public string UserId { get; set; }

		/// <summary>
		/// Gets or sets the email address.
		/// </summary>
		/// <value>The email address.</value>
		[MaxLength(DataAnnotations.AspNetUserLoginDetailDataAnnotations.Email.MaxLength)]
		public string Email { get; set; }

		/// <summary>
		/// Gets or sets the host address used when user logged in.
		/// </summary>
		/// <value>The host address.</value>
		[MaxLength(DataAnnotations.AspNetUserLoginDetailDataAnnotations.Host.MaxLength)]
		public string Host { get; set; }
	}
}