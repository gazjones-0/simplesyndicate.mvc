﻿namespace SimpleSyndicate.Mvc.Models.DataAnnotations
{
	/// <summary>
	/// The <see cref="DataAnnotations"/> namespace contains classes that hold data annotations for the <see cref="Models"/>.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
