﻿namespace SimpleSyndicate.Mvc.Models.DataAnnotations.AspNetUserLoginDetailDataAnnotations
{
	/// <summary>
	/// Host data annotations.
	/// </summary>
	public static class Host
	{
		/// <summary>
		/// Maximum length of host.
		/// </summary>
		public const int MaxLength = 255;
	}
}
