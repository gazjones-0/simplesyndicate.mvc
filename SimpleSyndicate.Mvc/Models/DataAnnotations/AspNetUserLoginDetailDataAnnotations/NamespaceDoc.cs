﻿namespace SimpleSyndicate.Mvc.Models.DataAnnotations.AspNetUserLoginDetailDataAnnotations
{
	/// <summary>
	/// The <see cref="AspNetUserLoginDetailDataAnnotations"/> namespace contains classes that hold data annotations for the <see cref="Models.AspNetUserLoginDetail"/> model.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
