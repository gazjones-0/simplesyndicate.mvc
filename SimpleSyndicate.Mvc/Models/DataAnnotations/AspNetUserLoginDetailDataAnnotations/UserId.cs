﻿namespace SimpleSyndicate.Mvc.Models.DataAnnotations.AspNetUserLoginDetailDataAnnotations
{
	/// <summary>
	/// User id data annotations.
	/// </summary>
	public static class UserId
	{
		/// <summary>
		/// Maximum length of user id.
		/// </summary>
		public const int MaxLength = 128;
	}
}
