﻿namespace SimpleSyndicate.Mvc.Models.DataAnnotations.AspNetUserLoginDetailDataAnnotations
{
	/// <summary>
	/// Email data annotations.
	/// </summary>
	public static class Email
	{
		/// <summary>
		/// Maximum length of email address.
		/// </summary>
		public const int MaxLength = 255;
	}
}
