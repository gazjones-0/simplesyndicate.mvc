﻿namespace SimpleSyndicate.Mvc.Models.DataAnnotations.AspNetUserDefaultRoleDataAnnotations
{
	/// <summary>
	/// Role id data annotations.
	/// </summary>
	public static class RoleId
	{
		/// <summary>
		/// Maximum length of role id.
		/// </summary>
		public const int MaxLength = 128;
	}
}
