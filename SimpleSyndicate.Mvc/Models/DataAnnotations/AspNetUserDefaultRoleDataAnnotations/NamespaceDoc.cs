﻿namespace SimpleSyndicate.Mvc.Models.DataAnnotations.AspNetUserDefaultRoleDataAnnotations
{
	/// <summary>
	/// The <see cref="AspNetUserDefaultRoleDataAnnotations"/> namespace contains classes that hold data annotations for the <see cref="Models.AspNetUserDefaultRole"/> model.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}
