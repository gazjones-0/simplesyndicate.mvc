// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.App_Start.AutoMapperMappings", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "namespace", Target = "SimpleSyndicate.Mvc.App_Start.AutoMapperMappings", Justification = "Use of underscore in App_Start is the convention in MVC.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "namespace", Target = "SimpleSyndicate.Mvc.App_Start.Templates", Justification = "Use of underscore in App_Start is the convention in MVC.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1707:IdentifiersShouldNotContainUnderscores", Scope = "namespace", Target = "SimpleSyndicate.Mvc.App_Start", Justification = "Use of underscore in App_Start is the convention in MVC.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Controllers.Account", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.MinorVersion", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.ReleaseDate", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Models", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Web.Mvc.Html", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Web.Routing", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Repositories", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.ReleaseNotes", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.PointVersion", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Controllers.VersionHistory.DataAnnotations.MajorVersion", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Auditing", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.AspNet.Identity", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Naming", "CA1726:UsePreferredTerms", MessageId = "Login", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Controllers.AspNetUserLoginDetails.DataAnnotations", Justification = "Login is the convention in MVC.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Controllers.AspNetUserLoginDetails.DataAnnotations", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.Controllers.AspNetUserDefaultRoles.DataAnnotations", Justification = "Number of types dictated by design.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA2210:AssembliesShouldHaveValidStrongNames", Justification = "Strong naming considered harmful.")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", Scope = "namespace", Target = "SimpleSyndicate.Mvc.DependencyInjection.SimpleInjector", Justification = "Number of types dictated by design.")]
