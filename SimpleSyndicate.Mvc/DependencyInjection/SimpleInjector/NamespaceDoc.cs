﻿namespace SimpleSyndicate.Mvc.DependencyInjection.SimpleInjector
{
	/// <summary>
	/// The <see cref="SimpleInjector"/> namespace contains classes for working with the Simple Injector dependency injection package.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}