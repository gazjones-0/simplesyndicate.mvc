﻿using System;
using SimpleInjector;
using SimpleSyndicate.Models;
using SimpleSyndicate.Repositories;

namespace SimpleSyndicate.Mvc.DependencyInjection.SimpleInjector
{
	/// <summary>
	/// The <see cref="ContainerExtensions"/> class contains methods that extend the <see cref="Container"/> class.
	/// </summary>
	public static class ContainerExtensions
	{
		/// <summary>
		/// Gets whether <see cref="IRepository{VersionHistoryItem}"/> is registered.
		/// </summary>
		/// <param name="container">The Simple Injector <see cref="Container"/> instance that this method extends.</param>
		/// <returns><c>true</c> if <see cref="IRepository{VersionHistoryItem}"/> is registered; <c>false</c> otherwise.</returns>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="container"/> is <c>null</c>.</exception>
		public static bool IsVersionHistoryItemRepositoryRegistered(this Container container)
		{
			if (container == null)
			{
				throw new ArgumentNullException("container");
			}
			var registrations = container.GetCurrentRegistrations();
			foreach (var registration in registrations)
			{
				if (registration.ServiceType == typeof(IRepository<VersionHistoryItem>))
				{
					return true;
				}
			}
			return false;
		}
	}
}