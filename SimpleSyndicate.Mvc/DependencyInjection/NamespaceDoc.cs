﻿namespace SimpleSyndicate.Mvc.DependencyInjection
{
	/// <summary>
	/// The <see cref="DependencyInjection"/> namespace contains classes for working with dependency injection packages.
	/// </summary>
	[System.Runtime.CompilerServices.CompilerGeneratedAttribute]
	internal class NamespaceDoc
	{
	}
}