﻿using Microsoft.AspNet.Identity.EntityFramework;
using SimpleSyndicate.Mvc.Models;

namespace SimpleSyndicate.Mvc.Help.Examples.Models
{
	public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
	{
		public ApplicationDbContext()
			: base("DefaultConnection", throwIfV1Schema: false)
		{
		}
	}
}
