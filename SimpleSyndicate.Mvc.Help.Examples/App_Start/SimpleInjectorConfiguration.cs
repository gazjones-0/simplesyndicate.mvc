﻿using System;
using Owin;
using SimpleInjector;
using SimpleSyndicate.Mvc.App_Start;

namespace SimpleSyndicate.Mvc.Help.Examples.App_Start
{
	/// <summary>
	/// <para>
	/// Template Simple Injector configuration class, ready to use in an MVC application.
	/// </para>
	/// <para>
	/// 1. Create a <c>SimpleInjectorConfiguration</c> class in the <c>App_Start</c> folder of your MVC application;
	/// </para>
	/// <para>
	/// 2. Copy and paste the whole contents of this file into it;
	/// </para>
	/// <para>
	/// 3. Change the namespace from <c>SimpleSyndicate.Mvc.Help.Examples.App_Start</c> to whatever is appropriate for your app, e.g. <c>MyMvcApp.App_Start</c>;
	/// </para>
	/// <para>
	/// 4. Add <c>App_Start.SimpleInjectorConfiguration.Configure();</c> to <c>Application_Start</c> in global.asax.cx;
	/// </para>
	/// <para>
	/// 5. Customise <c>SetupContainer</c> as required.
	/// </para>
	/// </summary>
	public class SimpleInjectorConfiguration : SimpleInjectorConfigurationBase<Models.ApplicationDbContext>
	{
		/// <summary>
		/// Configures Simple Injector by creating a new instance of the <see cref="SimpleInjectorConfiguration"/> class and calling <see cref="SimpleInjectorConfigurationBase{TIdentityDbContext, TApplicationUser, TApplicationUserManager, TApplicationSignInManager}.Setup"/>.
		/// </summary>
		/// <param name="appBuilder">The app builder in use; this will be registered with the <see cref="Container"/> as a single instance.</param>
		public static void Configure(IAppBuilder appBuilder)
		{
			var simpleInjectorConfiguration = new SimpleInjectorConfiguration();

			// the configuration might get set up multiple times (e.g. during unit tests), so call Setup if
			// it has never been set up, or ReSetup if it is already set up
			if (SimpleInjectorConfiguration.Container == null)
			{
				simpleInjectorConfiguration.Setup(appBuilder, () => new Models.ApplicationDbContext());
			}
			else
			{
				simpleInjectorConfiguration.ReSetup();
			}
		}

		/// <summary>
		/// Performs application-specific ASP.Net Identity configuration.
		/// </summary>
		protected override void SetupAspNetIdentityConfiguration()
		{
			// set the configuration through this.AspNetIdentityConfiguration if you need something
			// different to the standard settings which should be suitable for most MVC applications
		}

		/// <summary>
		/// Performs application-specific Simple Injector configuration.
		/// </summary>
		/// <param name="appBuilder">The app builder in use.</param>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="appBuilder"/> is <c>null</c>.</exception>
		protected override void SetupContainer(IAppBuilder appBuilder)
		{
			if (appBuilder == null)
			{
				throw new ArgumentNullException("appBuilder");
			}

			// add your application specific registrations here
		}
	}
}
