﻿using SimpleSyndicate.Mvc.App_Start;

namespace SimpleSyndicate.Mvc.Help.Examples.App_Start
{
	/// <summary>
	/// <para>
	/// Template AutoMapper configuration class, ready to use in an MVC application.
	/// </para>
	/// <para>
	/// 1. Create an <c>AutoMapperConfiguration</c> class in the <c>App_Start</c> folder of your MVC application;
	/// </para>
	/// <para>
	/// 2. Copy and paste the whole contents of this file into it;
	/// </para>
	/// <para>
	/// 3. Change the namespace from <c>SimpleSyndicate.Mvc.Help.Examples.App_Start</c> to whatever is appropriate for your app, e.g. <c>MyMvcApp.App_Start</c>;
	/// </para>
	/// <para>
	/// 4. Add <c>App_Start.AutoMapperConfiguration.Configure();</c> to <c>Application_Start</c> in the <c>global.asax.cs</c> file of your MVC application;
	/// </para>
	/// <para>
	/// 5. Customise <c>SetupMappings</c> to configure the mappings for your MVC application.
	/// </para>
	/// </summary>
	public class AutoMapperConfiguration : AutoMapperConfigurationBase
	{
		/// <summary>
		/// Configures AutoMapper by creating a new instance of the <see cref="AutoMapperConfiguration"/> class and calling <see cref="AutoMapperConfigurationBase.Setup"/>.
		/// </summary>
		public static void Configure()
		{
			var autoMapperConfiguration = new AutoMapperConfiguration();
			autoMapperConfiguration.Setup();
		}

		/// <summary>
		/// Sets up application-specific mappings for AutoMapper.
		/// </summary>
		protected override void SetupMappings()
		{
			// setup your mappings here; for code readability and maintenance it's recommended that you create a sub-folder
			// called AutoMapperMappings and create a class per entity being mapped that sets up the mappings for that entity

			// for an example see SimpleSyndicate.Mvc.App_Start.AutoMapperMappings.VersionHistoryItem that would be called like:
			// AutoMapperMappings.VersionHistoryItem.CreateMaps();
			// NB: by default VersionHistoryItem is already mapped by AutoMapperConfigurationBase
		}
	}
}