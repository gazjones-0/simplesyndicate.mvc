﻿using System;
using System.Web.Mvc;
using SimpleSyndicate.Mvc.App_Start;

namespace SimpleSyndicate.Mvc.Help.Examples.App_Start
{
	/// <summary>
	/// <para>
	/// Template filter configuration class, ready to use in an MVC application.
	/// </para>
	/// <para>
	/// 1. Create a <c>FilterConfiguration</c> class in the <c>App_Start</c> folder of your MVC application;
	/// </para>
	/// <para>
	/// 2. Copy and paste the whole contents of this file into it;
	/// </para>
	/// <para>
	/// 3. Change the namespace from <c>SimpleSyndicate.Mvc.Help.Examples.App_Start</c> to whatever is appropriate for your app, e.g. <c>MyMvcApp.App_Start</c>;
	/// </para>
	/// <para>
	/// 4. Add <c>App_Start.FilterConfiguration.Configure();</c> to <c>Application_Start</c> in the <c>global.asax.cs</c> file of your MVC application;
	/// </para>
	/// <para>
	/// 5. Customise <c>SetupFilters</c> to configure the filters for your MVC application.
	/// </para>
	/// </summary>
	public class FilterConfiguration : FilterConfigurationBase
	{
		/// <summary>
		/// Configures filters by creating a new instance of the <see cref="FilterConfiguration"/> class and calling <see cref="FilterConfigurationBase.Setup(SimpleInjector.Container)"/>.
		/// </summary>
		public static void Configure()
		{
			var filterConfiguration = new FilterConfiguration();
			filterConfiguration.Setup(SimpleInjectorConfiguration.Container);
		}

		/// <summary>
		/// Performs application-specific filter configuration.
		/// </summary>
		/// <param name="filters">The current filters.</param>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="filters"/> is <c>null</c>.</exception>
		protected override void SetupFilters(GlobalFilterCollection filters)
		{
			if (filters == null)
			{
				throw new ArgumentNullException("filters");
			}
		}
	}
}
