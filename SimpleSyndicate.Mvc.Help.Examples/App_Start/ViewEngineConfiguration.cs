﻿using System;
using System.Web.Mvc;
using SimpleSyndicate.Mvc.App_Start;

namespace SimpleSyndicate.Mvc.Help.Examples.App_Start
{
	/// <summary>
	/// <para>
	/// Template view engine configuration class, ready to use in an MVC application.
	/// </para>
	/// <para>
	/// 1. Create a <c>ViewEngineConfiguration</c> class in the <c>App_Start</c> folder of your MVC application;
	/// </para>
	/// <para>
	/// 2. Copy and paste the whole contents of this file into it;
	/// </para>
	/// <para>
	/// 3. Change the namespace from <c>SimpleSyndicate.Mvc.Help.Examples.App_Start</c> to whatever is appropriate for your app, e.g. <c>MyMvcApp.App_Start</c>;
	/// </para>
	/// <para>
	/// 4. Add <c>App_Start.ViewEngineConfiguration.Configure();</c> to <c>Application_Start</c> in the <c>global.asax.cs</c> file of your MVC application;
	/// </para>
	/// <para>
	/// 5. If you want to use MVC Code Routing add <c>App_Start.ViewEngineConfiguration.EnableCodeRouting();</c> to <c>Application_Start</c> in global.asax.cx, but it must go after <c>App_Start.ViewEngineConfiguration.Configure();</c>.
	/// </para>
	/// <para>
	/// 6. Customise <c>SetupViewEngines</c> if you want to configure the view engines differently.
	/// </para>
	/// </summary>
	public class ViewEngineConfiguration : ViewEngineConfigurationBase
	{
		/// <summary>
		/// Configures the view engines by creating a new instance of the <see cref="ViewEngineConfiguration"/> class and calling <see cref="ViewEngineConfigurationBase.Setup"/>.
		/// </summary>
		public static void Configure()
		{
			var viewEngineConfiguration = new ViewEngineConfiguration();
			viewEngineConfiguration.Setup();
		}

		/// <summary>
		/// Sets up the view engines to be just a single, C#-specific, view engine.
		/// </summary>
		/// <param name="viewEngines">The current view engines.</param>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="viewEngines"/> is <c>null</c>.</exception>
		protected override void SetupViewEngines(ViewEngineCollection viewEngines)
		{
			if (viewEngines == null)
			{
				throw new ArgumentNullException("viewEngines");
			}
			ViewEngineConfigurer.RegisterCSharpRazorViewEngineOnly();
		}
	}
}