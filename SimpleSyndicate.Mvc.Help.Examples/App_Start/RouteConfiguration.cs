﻿using System;
using System.Web.Mvc;
using System.Web.Routing;
using MvcCodeRouting;
using SimpleSyndicate.Mvc.App_Start;

namespace SimpleSyndicate.Mvc.Help.Examples.App_Start
{
	/// <summary>
	/// <para>
	/// Template route configuration class, ready to use in an MVC application.
	/// </para>
	/// <para>
	/// 1. Create a <c>RouteConfiguration</c> class in the <c>App_Start</c> folder of your MVC application;
	/// </para>
	/// <para>
	/// 2. Copy and paste the whole contents of this file into it;
	/// </para>
	/// <para>
	/// 3. Change the namespace from <c>SimpleSyndicate.Mvc.Help.Examples.App_Start</c> to whatever is appropriate for your app, e.g. <c>MyMvcApp.App_Start</c>;
	/// </para>
	/// <para>
	/// 4. Add <c>App_Start.RouteConfiguration.Configure();</c> to <c>Application_Start</c> in the <c>global.asax.cs</c> file of your MVC application;
	/// </para>
	/// <para>
	/// 5. Customise <c>SetupRoutes</c> to configure the routes for your MVC application.
	/// </para>
	/// </summary>
	public class RouteConfiguration : RouteConfigurationBase
	{
		/// <summary>
		/// Configures routing by creating a new instance of the <see cref="RouteConfiguration"/> class and calling <see cref="RouteConfigurationBase.Setup"/>.
		/// </summary>
		public static void Configure()
		{
			var routeConfiguration = new RouteConfiguration();
			routeConfiguration.Setup();
		}

		/// <summary>
		/// Sets up any application-specific routes.
		/// </summary>
		/// <param name="routes">The current routes.</param>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="routes"/> is <c>null</c>.</exception>
		protected override void SetupRoutes(RouteCollection routes)
		{
			if (routes == null)
			{
				throw new ArgumentNullException("routes");
			}

			// change Controller in the following line to reference the home controller in your application
			routes.MapCodeRoutes(rootController: typeof(Controller));
		}
	}
}
