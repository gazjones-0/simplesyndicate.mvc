﻿using System;
using Owin;
using SimpleSyndicate.Mvc.App_Start;

namespace SimpleSyndicate.Mvc.Help.Examples.App_Start
{
	/// <summary>
	/// <para>
	/// Template authentication configuration class, ready to use in an MVC application.
	/// </para>
	/// <para>
	/// 1. Create a <c>AuthenticationConfiguration</c> class in the <c>App_Start</c> folder of your MVC application;
	/// </para>
	/// <para>
	/// 2. Copy and paste the whole contents of this file into it;
	/// </para>
	/// <para>
	/// 3. Change the namespace from <c>SimpleSyndicate.Mvc.Help.Examples.App_Start</c> to whatever is appropriate for your app, e.g. <c>MyMvcApp.App_Start</c>;
	/// </para>
	/// <para>
	/// 4. Add <c>App_Start.AuthenticationConfiguration.Configure();</c> to <c>Application_Start</c> in the <c>global.asax.cs</c> file of your MVC application;
	/// </para>
	/// <para>
	/// 5. Customise <c>SetupAuthentication</c> to configure the authentication for your MVC application.
	/// </para>
	/// </summary>
	public class AuthenticationConfiguration : AuthenticationConfigurationBase
	{
		/// <summary>
		/// Configures authentication by creating a new instance of the <see cref="AuthenticationConfiguration"/> class and calling <see cref="AuthenticationConfigurationBase{TApplicationUser, TApplicationUserManager}.Setup"/>.
		/// </summary>
		/// <param name="appBuilder">The app builder being used.</param>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="appBuilder"/> is <c>null</c>.</exception>
		public static void Configure(IAppBuilder appBuilder)
		{
			if (appBuilder == null)
			{
				throw new ArgumentNullException("appBuilder");
			}
			var authenticationConfiguration = new AuthenticationConfiguration();
			authenticationConfiguration.Setup(appBuilder);
		}

		/// <summary>
		/// Performs application-specific authentication configuration.
		/// </summary>
		/// <param name="appBuilder">The app builder being used.</param>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="appBuilder"/> is <c>null</c>.</exception>
		protected override void SetupAuthentication(IAppBuilder appBuilder)
		{
			if (appBuilder == null)
			{
				throw new ArgumentNullException("appBuilder");
			}
			AuthenticationConfigurer.ConfigureCookie();
			AuthenticationConfigurer.ConfigureGoogle();
		}
	}
}