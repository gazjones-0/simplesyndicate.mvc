﻿using System;
using System.Web.Optimization;
using SimpleSyndicate.Mvc.App_Start;

namespace SimpleSyndicate.Mvc.Help.Examples.App_Start
{
	/// <summary>
	/// <para>
	/// Template bundle configuration class, ready to use in an MVC application.
	/// </para>
	/// <para>
	/// 1. Create a <c>BundleConfiguration</c> class in the <c>App_Start</c> folder of your MVC application;
	/// </para>
	/// <para>
	/// 2. Copy and paste the whole contents of this file into it;
	/// </para>
	/// <para>
	/// 3. Change the namespace from <c>SimpleSyndicate.Mvc.Help.Examples.App_Start</c> to whatever is appropriate for your app, e.g. <c>MyMvcApp.App_Start</c>;
	/// </para>
	/// <para>
	/// 4. Add <c>App_Start.BundleConfiguration.Configure();</c> to <c>Application_Start</c> in the <c>global.asax.cs</c> file of your MVC application;
	/// </para>
	/// <para>
	/// 5. Customise <c>SetupBundles</c> to configure the bundles for your MVC application.
	/// </para>
	/// </summary>
	public class BundleConfiguration : BundleConfigurationBase
	{
		/// <summary>
		/// Configures bundles by creating a new instance of the <see cref="BundleConfiguration"/> class and calling <see cref="BundleConfigurationBase.Setup"/>.
		/// </summary>
		public static void Configure()
		{
			var bundleConfiguration = new BundleConfiguration();
			bundleConfiguration.Setup();
		}

		/// <summary>
		/// Performs application-specific bundle configuration.
		/// </summary>
		/// <param name="bundles">The current bundles.</param>
		/// <exception cref="ArgumentNullException">Thrown when <paramref name="bundles"/> is <c>null</c>.</exception>
		protected override void SetupBundles(BundleCollection bundles)
		{
			if (bundles == null)
			{
				throw new ArgumentNullException("bundles");
			}
			SetupStyleBundles(bundles);
			SetupScriptBundles(bundles);
		}

		private static void SetupStyleBundles(BundleCollection bundles)
		{
			bundles.Add(new StyleBundle("~/Content/css").Include("~/Content/site.css"));
		}

		private static void SetupScriptBundles(BundleCollection bundles)
		{
			bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
				"~/Scripts/jquery-{version}.js"));

			bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
				"~/Scripts/jquery.validate*"));

			// use dev version of Modernizr; when ready for production, use build tool at http://modernizr.com to pick only tests needed
			bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
				"~/Scripts/modernizr-*"));
		}
	}
}
